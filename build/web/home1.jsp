<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

<head>
    <title>Home</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/home1.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>
    <jsp:include page="menu.jsp"></jsp:include>
    <div class="page-content bg-white" style="padding-top: 135px;">
        <div class="section-area section-sp1 ovpr-dark bg-fix online-cours">
            <div class="background-color-tim background-header-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center text-white pt-5 pb-5">
                            <h2>Online Courses To Learn</h2>
                            <h5>Own Your Feature Learning New Skills Online</h5>
                            <form class="cours-search pt-4 ">
                                <div class="input-group" style="width: 40%; margin: auto;">
                                    <input type="text" class="form-control" placeholder="What do you want to learn today?	">
                                    <div class="input-group-append">
                                        <button class="btn bg-warning" type="submit">Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="mw800 m-auto">
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main slider -->
        <div class="content-block pt-5">
            <!-- Popular Courses -->
            <div class="section-area section-sp2 popular-courses-bx">
                <div class="container">
                    <div class="row pb-5">
                        <div class="col-md-12 heading-bx left">
                            <h2 class="title-head">Popular<span>Courses</span></h2>
                            <p style="margin-top: 10px;">It is a long established fact that a reader will be distracted
                                by the readable content of a page</p>
                        </div>
                    </div>
                    <div id="carouselId" class="carousel slide p-3 bg-light" data-ride="carousel"
                        style="border-radius: 5px;">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselId" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselId" data-slide-to="1"></li>
                        <li data-target="#carouselId" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="item col-md-3">
                                       <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price">
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price">
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price">
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price">
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star ">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price text-center">
                                                    
                                                        <del>$190</del>
                                                        <h5>$120</h5>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price">
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price">
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price">
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="item col-md-3">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="./image/course/pic1.jpg" alt="">
                                            <a href="#" class="btn">Read More</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                            <span>Programming</span>
                                        </div>
                                        <div class="cours-more-info">
                                            <div class="review">
                                                <span>3 Review</span>
                                                <ul class="cours-star ">
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                            <div class="price text-center">
                                                
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item col-md-3">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="./image/course/pic1.jpg" alt="">
                                            <a href="#" class="btn">Read More</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                            <span>Programming</span>
                                        </div>
                                        <div class="cours-more-info">
                                            <div class="review">
                                                <span>3 Review</span>
                                                <ul class="cours-star">
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                            <div class="price">
                                                <del>$190</del>
                                                <h5>$120</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item col-md-3">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="./image/course/pic1.jpg" alt="">
                                            <a href="#" class="btn">Read More</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                            <span>Programming</span>
                                        </div>
                                        <div class="cours-more-info">
                                            <div class="review">
                                                <span>3 Review</span>
                                                <ul class="cours-star">
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                            <div class="price">
                                                <del>$190</del>
                                                <h5>$120</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item col-md-3">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="./image/course/pic1.jpg" alt="">
                                            <a href="#" class="btn">Read More</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                            <span>Programming</span>
                                        </div>
                                        <div class="cours-more-info">
                                            <div class="review">
                                                <span>3 Review</span>
                                                <ul class="cours-star">
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                            <div class="price">
                                                <del>$190</del>
                                                <h5>$120</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <a style="width: 50px"  class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            
                            <span class="sr-only">Previous</span>
                        </a>
                        <a style="width: 50px" class="carousel-control-next"  href="#carouselId" role="button" data-slide="next">
                            <span class="carousel-control-next-icon " aria-hidden="true"></span>
                            
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>


        <!-- Popular Courses END -->
        <div class="content-block">
            <div class="section-area section-sp2 bg-fix ovbl-dark join-bx text-center"
                style="background-image:url(image/bg1.jpg);">
                
            </div>
        </div>

        <!-- Form END -->
        

        <!-- Testimonials -->
        <div class="section-area section-5 bg-fix text-white" style="background-image:url(../images/bg1.jpg);">
            <div class="background-color-tim">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                            <div class="counter-style-1">
                                <div class="text-white">
                                    <span class="counter">3000</span><span class="cong">+</span>
                                </div>
                                <span class="counter-text">Completed Projects</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                            <div class="counter-style-1">
                                <div class="text-white">
                                    <span class="counter">2500</span><span class="cong">+</span>
                                </div>
                                <span class="counter-text">Happy Clients</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                            <div class="counter-style-1">
                                <div class="text-white">
                                    <span class="counter">1500</span><span class="cong">+</span>
                                </div>
                                <span class="counter-text">Questions Answered</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                            <div class="counter-style-1">
                                <div class="text-white">
                                    <span class="counter">1000</span><span class="cong">+</span>
                                </div>
                                <span class="counter-text">Ordered Coffee's</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonials END -->

        <!-- Lớp học nổi bật -->
        <section>
            <div class="container" style="padding: 70px 0px;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="container ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="widget">
                                            <h2 class="widget-title line-bottom-theme-colored-2 ">Gia sư <b
                                                    class="text-theme-colored2">Mới</b></h2>
                                            <ul class="angle-double-right list-border">
                                                <li><a target="_blank" href="/gia-su/trinh-thi-kim-chi-45064"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/45064_avatar.jpeg"
                                                            alt="Trịnh Thị Kim Chi" class="img-circle mr-10"> Trịnh Thị Kim
                                                        Chi</a> </li>
                                                <li><a target="_blank" href="/gia-su/nguyen-thi-tam-45043"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/45043_avatar.jpeg"
                                                            alt="Nguyễn Thị Tâm" class="img-circle mr-10"> Nguyễn Thị
                                                        Tâm</a> </li>
                                                <li><a target="_blank" href="/gia-su/dong-ho-anh-minh-44957"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44957_avatar.jpeg"
                                                            alt="Đồng Hồ Ánh Minh" class="img-circle mr-10"> Đồng Hồ Ánh
                                                        Minh</a> </li>
                                                <li><a target="_blank" href="/gia-su/phung-thi-diep-44937"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44937_avatar.jpg"
                                                            alt="Phùng Thị Điệp" class="img-circle mr-10"> Phùng Thị
                                                        Điệp</a> </li>
                                                <li><a target="_blank" href="/gia-su/nguyen-thi-dieu-linh-44795"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44795_avatar.jpg"
                                                            alt="Nguyễn Thị diệu Linh" class="img-circle mr-10"> Nguyễn Thị
                                                        diệu Linh</a> </li>
                                                <li><a target="_blank" href="/gia-su/bao-ngoc-44787"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44787_avatar.jpeg"
                                                            alt="Bảo Ngọc" class="img-circle mr-10"> Bảo Ngọc</a> </li>
                                                <li><a target="_blank" href="/gia-su/thanh-ngan-44785"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44785_avatar.jpeg"
                                                            alt="Thanh Ngân" class="img-circle mr-10"> Thanh Ngân</a> </li>
                                                <li><a target="_blank" href="/gia-su/pham-tran-mai-duyen-44780"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44780_avatar.jpeg"
                                                            alt="Phạm Trần Mai Duyên" class="img-circle mr-10"> Phạm Trần
                                                        Mai Duyên</a> </li>
                                                <li><a target="_blank" href="/gia-su/cao-thi-yen-nhi-44779"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44779_avatar.jpeg"
                                                            alt="Cao Thị Yến Nhi" class="img-circle mr-10"> Cao Thị Yến
                                                        Nhi</a> </li>
                                                <li><a target="_blank" href="/gia-su/tran-thuy-linh-44778"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44778_avatar.jpg"
                                                            alt="Trần Thuỳ Linh" class="img-circle mr-10"> Trần Thuỳ
                                                        Linh</a> </li>
    
                                            </ul>
    
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="widget">
                                            <h2 class="widget-title line-bottom-theme-colored-2 ">Gia sư nổi bật<b
                                                    class="text-theme-colored2"> Tháng 05</b></h2>
                                            <ul class="angle-double-right list-border">
                                                <li><a target="_blank" href="/gia-su/pham-thi-thom-2946"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/2946_avatar.jpg"
                                                            alt="Phạm Thị Thơm" class="img-circle mr-10"> Phạm Thị Thơm</a>
                                                    <span class="badge" data-toggle="tooltip" data-placement="top" title=""
                                                        data-original-title="2 lần nhận lớp">2</span></li>
                                                <li><a target="_blank" href="/gia-su/nguyen-tang-ky-15168"><img
                                                            style="width:24px;"
                                                            src="../images/default-avatar.png"
                                                            alt="Nguyễn Tăng Kỳ" class="img-circle mr-10"> Nguyễn Tăng
                                                        Kỳ</a> <span class="badge" data-toggle="tooltip"
                                                        data-placement="top" title=""
                                                        data-original-title="2 lần nhận lớp">2</span></li>
                                                <li><a target="_blank" href="/gia-su/vo-phuc-hanh-38741"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/38741_avatar.jpeg"
                                                            alt="Võ Phúc Hạnh" class="img-circle mr-10"> Võ Phúc Hạnh</a>
                                                    <span class="badge" data-toggle="tooltip" data-placement="top" title=""
                                                        data-original-title="2 lần nhận lớp">2</span></li>
                                                <li><a target="_blank" href="/gia-su/truong-thi-thu-ha-22117"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/22117_avatar.jpeg"
                                                            alt="Trương Thị Thu Hà" class="img-circle mr-10"> Trương Thị Thu
                                                        Hà</a> <span class="badge" data-toggle="tooltip"
                                                        data-placement="top" title=""
                                                        data-original-title="1 lần nhận lớp">1</span></li>
                                                <li><a target="_blank" href="/gia-su/huynh-thi-kim-thanh-1550"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/1550_avatar.jpeg"
                                                            alt="HUỲNH THỊ KIM THANH" class="img-circle mr-10"> HUỲNH THỊ
                                                        KIM THANH</a> <span class="badge" data-toggle="tooltip"
                                                        data-placement="top" title=""
                                                        data-original-title="1 lần nhận lớp">1</span></li>
                                                <li><a target="_blank" href="/gia-su/nguyen-thanh-tung-44502"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44502_avatar.jpeg"
                                                            alt="Nguyễn Thanh Tùng" class="img-circle mr-10"> Nguyễn Thanh
                                                        Tùng</a> <span class="badge" data-toggle="tooltip"
                                                        data-placement="top" title=""
                                                        data-original-title="1 lần nhận lớp">1</span></li>
                                                <li><a target="_blank" href="/gia-su/nguyen-hoang-tam-37416"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/37416_avatar.jpeg"
                                                            alt="Nguyễn Hoàng Tâm" class="img-circle mr-10"> Nguyễn Hoàng
                                                        Tâm</a> <span class="badge" data-toggle="tooltip"
                                                        data-placement="top" title=""
                                                        data-original-title="1 lần nhận lớp">1</span></li>
                                                <li><a target="_blank" href="/gia-su/dinh-vu-uyen-giang-2011"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/2011_avatar.jpeg"
                                                            alt="Đinh Vũ Uyên Giang" class="img-circle mr-10"> Đinh Vũ Uyên
                                                        Giang</a> <span class="badge" data-toggle="tooltip"
                                                        data-placement="top" title=""
                                                        data-original-title="1 lần nhận lớp">1</span></li>
                                                <li><a target="_blank" href="/gia-su/nguyen-thi-thanh-thuy-43797"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/43797_avatar.JPG"
                                                            alt="Nguyễn Thị Thanh Thuỷ" class="img-circle mr-10"> Nguyễn Thị
                                                        Thanh Thuỷ</a> <span class="badge" data-toggle="tooltip"
                                                        data-placement="top" title=""
                                                        data-original-title="1 lần nhận lớp">1</span></li>
                                                <li><a target="_blank" href="/gia-su/vu-minh-nhat-43647"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/43647_avatar.jpeg"
                                                            alt="Vũ Minh Nhật" class="img-circle mr-10"> Vũ Minh Nhật</a>
                                                    <span class="badge" data-toggle="tooltip" data-placement="top" title=""
                                                        data-original-title="1 lần nhận lớp">1</span></li>
    
                                            </ul>
    
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="widget">
                                            <h2 class="widget-title line-bottom-theme-colored-2 ">Lớp <b
                                                    class="text-theme-colored2">Mới</b></h2>
                                            <ul class="angle-double-right list-border">
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-day-kem-tieng-phap-cho-be-lop-4-tai-quan-8-ho-chi-minh-19249"><b>19249</b>
                                                        - Cần gia sư dạy kèm tiếng pháp cho bé lớp 4 tại quận 8, Hồ Chí
                                                        Minh</a> </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-mon-toan-lop-8-tai-quan-8-ho-chi-minh-19248"><b>19248</b>
                                                        - Cần gia sư môn Toán lớp 8 tại quận 8, Hồ Chí Minh</a> </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-mon-nhay-hien-dai-tai-quan-7-ho-chi-minh-19247"><b>19247</b>
                                                        - Cần gia sư môn nhảy hiện đại tại quận 7, Hồ Chí Minh</a> </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-cho-be-hoc-lop-7-tai-bien-hoa-dong-nai-19246"><b>19246</b>
                                                        - Cần gia sư cho bé học lớp 7 tại Biên Hòa, Đồng Nai</a> </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-1-ban-day-mon-piano-cho-trung-tam-tai-go-vap-ho-chi-minh-19240"><b>19240</b>
                                                        - Cần 1 bạn dạy môn Piano cho trung tâm tại Gò Vấp, Hồ Chí Minh</a>
                                                </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-mon-ukulele-tai-son-tra-da-nang-19227"><b>19227</b>
                                                        - Cần gia sư môn Ukulele tại Sơn Trà , Đà Nẵng</a> </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-mon-tieng-viet-cho-han-quoc-tai-nha-trang-khanh-hoa-hoc-online-19222"><b>19222</b>
                                                        - Cần gia sư môn Tiếng Việt cho Hàn Quốc tại Nha Trang, Khánh Hòa -
                                                        HỌC ONLINE</a> </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-mon-boxing-tai-phu-rieng-binh-phuoc-19214"><b>19214</b>
                                                        - Cần gia sư môn Boxing tại Phú Riềng, Bình Phước</a> </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-mon-trong-jazz-tai-xuyen-moc-ba-ria-vung-tau-19213"><b>19213</b>
                                                        - Cần gia sư môn Trống Jazz tại Xuyên Mộc, Bà Rịa - Vũng Tàu</a>
                                                </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-mon-violin-tai-long-bien-ha-noi-19206"><b>19206</b>
                                                        - Cần gia sư môn Violin tại Long Biên, Hà Nội</a> </li>
    
                                            </ul>
    
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12" style="padding: 0;">
                                    <div class="widget">
                                        <h4 class="widget-title line-bottom-theme-colored-2">Tìm gia sư theo các <b
                                                class="text-theme-colored2">môn phổ biến</b> </h4>
                                        <div class="tags subject-tag">

                                            <a class="active" ng-href="/gia-su?mon-hoc=Toán Lớp 2" href="/gia-su?mon-hoc=Toán Lớp 2">Toán Lớp 2</a>
                                            <a
                                                ng-href="/gia-su?mon-hoc=Toán lớp 3" href="/gia-su?mon-hoc=Toán lớp 3">Toán lớp 3</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Toán Lớp 1" href="/gia-su?mon-hoc=Toán Lớp 1">Toán Lớp 1</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Toán lớp 4" href="/gia-su?mon-hoc=Toán lớp 4">Toán lớp 4</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Toán lớp 5" href="/gia-su?mon-hoc=Toán lớp 5">Toán lớp 5</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Toán lớp 6" href="/gia-su?mon-hoc=Toán lớp 6">Toán lớp 6</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Toán lớp 7" href="/gia-su?mon-hoc=Toán lớp 7">Toán lớp 7</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Việt Lớp 1" href="/gia-su?mon-hoc=Tiếng Việt Lớp 1">Tiếng Việt Lớp 1</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Việt Lớp 2" href="/gia-su?mon-hoc=Tiếng Việt Lớp 2">Tiếng Việt Lớp 2</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Toán lớp 8" href="/gia-su?mon-hoc=Toán lớp 8">Toán lớp 8</a><a
                                                ng-href="/gia-su?mon-hoc=Tiếng Việt lớp 3" href="/gia-su?mon-hoc=Tiếng Việt lớp 3">Tiếng Việt lớp 3</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Việt lóp 4" href="/gia-su?mon-hoc=Tiếng Việt lóp 4">Tiếng Việt lóp 4</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Anh" href="/gia-su?mon-hoc=Tiếng Anh">Tiếng Anh</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Anh lớp 3" href="/gia-su?mon-hoc=Tiếng Anh lớp 3">Tiếng Anh lớp 3</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Toán lớp 9	" href="/gia-su?mon-hoc=Toán lớp 9	">Toán lớp 9 </a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Đàn Piano" href="/gia-su?mon-hoc=Đàn Piano">Đàn
                                                Piano</a><a ng-href="/gia-su?mon-hoc=Tiếng Anh Lớp 2" href="/gia-su?mon-hoc=Tiếng Anh Lớp 2">Tiếng Anh Lớp 2</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Anh Lớp 1" href="/gia-su?mon-hoc=Tiếng Anh Lớp 1">Tiếng Anh Lớp 1</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Việt lớp 5" href="/gia-su?mon-hoc=Tiếng Việt lớp 5">Tiếng Việt lớp 5</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Anh lóp 4" href="/gia-su?mon-hoc=Tiếng Anh lóp 4">Tiếng Anh lóp 4</a>

                                        </div>

                                    </div>
                                </div>


                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </section>
        <!-- section - event -->
        
    </div>
    <jsp:include page="footer.jsp"></jsp:include>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>

</html>