<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <title>Title</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/menu.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
    <style>
        .header{
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            background-color: #f2f2f2;
            transition: top 0.3s ease-in-out;
            z-index: 9998;

        }
        .header.scrolled{
            top: -100%;
        }
    </style>

    <body>
        <div id="header" class="header">
            <div class="header-top  " style="height: 45px ;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 ">
                            <div class="topbar-left">
                                <ul class="list-inline topbar-left-infor xs-text-center text-white ">
                                    <li class="m-0 pl-2 pr-1">
                                        <span>

                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <ul class="list-inline  text-right text-white mb-sm-10 mt-2 ">
                                <li class="mb-5">
                                    <div class="dropdown">
                                        <a style="text-decoration: none;" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <b class="text-dark text-weight-900 mr-2">${sessionScope.acc.user_name}</b>
                                            <img style="display:inline-block; border-radius: 40px;"
                                                 src="./images/default-avatar.png" class="avatar img-responsive" width="30"
                                                 height="30">
                                        </a>
                                        <style>
                                            .nut-login{
                                                border-radius: 8px;
                                                color: white;
                                                background-color: green;
                                                width: 100px;

                                            }

                                            .nut-login:hover{
                                                color: black;
                                                background-color: yellow;
                                                border-radius: 8px;
                                            }

                                        </style>
                                        <c:if test="${sessionScope.acc == null}">
                                            <a style="text-decoration: none;" href="login" class="nut-login">
                                                <b class=" text-weight-900 mr-1 login-home1"> Login </b>
                                                <i style="display:inline-block;" class="fa fa-sign-in " aria-hidden="true"></i>
                                            </a>
                                        </c:if>

                                        <ul class="dropdown-menu mt-2" aria-labelledby="dLabel">
                                            <li><a class="dropdown-item" href="editprofile"><i
                                                        class="fa fa-info-circle" aria-hidden="true"></i> Thông tin cá
                                                    nhân</a></li>
                                            <li><a class="dropdown-item" href="/thong-tin-dang-nhap"><i
                                                        class="fa fa-user-circle" aria-hidden="true"></i> Thông tin đăng
                                                    nhập</a></li>

                                            <c:if test="${sessionScope.acc.role_id == 3}">  
                                                <li><a class="dropdown-item" href="listRequest"><i
                                                            class="far fa-list-alt" aria-hidden="true"></i> List request by me</a></li>
                                                <li><a class="dropdown-item" href="staticRequest"><i
                                                            class="far fa-list-alt" aria-hidden="true"></i> Statistic request by me</a></li>
                                                    </c:if>

                                            <c:if test="${sessionScope.acc.role_id == 2}">  
                                                <li role="separator" class="divider"></li>
                                                <li><a class="dropdown-item" href="ViewAllStatisticRequestMentor"><i class="fas fa-glasses"
                                                                                                                     aria-hidden="true"></i>View All Statistic Request</a></li>
                                                <li><a class="dropdown-item" href="listinviting"><i class="fas fa-glasses"
                                                                                                    aria-hidden="true"></i> List Inviting Request</a></li>
                                                    </c:if>
                                            <li role="separator" class="divider"></li>
                                                <c:if test="${sessionScope.acc != null}">
                                                <li><a class="dropdown-item" href="logout"><i
                                                            class="fas fa-sign-out-alt" aria-hidden="true"></i> Đăng xuất</a>
                                                </li>
                                            </c:if>
                                            <c:if test="${sessionScope.acc == null}">
                                                <li><a class="dropdown-item" href="login"><i class="fas fa-sign-out-alt" aria-hidden="true"></i> Đăng nhập</a>
                                                </li>
                                            </c:if>
                                        </ul>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
            <!-- menu -->
            <div class="header-nav" style="box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1); ">
                <div class="header-nav-wrapper bg-light navbar-inverse" style="z-index: 999;">
                    <div class="container-fluid">
                        <nav class="navbar navbar-expand-sm navbar-light ">
                            <a class="navbar-brand ml-5" href="home1.jsp"><img style="width: 100px;"
                                                                               src="./images/R-removebg-preview.png" alt=""></a>
                            <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse"
                                    data-target="#collapsibleNavId" aria-controls="collapsibleNavId" aria-expanded="false"
                                    aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <div class="collapse navbar-collapse" id="collapsibleNavId">
                                <ul class="navbar-nav ml-auto mt-2 mr-5 mt-lg-0 menu" style="column-gap: 65px;">
                                    <li class="nav-item active">
                                        <a class="nav-link text-white" href="home1.jsp">Home </a>
                                    </li>
                                    <li class="nav-item ">
                                        <a class="nav-link" href="#">Find Mentor</a>
                                    </li>
                                    <c:if test="${sessionScope.acc.role_id == 2 && sessionScope.status == 3}">
                                        <li class="nav-item ">
                                            <a class="nav-link" href="registmentor">Create CV</a>
                                        </li>
                                    </c:if>

                                    <c:if test="${sessionScope.acc.role_id == 1}">
                                        <li class="nav-item ">
                                            <a class="nav-link" href="listskill">List Skill</a>
                                        </li>
                                    </c:if>
                                    <li class="nav-item ">
                                        <a class="nav-link" href="viewskill">Skill</a>
                                    </li>
                                    <c:if test="${sessionScope.acc.role_id == 1}">

                                        <li class="nav-item ">
                                            <a class="nav-link" href="StatisticOfAllMentee">Manager</a>
                                        </li>
                                    </c:if>
                                    <c:if test="${sessionScope.acc.role_id == 3}">
                                        <li class="nav-item ">
                                            <a class="nav-link" href="listallmentor">List Mentor </a>
                                        </li>
                                    </c:if>


                                </ul>
                                <form class="form-inline my-2 my-lg-0">
                                    <input class="form-control mr-sm-2" type="text" placeholder="Search">
                                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fa fa-search"></i> Search</button>
                                </form>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    </body>

</html>
