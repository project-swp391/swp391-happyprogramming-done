
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!doctype html>
<html lang="en">

    <head>
        <title>Statistic Request By Me</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    </head>
    <style>
        body {
            background-image: url("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQCHlZo0FxUN3WuSp1Z_eM1OzoJh4qCdIzgCWaNmfjXTLrIA_p8LQCSa8k18GJw2o6n2y0&usqp=CAU");
        }

        .text-thong-ke {
            font-family: 'Times New Roman', Times, serif;
            color: rgb(158, 208, 153);
            font-size: 40px;

        }

        .table-thong-ke {
            border-radius: 10px;
            justify-content: center;
            column-gap: 40px;

        }

        .content-thong-ke {
            font-family: 'Times New Roman', Times, serif;
            font-size: 20px;
        }

        .text-thong-ke .line::after {
            content: "";
            display: block;
            height: 2px;
            width: 165px;
            background-color: #6f7478;
        }

        .content-thong-ke {
            border: 1px solid gray;
            border-radius: 20px;
            box-shadow: 10px 10px gray;

        }

        .content-thong-ke p {
            color: rgb(15, 32, 13);
        }
    </style>

    <body>
        <Section>
            <button style="background-color: greenyellow; border-radius: 5px"><a style="text-decoration: none; color: black" href="home1.jsp">Home</a></button>
            <div class="container pb-5">
                <div class="thong-ke-request">
                    <div class="text-thong-ke pb-3 ">
                        <p class="line">Thống Kê</p>
                    </div>

                    <div class="table-thong-ke row text-center text-secondary">
                        <div class="col-md-3 mt-2 content-thong-ke bg-light">
                            <p>Tổng số yêu cầu</p>
                            <div>
                                <p class="text-dark">${totalRequest}</p>
                            </div>
                        </div>
                        <div class="col-md-3 mt-2 content-thong-ke bg-light">
                            <p>Tổng thời gian yêu cầu</p>
                            <div>
                                <p class="text-dark">${totalHour}</p>
                            </div>
                        </div>
                        <div class="col-md-3 mt-2 content-thong-ke bg-light">
                            <p>Tổng số mentor</p>
                            <div>
                                <p class="text-dark">${totalMentor}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-statistical container pt-5">
                <table class="table" style="border:  1px solid gray;">
                    <thead class="bg-dark text-white">
                        <tr class="text-center">
                            <th scope="col">STT</th>
                            <th scope="col">From</th>
                            <th scope="col">To</th>
                            <th scope="col">Content</th>
                            <th scope="col">Create Date</th>
                            <th scope="col">Deadline Date</th>
                            <th scope="col">Deadline Hour</th>

                        </tr>
                    </thead>
                <c:set var="stt" value="1"></c:set>
                    <tbody>
                        <c:forEach items="${listReq}" var="lr" >
                            <tr class="text-center">
                                <th scope="row">${lr.id}</th>
                                <td>${lr.from.user_name}</td>
                                <td>${lr.to.user_name}</td>
                                <td>${lr.content}</td>
                                <td>${lr.createDate}</td>
                                <td>${lr.deadlineDate}</td>
                                <td>${lr.deadlineHour}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </Section>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    </body>

</html>
