<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>List of Invited Requests</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <style>


            .request-card {
                border-radius: 10px;
                box-shadow: 0 2px 6px rgba(0, 0, 0, 0.1);
                margin-bottom: 20px;
            }
            .request-card .card-header {
                background-color: #f8f9fa;
                font-weight: bold;
            }
            .request-card .card-body {
                padding: 20px;
            }
            .request-card .btn-action {
                margin-right: 10px;
                font-weight: bold;
                .empty-page {
                    display: flex;
                    flex-direction: column;
                    justify-content: center;
                    align-items: center;
                    height: 100vh;
                }

                .empty-image {
                    width: 200px;
                    height: 200px;

                    background-size: cover;
                    background-position: center;
                    margin-bottom: 20px;
                }

                .empty-page h2 {
                    font-size: 24px;
                    margin-bottom: 10px;
                }

                .empty-page p {
                    font-size: 16px;
                    color: #777;
                }
            }
        </style>
    </head>
    <body>
        <jsp:include page="menu.jsp"></jsp:include>
            <div class="container" style="padding-top: 125px;">
                <h1 class="mt-5 mb-4"></h1>

                <div class="row">
                <c:if test="${listR.size() == 0}">
                    <div class="col-md-12 empty-page" style="text-align: center;margin-top: 100px;">
                        <div class="empty-image"></div>
                        <h2>Danh sách trống</h2>
                        <p>Không có yêu cầu nào hiển thị</p>
                    </div>
                </c:if>
                <c:if test="${listR.size() != 0}">
                    <c:forEach var="r"  items="${listR}">
                        
                        <div class="col-md-12">
                            <form action="acceptRequest" method="post">
                            <div class="card request-card">
                                <div class="card-header">
                                    <h5 class="card-title">${r.title}</h5>
                                </div>
                                <div class="card-body">
                                    <p class="card-text">Deadline Date: ${r.deadlineDate}</p>
                                    <p class="card-text">Deadline Hour: ${r.deadlineHour}</p>
                                    <p class="card-text">Content: ${r.content}.</p>
                                    <p class="card-text">Skill: 

                                        <c:forEach var="rs" items="${listRS}" varStatus="dem">

                                            <c:if test="${r.getId() == rs.getRequest().getId()}">
                                                <c:forEach var="s" items="${listS}">
                                                    <c:if test="${rs.getSkill().getId() == s.id}">     
                                                        ${s.name},
                                                    </c:if>
                                                </c:forEach> 
                                            </c:if>
                                        </c:forEach>


                                    </p>
                                    <p class="card-text">Status:
                                        <select name="status">
                                            <c:forEach var="o" items="${listSM}">
                                                <option  value="${o.id}" 
                                                         <c:if test="${o.id == r.getStatus().id}">
                                                             selected
                                                         </c:if>
                                                         >${o.name}</option>
                                            </c:forEach>
                                        </select>
                                    </p> 
                                    <input type="hidden" name="rid" value="${r.id}">
                                    <button type="submit" class="btn btn-success btn-action">Update</button>

                                </div>
                            </div>
                           </form>
                        </div>
                       
                    </c:forEach>
                </c:if>

            </div>
        </div>

        <!-- Add Bootstrap JS (optional) -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    </body>
</html>