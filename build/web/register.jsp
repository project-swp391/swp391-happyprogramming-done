

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <title>Register</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/register.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="container">
            <div class="d-flex justify-content-center h-100">


                <div class="card">

                    <div class="card-header" style="padding:0">
                        <h3>Sign Up<a style="text-decoration: none;
                                      margin-left: 70px;" href="home.jsp"><img style="width: 80px; " src="./images/R-removebg-preview.png" alt="alt"/></a></h3>
                        <div class="d-flex justify-content-end social_icon">
                            <span><i class="fa fa-facebook-square" aria-hidden="true"></i></span>
                            <span><i class="fa fa-google-plus-square" aria-hidden="true"></i></span>

                        </div>
                    </div>
                    <div class="card-body" style=" margin-top: 20px;
                         padding: 0px 10px;
                         height: 609px;">
                        <form action="registermail" method="post">

                            <!-- username -->
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="Username" name="username">
                                <p class="text-danger">${mess}</p>

                            </div>
                            <!-- password -->
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-key" aria-hidden="true"></i></span>
                                </div>
                                <input type="password" class="form-control" placeholder="Password" name="password">
                                <p class="text-danger">${mess1}</p>
                            </div>
                            <!-- repass -->
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-key" aria-hidden="true"></i></span>
                                </div>
                                <input name="repassword" type="password" class="form-control" placeholder="Re_password" autocomplete="current-password">
                                <p class="text-danger">${mess2}</p>
                            </div>
                            <!-- full_name -->
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="Fullname" name="fullname">

                            </div>
                            <!-- phone -->
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-phone-square" aria-hidden="true"></i></span>
                                </div>
                                <input name="phone" type="tel" class="form-control" placeholder="PhoneNumber" autocomplete="phonenumber">
                                <p class="text-danger">${mess3}</p>
                            </div>
                            <!-- date_of_birth -->
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                                <input name="dob" type="date" class="form-control" placeholder="DateofBirth" autocomplete="dateofbirth">
                                <p class="text-danger">${mess5}</p>
                            </div>
                            <!-- email -->
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                                </div>
                                <input name="email" type="email" class="form-control" placeholder="Email" autocomplete="email">
                                <p class="text-danger">${mess4}</p>
                            </div>
                            <div class="input-group form-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-home" style="font-size:24px" aria-hidden="true"></i></span>
                                </div>
                                <input name="address" type="address" class="form-control" placeholder="Address" autocomplete="Address">
                            </div>
                            <div class="input-group-prepend" style="margin-bottom: 10px;" >
                                <span class="input-group-text"><i class="fa fa-intersex" style="font-size:24px" aria-hidden="true"></i></span>
                                <select name="gender"  >
                                    <option selected value="0" >Male</option>
                                    <option value="1" >Female</option>
                                </select>
                            </div>
  
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user" style="font-size:24px" aria-hidden="true"></i></span>
                                <select name="role"  >
                                    <option  value="2" >Mentor</option>
                                    <option selected="" value="3" >Mentee</option>
                                </select>
                            </div>
                            <!-- sign up -->
                            <div class="form-group" style="margin-top: 10px;">
                                <input type="submit" value="Signup" class="btn float-right btn-warning signup_btn signup">
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-center links">
                            Don't have an account?<a href="login.jsp">Login</a>
                        </div>
                        <div class="d-flex justify-content-center">
                            <a href="forgotpass.html">Forgot your password?</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
