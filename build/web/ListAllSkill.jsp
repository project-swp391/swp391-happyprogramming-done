<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Manager Skill</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    </head>
    <style>
        body {
            overflow: hidden;
            padding: 15px;
            font-family: Arial, sans-serif;
            background-color: #f8f8f8;
            background-image: url('https://haycafe.vn/wp-content/uploads/2021/12/Hinh-nen-trang-den-dep-nhat-2022.jpg ');
        }

        .menu {
            height: 100vh;
            background-color: #545b62;
            padding: 20px;
        }
        .menu .list-group-item {
            transition: background-color 0.3s;
        }
        .avatar {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-bottom: 20px;
        }

        .avatar-img {
            width: 150px;
            height: 150px;
            border-radius: 50%;
        }

        .username {
            margin-top: 10px;
            font-weight: bold;
            color:white;
        }


        .menu .list-group-item:hover {
            background-color: #e9ecef;
        }

        .menu .list-group-item.active {
            background-color: #007bff;
            color: blue;
        }


        .content {
            height: 100vh;
            overflow-y: scroll;
            padding: 20px;
        }

        .scrollable-content {
            height: 100%;
        }
        .table{
            padding-bottom: 45px;
        }
        .empty-image {
            width: 200px;
            height: 200px;

            background-size: cover;
            background-position: center;
            margin-bottom: 20px;
        }

        .empty-page h2 {
            font-size: 24px;
            margin-bottom: 10px;
        }

        .empty-page p {
            font-size: 16px;
            color: #777;
        }
        .list-group li {
            padding: 0;
        }


    </style>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 menu">
                    <div class="avatar">
                        <img src="https://antimatter.vn/wp-content/uploads/2022/10/hinh-anh-gai-xinh-de-thuong.jpg" alt="Avatar" class="avatar-img">
                        <div class="username">${sessionScope.acc.user_name}</div>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item ">
                            <a class="nav-link " href="home1.jsp">
                                <i class="fas fa-home"></i> Trang chủ
                            </a>
                        </li>
                       
                        <li class="nav-item " >
                            <a class="list-group-item" href="StatisticOfAllMentee" >
                                <i class="fas fa-plus-square"></i> Statistic Of All Mentee
                            </a>
                        </li>
                        <li class="list-group-item " >
                            <a class="nav-link" href="createskill.jsp" >
                                <i class="fas fa-plus-square"></i> Create Skill
                            </a>
                        </li>
                        <li class="list-group-item ">
                            <a class="nav-link " href="list-all-mentor">
                                <i class="fas fa-users "></i> Manager mentor
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a class="nav-link text-white" href="listskill">
                                <i class="fas fa-book"></i> Manager skills
                            </a>
                        </li>
                        <li class="list-group-item ">
                            <a class="nav-link " href="list_register_mentor">
                                <i class="fas fa-book"></i> List register mentor
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-cogs"></i> Cài đặt
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9 content">
                    <div class="scrollable-content">
                        <div class="table table-striped" >
                            <table class="table" style="box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);">
                                <a href="createskill.jsp" class="btn btn-success" style="margin-bottom: 15px;margin-left: 30px ;float: right;" >+ Add New Skill</a><br><!-- comment -->
                                <thead class="thead-dark">

                                    <tr>
                                        <th scope="col">STT</th>
                                        <th scope="col">ID</th>
                                        <th scope="col">Name of Skill</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test="${ListS.size() == 0}">
                                    <div class="empty-page">
                                        <div class="empty-image"></div>
                                        <h1>Trang rỗng</h1>
                                        <p>Không có kỹ năng nào hiển thị.</p>
                                    </div>
                                </c:if>
                                <c:if test="${ListS.size() != 0}">
                                    <c:forEach var="o" items="${ListS}" varStatus="loop" begin="1" end="${ListS.size()}">
                                        <tr>
                                            <th scope="row"><c:out value="${loop.index}"/></th>
                                            <td>${o.id}</td>
                                            <td>${o.name}</td>
                                            <c:if test="${o.status == 'active'}">
                                                <td>
                                                    <button onclick="window.location.href = 'update-status-skill?sid=${o.id}&status=${o.status}'" class="btn btn-success">Enabled</button>
                                                </td>
                                            </c:if>
                                            <c:if test="${o.status == 'inactive'}">
                                                <td>
                                                    <button onclick="window.location.href = 'update-status-skill?sid=${o.id}&status=${o.status}'" class="btn btn-secondary">Disabled</button>
                                                </td>
                                            </c:if>
                                            <td><a href="updateSkill?id=${o.id}" class="btn btn-primary">Update Skill</a></td>
                                        </tr>
                                    </c:forEach>
                                </c:if>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>






