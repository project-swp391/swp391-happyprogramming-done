
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <title>Update CV of Mentor</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>

    <body>
        <style>
            ul.list.menuList {
                list-style: none;
            }
            ul.list.menuList li {
                padding: 0;
            }

            ul.list.menuList li:before {
                content: "";
                display: none;
            }

            ul.list.menuList li {
                list-style: none;
            }

            #avatarThumb {
                position: relative;
            }

            #avatarThumb .btn-change-avatar {
                position: absolute;
                top: 0;
                left: 0;
                background: rgba(0, 0, 0, 0.4);
                color: #ccc;
                border-radius: 0;
            }

            #avatarThumb:hover .btn-change-avatar {

                background: rgba(0, 0, 0, 0.8);
                color: white;
            }
            ul li a{
                text-decoration: none ;
                font-family: serif;
                font-size: 18px;
                color: black;
            }
            .regis-mentor{
                text-align: center;
            }
            label{
                font-family: serif;
                font-size: 17px;
            }
        </style>
        <jsp:include page="menu.jsp" ></jsp:include>
            <section ng-app="mainApp" id="mainAppContent" class="ng-scope" >
                <div class="container ng-scope" ng-controller="mainCtroller" >
                    <div class="section-content" style="padding-top: 165px;">
                        <div class="row">
                            <div class="col-sx-12 col-sm-3 col-md-3" style="background-color: #f8f9fa;
                                 height: 1313px;
                                 padding-top: 20px;
                                 margin-right: auto;
                                 box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);">

                            <c:if test="${cv.avatar != null }">
                                <div class="doctor-thumb" id="avatarThumb" style="background-color:#f8f9fa;padding-left: 15px;padding-top: 10px;margin-bottom: 20px;" >
                                    <img  src="${cv.avatar}" alt="" style="width:95%; height: 230px; object-fit: cover;border-radius: 70%;" >
                                    <div class="avatarEditor croppie-container" id="avatarThumbCroppie" style="display:none;">
                                        <div class="cr-boundary" aria-dropeffect="none" style="width: 263px; height: 263px;">
                                            <img class="cr-image" alt="preview" aria-grabbed="false">
                                            <div class="cr-viewport cr-vp-square" tabindex="0"
                                                 style="width: 263px; height: 263px;"></div>
                                            <div class="cr-overlay"></div>
                                        </div>
                                        <div class="cr-slider-wrap"><input class="cr-slider" type="range" step="0.0001"
                                                                           aria-label="zoom"></div>
                                    </div>

                                    <form action="imagementor" method="post" enctype="multipart/form-data">
                                        <div class="text-center btnSave mb-10" style="display: grid;">
                                            <input type="file"  name="fileimage" style="width: 95% ;">
                                            <input class="btn btn-xs btn-dark" type="submit" value="cập nhật" style="width: 95% ;">


                                        </div>
                                    </form>  
                                    <hr/>
                                </div>
                            </c:if>
                            <c:if test="${cv.avatar == null }">
                                <div class="doctor-thumb" id="avatarThumb" style="background-color: #f8f9fa;padding-left: 15px;padding-top: 10px;margin-bottom: 20px;" >
                                    <img  src="https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg" alt="anh khong ten" style="width:95%;height: auto;" >
                                    <div class="avatarEditor croppie-container" id="avatarThumbCroppie" style="display:none;">
                                        <div class="cr-boundary" aria-dropeffect="none" style="width: 263px; height: 263px;">
                                            <img class="cr-image" alt="preview" aria-grabbed="false">
                                            <div class="cr-viewport cr-vp-square" tabindex="0"
                                                 style="width: 263px; height: 263px;"></div>
                                            <div class="cr-overlay"></div>
                                        </div>
                                        <div class="cr-slider-wrap"><input class="cr-slider" type="range" step="0.0001"
                                                                           aria-label="zoom"></div>
                                    </div>

                                    <form action="imagementor" method="post" enctype="multipart/form-data">
                                        <div class="text-center btnSave mb-10" style="display: grid;">
                                            <input type="file"  name="fileimage" style="width: 95% ;">
                                            <input class="btn btn-xs btn-dark" type="submit" value="cập nhật" style="width: 95% ;">


                                        </div>
                                    </form>  
                                    <hr/>
                                </div>
                            </c:if>

                            <div class="info p-20 bg-black-333" style="background-color: #f8f9fa;">

                                <ul class="list angle-double-right m-0">
                                    <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Username: </strong> <b
                                            style="color:green;">${sessionScope.acc.user_name}</b></li>
                                </ul>
                                <ul class="list list-divider list-border menuList" style="padding: 20px;">

                                    <li class="list-group-item m-0">
                                        <a class="nav-link " href="editprofile">
                                            <i class="fas fa-user"></i> Thông tin cá nhân
                                        </a>
                                    </li>
                                    <c:if test="${sessionScope.acc.role_id == 3}">
                                        <li class="list-group-item ">
                                            <a class="nav-link " href="listRequest">
                                                <i class="fas fa-list-alt"></i> Danh sách yêu cầu 
                                            </a>
                                        </li>
                                        <li class="list-group-item ">
                                            <a class="nav-link " href="staticRequest">
                                                <i class="fas fa-list-alt"></i> Thống kê yêu cầu
                                            </a>
                                        </li>
                                    </c:if>
                                    <c:if test="${sessionScope.acc.role_id == 2 && (sessionScope.status == 1 || sessionScope.status == 2)}">
                                        <li class="list-group-item active">
                                            <a class="nav-link " href="updatementor">
                                                <i class="far fa-user-circle"></i> Cập nhật CV
                                            </a>
                                        </li>
                                        <li class="list-group-item ">
                                            <a class="nav-link " href="listinviting">
                                                <i class="fas fa-list-alt"></i> Danh sách lời mời
                                            </a>
                                        </li>
                                    </c:if>
                                    <li class="list-group-item m-0">
                                        <a class="nav-link " href="changepass.jsp">
                                            <i class="fas fa-address-card"></i> Đổi mật khẩu
                                        </a>
                                    </li>

                                    <li class="list-group-item ">
                                        <a class="nav-link " href="logout">
                                            <i class="fas fa-sign-out-alt"></i> Đăng xuất
                                        </a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-9" style="
                             margin-bottom: 45px;">
                            <div>
                                <form method="Post" action="updatementor" id="myform" class="ng-pristine ng-valid ng-valid-email" style="box-shadow: 0 2px 5px rgba(0, 0, 0.1, 0.2);
                                      padding:25px 25px ; margin-bottom:  20px;background-color: #f8f9fa; ">
                                    <h3 class="line-bottom" style="font-family:serif; font-weight: bold; text-align: center;margin-bottom: 20px;">Thông tin CV</h3>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>Fullname <i class="fas fa-star-of-life text-danger"></i></label>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-not-empty"
                                                   type="text" required="" name="fullname" value="${cv.fullname}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Phone <i class="fas fa-star-of-life text-danger"></i></label>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                                   type="phone" required="" id="phone" name="phone" value="${cv.phone}" >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-5">
                                            <label>Email <i class="fas fa-star-of-life text-danger"></i></label>
                                            <input
                                                class="form-control ng-pristine ng-valid ng-empty ng-valid-email ng-touched"
                                                type="email" id="email" name="email" value="${sessionScope.acc.email}" required="">
                                        </div>


                                        <div class="form-group col-md-3">
                                            <label>Date of Birth <i class="fas fa-star-of-life text-danger"></i></label><br>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                                   type="date" name="dob" max="${now}" value="${cv.dob}" >
                                        </div>
                                        <div class="form-group col-md-4" >
                                            <label>Gender</label><br>
                                            <c:if test="${cv.sex == false}">
                                                <select name="gender" style="height: 38px;" >
                                                    <option selected="" value="0" >Male</option>
                                                    <option value="1" >Female</option>
                                                </select>
                                            </c:if>
                                            <c:if test="${cv.sex == true}">
                                                <select name="gender" style="height: 38px;" >
                                                    <option  value="0" >Male</option>
                                                    <option selected="" value="1" >Female</option>
                                                </select>
                                            </c:if>

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Address <i class="fas fa-star-of-life text-danger"></i></label>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                                   type="text" required="" value="${cv.address}" name="address">
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label> Profession <i class="fas fa-star-of-life text-danger"></i>
                                            </label>
                                            <select class="form-group col-md-12" name="job" style="height: 40px;" >
                                                <option  value="0"></option>
                                                <c:choose>
                                                    <c:when test="${cv.job eq 'Sinh viên'}">
                                                        <option value="1" selected>-Sinh viên-</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="1">-Sinh viên-</option>
                                                    </c:otherwise>
                                                </c:choose>
                                                <c:choose>
                                                    <c:when test="${cv.job eq 'Giáo viên'}">
                                                        <option value="2" selected>-Giáo viên-</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="2">-Giáo viên-</option>
                                                    </c:otherwise>
                                                </c:choose>
                                                <c:choose>
                                                    <c:when test="${cv.job eq 'Đã tốt nghiệp'}">
                                                        <option value="3" selected>-Đã tốt nghiệp-</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="3">-Đã tốt nghiệp-</option>
                                                    </c:otherwise>
                                                </c:choose>

                                            </select>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Profession Introduction </label>
                                            <textarea name="intro_job"
                                                      class="form-control ng-pristine ng-valid ng-empty ng-touched" cols="20"
                                                      rows="3"  >${cv.introJob}</textarea>
                                        </div>
                                    </div>
                                    <div class="row" >
                                        <div class="form-group col-md-12" >
                                            <label>Skill Training <i class="fas fa-star-of-life text-danger"></i> </label><br>
                                            <c:forEach var="o" items="${List}">
                                                <div class="form-group col-md-3" style="display: inline-table">
                                                    <input type="checkbox" name="skill" value="${o.id}" 
                                                           <c:forEach var="s" items="${ListS}">
                                                               <c:if test="${o.id == s.id}">
                                                                   checked
                                                               </c:if>
                                                           </c:forEach> >
                                                    ${o.name}
                                                </div>
                                            </c:forEach>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Service Description </label>
                                            <textarea name="service"
                                                      class="form-control ng-pristine ng-valid ng-empty ng-touched" cols="20"
                                                      rows="2"  >${cv.service}</textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Achievement Description </label>
                                            <textarea name="achievement"
                                                      class="form-control ng-pristine ng-valid ng-empty ng-touched" cols="20"
                                                      rows="2"  >${cv.achievement}</textarea>
                                        </div>
                                    </div>
                                    <div class="row" >
                                        <div class="form-group col-md-12" >
                                            <label>Framework Training <i class="fas fa-star-of-life text-danger"></i> </label><br>
                                            <c:forEach var="o" items="${ListF}">
                                                <div class="form-group col-md-3" style="display: inline-table">
                                                    <input type="checkbox" name="famework" value="${o.fameID}" 
                                                           <c:forEach var="s" items="${Listf}">
                                                               <c:if test="${o.fameID == s.fameID}">
                                                                   checked
                                                               </c:if>
                                                           </c:forEach> >
                                                    ${o.fameName}
                                                </div>
                                            </c:forEach>
                                        </div>

                                    </div>

                                    <div class="regis-mentor" style="margin-bottom: 15px;">
                                                                           
                                        <button type="submit" onclick="return validateForm()" class="btn btn-success btn-lg mt-15" >OK</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script>
            function validateForm() {
                var framecheck = document.getElementsByName("famework");
                var skillcheck = document.getElementsByName("skill");
                var checkedf = false;
                var checkeds = false;
                var phoneNumber = document.getElementById("phone").value;
                var phoneNumberPattern = /^\d{10}$/; // Biểu thức chính quy kiểm tra dạng 10 chữ số
                var email = document.getElementById("email").value;
                var emailPattern = /^[a-zA-Z][^\s@]+@[^\s@]+\.[^\s@]+$/; // Biểu thức chính quy kiểm tra định dạng email

                if (!emailPattern.test(email)) {
                    alert("Vui lòng nhập email đúng định dạng.");
                    return false; // Ngăn chặn việc gửi form
                }

                if (!phoneNumberPattern.test(phoneNumber)) {
                    alert("Vui lòng nhập số điện thoại cho đúng.");
                    return false; // Ngăn chặn việc gửi form
                }
                for (var i = 0; i < framecheck.length; i++) {
                    if (framecheck[i].checked) {
                        checkedf = true;
                        break;
                    }
                }
                for (var i = 0; i < skillcheck.length; i++) {
                    if (skillcheck[i].checked) {
                        checkeds = true;
                        break;
                    }
                }
                if (!checkedf && !checkeds) {
                    alert("Vui lòng chọn Framework và Skill.");
                    return false; // Ngăn chặn việc gửi form
                }

                if (!checkedf) {
                    alert("Vui lòng chọn ít nhất một Framework.");
                    return false; // Ngăn chặn việc gửi form
                }
                if (!checkeds) {
                    alert("Vui lòng chọn ít nhất một Skill.");
                    return false; // Ngăn chặn việc gửi form
                }

                return true; // Cho phép gửi form
            }
        </script>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
        <jsp:include page="footer.jsp" ></jsp:include>
    </body>

</html>
