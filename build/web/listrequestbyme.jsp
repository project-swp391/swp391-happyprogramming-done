

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="zxx" class="no-js">

    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="img/fav.png" />
        <!-- Author Meta -->
        <meta name="author" content="colorlib" />
        <!-- Meta Description -->
        <meta name="description" content="" />
        <!-- Meta Keyword -->
        <meta name="keywords" content="" />
        <!-- meta character set -->
        <meta charset="UTF-8" />
        <!-- Site Title -->
        <title>Happy Programming</title>

        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:900|Roboto:400,400i,500,700" rel="stylesheet" />
        <!--
            CSS
            =============================================
        -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css" />

        <link rel="stylesheet" href="https://unpkg.com/@webpixels/css/dist/index.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <style>
            /* Login v4 */
            body {
                background-image: url("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQCHlZo0FxUN3WuSp1Z_eM1OzoJh4qCdIzgCWaNmfjXTLrIA_p8LQCSa8k18GJw2o6n2y0&usqp=CAU");
            }
            .signup_v4 .social-area {
                text-align: center;
                padding-top: 14px;
            }

            .signup_v4 .social-area .title {
                font-size: 20px;
                text-transform: uppercase;
                font-weight: 600;
                display: inline-block;
                color: #007bff;
                position: relative;
            }

            .signup_v4 .social-area .text {
                font-size: 17px;
                font-weight: 400;
                color: #143250;
            }

            .signup_v4 .social-area .title::before {
                position: absolute;
                content: '';
                width: 40px;
                height: 1px;
                background: rgba(0, 0, 0, .2);
                top: 50%;
                -webkit-transform: translateY(-50%);
                -ms-transform: translateY(-50%);
                transform: translateY(-50%);
                left: 100%;
                margin-left: 7px;
            }

            .signup_v4 .social-area .title::after {
                position: absolute;
                content: '';
                width: 40px;
                height: 1px;
                background: rgba(0, 0, 0, .2);
                top: 50%;
                -webkit-transform: translateY(-50%);
                -ms-transform: translateY(-50%);
                transform: translateY(-50%);
                right: 100%;
                margin-right: 7px;
            }

            .signup_v4 ul.social-links {
                padding: 0;
                margin: 0;
            }

            .signup_v4 .social-area .social-links li {
                display: inline-block;
            }

            .signup_v4 .social-area .social-links li a i {
                width: 50px;
                height: 50px;
                border-radius: 50%;
                line-height: 50px;
                display: inline-block;
                color: #fff;
                margin: 0 5px;
                -webkit-box-shadow: 0 5px 10px rgb(0 0 0 / 15%);
                box-shadow: 0 5px 10px rgb(0 0 0 / 15%);
            }

            .signup_v4 .social-area .social-links li a i.fa-facebook-f {
                background: #0069f7;
            }

            .signup_v4 .social-area .social-links li a i.fa-google-plus-g {
                background: #d1062c;
            }

            .signup_v4 .nav-tabs .nav-link.active {
                background: #007bff;
            }

            .signup_v4 .nav-tabs .nav-link {
                background: #143250;
            }

            .signup_v4 .nav-tabs .nav-link {
                border: 0;
                margin: 0;
                padding: 10px 0;
                text-align: center;
                border-radius: 0;
                color: #fff;
            }

            .signup_v4 .nav-tabs li.nav-item {
                width: 50%;
            }

            .signup_v4 .card-body {
                padding: 0px;
            }

            .signup_v4 .card-body .tab-content {
                padding: 0 1.25rem 1.75em;
            }
        </style>
    </head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"/>
</head>

<body>
    <section class="feature-area" style="margin-top: 1.5rem">

        <button style="background-color: greenyellow; border-radius: 5px"><a style="text-decoration: none; color: black" href="home1.jsp">Home</a></button>

        <div class="p-10 ">
            <div class="container">
                <div class="card">
                    <div class="card-header" style="text-align: center;font-family: Roboto;font-weight: bold">
                        <h6>List request by me</h6>
                    </div>
                    <div class="table-responsive">
                        <form action="listRequestOfMentee"  > 
                            <table class="table ">
                                <thead class="thead-dark">
                                    <tr style="text-align: center">
                                        <th scope="col">ID</th>
                                        <th scope="col">From</th>
                                        <th scope="col">To</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">desciption of request</th>
                                        <th scope="col">skill</th>
                                        <th scope="col">Date of Request</th>
                                        <th scope="col">Deadline Date</th>
                                        <th scope="col">Deadline Hour</th>
                                        <th  scope="col">Status</th>
                                        <th  scope="col">Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${listReq}" var="lr">
                                        <tr style="text-align: center">
                                            <td data-label="">${lr.id}</td>
                                            <td data-label="">${lr.from.user_name}</td>
                                            <td data-label="">
                                                ${lr.to.user_name}
                                            </td>
                                            <td data-label="">${lr.title}</td>
                                            <td data-label="">${lr.content}</td>
                                            <td data-label="Skill">
                                                <select>
                                                    <c:forEach items="${lr.skill}" var="s">
                                                        <option>${s.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </td>
                                            <td data-label="">${lr.createDate}</td>
                                            <td data-label="">${lr.deadlineDate}</td>
                                            <td data-label="">${lr.deadlineHour}</td>
                                            <td data-label=""> <c:if test="${lr.status.id == 1}" >Waiting</c:if>
                                                <c:if test="${lr.status.id== 2}" >Accept</c:if>
                                                <c:if test="${lr.status.id == 3}" >Complete</c:if>
                                                <c:if test="${lr.status.id == 4}" >Cancel</c:if>
                                                </td>
                                            <c:if test="${lr.status.id== 1}" > 
                                                <td data-label="" class="button-update-delete"><a class="button-update" href="updateRequest?rid=${lr.id}">Update</a>
                                                    <a class="button-delete" href="deleteRequest?reqID=${lr.id}" >Delete</a></td> 
                                                </c:if>
                                                <c:if test="${lr.status.id== 2}" > 
                                                <td data-label="">Can't edit</td> 
                                            </c:if>
                                            <c:if test="${lr.status.id== 3}" > 
                                                <td data-label="">Can't edit</td> 
                                            </c:if>
                                            <c:if test="${lr.status.id== 4}" > 
                                                <td data-label="">Can't edit</td> 
                                            </c:if>
                                    <style>
                                        .button-update-delete .button-update{
                                            text-decoration: none;
                                            color: black;
                                            background-color: greenyellow;
                                            border: 1px solid gray;
                                            border-radius: 5px
                                        }
                                        .button-update-delete .button-delete{
                                            text-decoration: none;
                                            color: black;
                                            background-color: red;
                                            border: 1px solid gray;
                                            border-radius: 5px
                                        }
                                    </style>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            <!--                            <table class="table table-hover table-nowrap">
                                                            <thead class="table-light">
                                                                <tr>
                                                                    <th scope="col">ID</th>
                                                                    <th scope="col">From</th>
                                                                    <th scope="col">To</th>
                                                                    <th scope="col">Title</th>
                                                                    <th scope="col">desciption of request</th>
                                                                    <th scope="col">Date of Request</th>
                                                                    <th scope="col">Deadline Date</th>
                                                                    <th scope="col">Deadline Hour</th>
                                                                    <th  scope="col">Status</th>
                            
                                                                    <th></th>
                            
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                            <c:forEach items="${listReq}" var="lr">
                                <tr>
                                    <td data-label="">${lr.id}</td>
                                    <td data-label="">${lr.from.user_name}</td>
                                    <td data-label="">
                                ${lr.to.user_name}
                            </td>
                            <td data-label="">${lr.title}</td>
                            <td data-label="">${lr.content}</td>
                            <td data-label="">${lr.createDate}</td>
                            <td data-label="">${lr.deadlineDate}</td>
                            <td data-label="">${lr.deadlineHour}</td>
                            <td data-label=""> <c:if test="${lr.status.id == 1}" >Waiting</c:if>
                                <c:if test="${lr.status.id== 2}" >Accept</c:if>
                                <c:if test="${lr.status.id == 3}" >Complete</c:if>
                                <c:if test="${lr.status.id == 4}" >Cancel</c:if>
                                </td>
                                <c:if test="${lr.status.id== 1}" > 
                                    <td data-label=""><a href="updateRequest?rid=${lr.id}">Update</a>
                                        <a href="deleteRequest?reqID=${lr.id}" >Delete</a></td> 
                                </c:if>
                        </tr>
                            </c:forEach>
                        </tbody>
                    </table>-->
                        </form>
                    </div>
                </div>
            </div>

        </div>

    </section>

    <script src="js/vendor/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
    crossorigin="anonymous"></script>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>

</body>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    function validateInput() {
        var input = document.getElementById("myInput");
        var value = parseInt(input.value);

        if (isNaN(value) || value <= 0) {
            alert("Please enter a positive number.");
            input.value = ""; // Clear the input field
            return false;
        }

        // Proceed with further actions or submit the form
    }
</script>-->

</html>



