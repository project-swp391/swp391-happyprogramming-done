
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <title>Update CV of Mentor</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"

              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>

    <body>
        <jsp:include page="menu.jsp"></jsp:include>
            <style>
                section{
                    background-image: url('https://haycafe.vn/wp-content/uploads/2022/05/Background-trang-xam.jpg ');
                   padding: 20px 0;
                }
                
                
                ul.list.menuList {
                    list-style: none;
                }

                ul.list.menuList li:before {
                    content: "";
                    display: none;
                }

                ul.list.menuList li {
                    list-style: none;
                }

                #avatarThumb {
                    position: relative;
                }

                #avatarThumb .btn-change-avatar {
                    position: absolute;
                    top: 0;
                    left: 0;
                    background: rgba(0, 0, 0, 0.4);
                    color: #ccc;
                    border-radius: 0;
                }

                #avatarThumb:hover .btn-change-avatar {

                    background: rgba(0, 0, 0, 0.8);
                    color: white;

                }
                .regis-mentor{
                    text-align: center;
                }
                label{
                    font-weight: bold;
                    font-family: serif;
                    font-size: 17px;
                }
            </style>

            <section  class="ng-scope" style="padding-top: 135px;">
                <div class="container ng-scope">
                    <div class="section-content">
                        <div class="row">
                            
                            <div class="col-xs-12 col-sm-9 col-md-9" style="box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);margin-left: 150px;">
                            <div>

                                <form name="registmentor" method="post" class="ng-pristine ng-valid ng-valid-email">
                                    <h3 class="line-bottom mt-6" style="text-align: center; font-weight: bold;margin: 20px 0;">Create CV</h3>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>Fullname</label>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-not-empty"
                                                   type="text" name="fullname" required="">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Phone</label>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                                   type="phone" name="phone" required >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-5">
                                            <label>Email</label>
                                            <input
                                                class="form-control ng-pristine ng-valid ng-empty ng-valid-email ng-touched"
                                                type="email" name="email" required value="${sessionScope.acc.email}" >
                                        </div>


                                        <div class="form-group col-md-3">
                                            <label>Date of birth</label><br>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                                   type="date" required name="dob"  >
                                        </div>
                                        <div class="form-group col-md-4" >
                                            <label>Gender</label><br>
                                        
                                                <select required name="gender" style="height: 38px;" >
                                                    <option selected=""  value="0" >Male</option>
                                                    <option  value="1" >Female</option>
                                                </select>
                                           

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Address</label>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                                   type="text" required  name="address">
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label> Profession:
                                                <i class="fas fa-star-of-life text-danger"></i>
                                            </label>
                                            <select required class="form-group col-md-12" name="job" style="height: 40px;" >

                                                <option  value="0"></option>
                                                <option  value="1"  >- Student -</option>
                                                <option  value="2" selected="" >- Teacher -</option>
                                                <option  value="3"  >- Graduated -</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Profession Introduction: </label>
                                            <textarea name="intro_job"
                                                      class="form-control ng-pristine ng-valid ng-empty ng-touched" cols="20"
                                                      rows="3"  ></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Skill Training:
                                                <i class="fas fa-star-of-life text-danger"></i>
                                            </label><br>
                                            <c:forEach var="o" items="${ListS}">
                                                <div class="form-group col-md-3" style="display: inline-table">
                                                    <input  type="checkbox"  name="skill" value="${o.id}"> ${o.name} <br>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Service Description: </label>
                                            <textarea name="service"
                                                      class="form-control ng-pristine ng-valid ng-empty ng-touched" cols="20"
                                                      rows="3"  ></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Achievement Description: </label>
                                            <textarea name="achievement"
                                                      class="form-control ng-pristine ng-valid ng-empty ng-touched" cols="20"
                                                      rows="3"  ></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Famework Training:
                                                <i class="fas fa-star-of-life text-danger"></i>
                                            </label><br>
                                            <c:forEach var="o" items="${ListF}">
                                                <div class="form-group col-md-3" style="display: inline-table">
                                                    <input  type="checkbox"  name="famework" value="${o.fameID}"> ${o.fameName} <br>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                    <div class="regis-mentor" style="margin-bottom: 15px;">
                                        <input class="btn btn-success btn-lg mt-15" type="submit" value="Register Mentor" />
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </section>


        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
        <jsp:include page="footer.jsp" ></jsp:include>
    </body>

</html>
