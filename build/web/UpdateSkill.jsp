

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Update Skill</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css">
        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: #f8f8f8;
                background-image: url('https://phunugioi.com/wp-content/uploads/2020/08/hinh-nen-mau-trang.jpg');
            }

            .container {
                max-width: 500px;
                margin: 0 auto;
                padding: 20px;
                background-color: #fff;
                border-radius: 5px;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
            }

            h1 {
                text-align: center;
                margin-bottom: 20px;
            }

            label {
                display: block;
                font-weight: bold;
                margin-bottom: 5px;
            }

            input[type="text"] {
                width: 100%;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            select {
                width: 100%;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 4px;
                box-sizing: border-box;
            }

            .btn-custom {
                display: block;
                width: 100%;
                padding: 10px;
                background-color: #4CAF50;
                color: #fff;
                border: none;
                border-radius: 4px;
                cursor: pointer;
                transition: background-color 0.3s ease;
            }

            .btn-custom:hover {
                background-color: #45a049;
            }
            .slideshow-container {
                max-width: 100%;
                position: relative;
                margin: auto;
            }

            .slide {
                display: none;
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
            }

            .slide img {
                width: 100%;
                height: auto;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <a href="listskill" class="btn btn-success" style="margin-bottom: 25px;float: left;" >Back</a><br>
            <h1 class="mb-4">Update Skill</h1>
             
            <form action="updateSkillDetail" method="post">
                <div class="mb-2">
                    <label for="skillId" class="form-label">ID</label>
                    <input type="text" class="form-control" name="id"  value="${s.id}" readonly="">
                </div>
                <div class="mb-2">
                    <label for="skillName" class="form-label">Skill Name</label>
                    <input type="text" class="form-control" value="${s.name}" name="name" required>
                </div>
                <div class="mb-2">
                    <label for="skillName" class="form-label">Price</label>
                    <input type="text" class="form-control" value="${s.price}" name="price" required>
                </div>
               
                
                <div class="mb-2">
                    <label for="url" class="form-label">URL Image</label>
                    <input type="text" class="form-control" value="${s.url}"  name="url" required>
                </div>
                <div class="mb-2">
                    <label for="skillName" class="form-label">description</label>
                    <textarea name="description"
                              class="form-control ng-pristine ng-valid ng-empty ng-touched" cols="20"
                              rows="2"  >${s.description}</textarea>
                </div>

                <div class="mb-2">
                    <label for="status" class="form-label">Status</label>
                    <select class="form-select"  name="status">
                        <option value="active" selected>Active</option>
                        <option value="inactive">Inactive</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-custom">Update</button>
            </form>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
    </body>
</html>

