
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <title>Manager Skill</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css" />
        <link rel="stylesheet" href="css/main.css" />
        <link rel="stylesheet" href="https://unpkg.com/@webpixels/css/dist/index.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    </head>
    <style>
        
        body {
            overflow: hidden;
            padding: 15px;
            font-family: Arial, sans-serif;
            background-color: #f8f8f8;
            background-image: url('https://haycafe.vn/wp-content/uploads/2021/12/Hinh-nen-trang-den-dep-nhat-2022.jpg ');
        }
        .avatar {
            display: flex;
            flex-direction: column;
            align-items: center;
            width: 150px;
            height: 188px;
            margin: auto;
            border-radius: 50%;
        }
        .menu {
            height: 100vh;
            background-color: #545b62;
            padding: 20px;
        }
        .menu .list-group-item {
            transition: background-color 0.3s;
        }
        .avatar {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-bottom: 20px;
        }

        .avatar-img {
            width: 150px;
            height: 150px;
            border-radius: 50%;
        }

        .username {
            margin-top: 10px;
            font-weight: bold;
            color:white;
        }


        .menu .list-group-item:hover {
            background-color: #e9ecef;
        }

        .menu .list-group-item.active {
            background-color: #007bff;
            color: blue;
        }


        .content {
            height: 100vh;
            overflow-y: scroll;
            padding: 20px;
        }

        .scrollable-content {
            height: 100%;
        }
        .table{
            padding-bottom: 45px;
        }
       .list-group li {
            padding: 0;
        }
        .empty-image {
            width: 200px;
            height: 200px;

            background-size: cover;
            background-position: center;
            margin-bottom: 20px;
        }

        .empty-page h2 {
            font-size: 24px;
            margin-bottom: 10px;
        }

        .empty-page p {
            font-size: 16px;
            color: #777;
        }
        .avatar-img {
            width: 150px;
            height: 150px;
            border-radius: 50%;
        }
        .nav-link{
            font-size: 15px;
        }
    </style>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 menu">
                    <div class="avatar">
                        <img src="https://antimatter.vn/wp-content/uploads/2022/10/hinh-anh-gai-xinh-de-thuong.jpg" alt="Avatar" class="avatar-img">
                        <div class="username">${sessionScope.acc.user_name}</div>
                    </div>
                    <ul class="list-group" style="width: 100%;">
                        <li class="list-group-item ">
                            <a class="nav-link " href="home1.jsp">
                                <i class="fas fa-home"></i> Trang chủ
                            </a>
                        </li>
                      
                        <li class="list-group-item active " >
                            <a class="nav-link text-white" href="StatisticOfAllMentee" >
                                <i class="fas fa-plus-square"></i> Statistic Of All Mentee
                            </a>
                        </li>
                        <li class="list-group-item " >
                            <a class="nav-link" href="createskill.jsp" >
                                <i class="fas fa-plus-square"></i> Create Skill
                            </a>
                        </li>
                        <li class="list-group-item ">
                            <a class="nav-link " href="list-all-mentor">
                                <i class="fas fa-users "></i> Manager all mentor
                            </a>
                        </li>
                        <li class="list-group-item ">
                            <a class="nav-link" href="listskill">
                                <i class="fas fa-book"></i> Manager skills
                            </a>
                        </li>
                        <li class="list-group-item ">
                            <a class="nav-link " href="list_register_mentor">
                                <i class="fas fa-book"></i> List register mentor
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-cogs"></i> Cài đặt
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9 content">
                    <div class="scrollable-content">
                        <div class="table table-striped" >
                            <table class="table">
                                <tbody>
                                    <tr style="text-align: center">
                                        <td>
                                            <div class=" mt-2 border-right" style="text-align: center">
                                                <div class="box p-2 rounded">
                                                    <span class="fas fa-star text-primary px-1"></span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <div class="text-muted">Total Mentee</div>
                                                <div >

                                                    <b>${totalMentee}</b>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <div class="text-muted">Total hour of all request</div>
                                                <div><b>${totalH}</b></div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <div class="text-muted">Total skill of all request</div>
                                                <div><b>${totalS}</b></div>
                                            </div>
                                        </td>

                                    </tr>
                                </tbody>
                            </table>

                            <table class="table">
                                <thead>
                                    <tr style="text-align: center">
                                        <th>No.</th>
                                        <th>Name</th>
                                        <th>AccountName</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${listMentee}" var="l" varStatus="i">
                                        <tr style="text-align: center">
                                            <td>
                                                <div class=" border-right">
                                                    <div class="box rounded">
                                                        <span class="text-primary font-weight-bold">${(i.index + 1)+(curP-1)*2}</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="flex-column">
                                                    <div class="align-items-center">
                                                        <b class="pl-2">${l.user_name}</b>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class=" flex-column">
                                                    <div><b>${l.email}</b></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>

                            <!--                            <div style="text-align: right" id="page">
                            <c:if test="${curP!=1}"><button onclick="Pagination('${curP-1}', '${totalP}')" class="btn btn-outline-info">Previous</button></c:if>
                            <c:forEach begin="${curP}" end="${totalP}" varStatus="i">
                                <a href="view?id=listMentor&page=${i.index}"   class="btn btn-outline-info  ${curP==i.index? "btn-outline-danger":""}">${i.index}</a>
                                <button  onclick="Pagination('${i.index}', '${totalP+0}')"  class="btn btn-outline-info  ${curP==i.index? "btn-outline-danger":""}">${i.index}</button>
                            </c:forEach>
                            <c:if test="${curP ne totalP}">
                                <button onclick="Pagination('${curP+1}', '${totalP}')" class="btn btn-outline-info">Next</button>
                            </c:if> 
                        </div>-->
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script type="text/javascript">
        function Page(index, totalP) {
            $.ajax({
                url: "/SWP391_HappyProgramming/admin",
                type: "get", //send it through get method
                data: {
                    action: 'StatisticOfAllMentee',
                    page: index,
                    totalP: totalP
                },
                success: function (response) {
                    //Do Something
                    document.getElementById("page").innerHTML = response;

                },
                error: function (xhr) {
                    //Do Something to handle error
                }
            });
        }

        function Pagination(index, totalP) {
            $.ajax({
                url: "StatisticOfAllMentee",
                type: "get", //send it through get method
                data: {
                    action: 'StatisticOfAllMentee',
                    page: index,
                    totalP: totalP
                },
                success: function (response) {
                    //Do Something
                    document.getElementById("data").innerHTML = response;
                    Page(index, totalP);
                },
                error: function (xhr) {
                    //Do Something to handle error
                }
            });
        }
    </script>
</html>







