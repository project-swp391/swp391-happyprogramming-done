<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Manager Mentor</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <link rel="stylesheet" href="./css/listAllMentor.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    </head>
    <style>
        tr td {
            font-family: serif;
            font-weight: bold;
            font-size: 17px;
            }
            .list-group li {
            padding: 0;
        }
        .pagination {
/*            position: fixed;*/
            transform: translateY(100%); 
            padding-bottom: 80px;
/*            padding-bottom: 23px;
            display: flex;
            justify-content: center;
            margin-top: 20px;*/

        }
    </style>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 menu">
                    <div class="avatar">
                        <img src="https://antimatter.vn/wp-content/uploads/2022/10/hinh-anh-gai-xinh-de-thuong.jpg" alt="Avatar" class="avatar-img">
                        <div class="username">${sessionScope.acc.user_name}</div>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a class="nav-link" href="home1.jsp">
                                <i class="fas fa-home"></i> Trang chủ
                            </a>
                        </li>
                      
                        <li class="list-group-item " >
                            <a class="nav-link" href="StatisticOfAllMentee" >
                                <i class="fas fa-plus-square"></i> Statistic Of All Mentee
                            </a>
                        </li>
                        <li class="list-group-item " >
                            <a class="nav-link" href="createskill.jsp" >
                                <i class="fas fa-plus-square"></i> Create Skill
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a class="nav-link text-white" href="list-all-mentor">
                                <i class="fas fa-users "></i> Manager all mentor
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="listskill">
                                <i class="fas fa-book"></i> Manager skills
                            </a>
                        </li>
                        <li class="list-group-item ">
                            <a class="nav-link " href="list_register_mentor">
                                <i class="fas fa-book"></i> List register mentor
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-cogs"></i> Cài đặt
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9 content">
                    <div class="scrollable-content">
                        <form action="searchByName" method="post">
                            <input type="text" placeholder="Search..." name="search" value="${search}">
                            <button type="submit">Search</button>
                        </form>
                        <div class="table table-striped" >
                            <table>
                                <thead class="thead-dark">
                                    <tr>
                                        <th>STT</th>
                                        <th>ID</th>
                                        <th>Fullname</th>
                                        <th>Account</th>
                                        <th>Profession</th>
                                        <th>Accept Requests</th>
                                        <th>Completed</th>
                                        <th>Rate Star</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                <c:if test="${listCV.size() != 0}">
                                    <c:forEach var="cv" items="${listCV}" varStatus="loop">
                                        <tr>
                                            <td>${loop.count}</td>
                                            <td>${cv.mentorId}</td>
                                            <td>${cv.fullname}</td>
                                            <c:forEach var="m" items="${listM}">
                                                <c:if test="${cv.mentorId == m.id}">
                                                    <td>${m.getMentor().getUser_name()}</td>
                                                </c:if>
                                            </c:forEach>
                                            <td>${cv.job}</td>

                                            <c:forEach var="cr" items="${listCR}">
                                                <c:if test="${cv.mentorId == cr.getMentorId()}" >

                                                    <td>${cr.getCount()}</td>
                                                </c:if>
                                            </c:forEach>

                                            <c:forEach var="pr" items="${listPR}">
                                                <c:if test="${cv.mentorId == pr.getMentorId()}">
                                                    <td>${pr.getPercent()}%</td>
                                                </c:if>

                                            </c:forEach>

                                            <c:forEach var="rv" items="${listRV}">
                                                <c:if test="${cv.mentorId == rv.getMentorID()}">
                                                    <td>${rv.getAvg()}<i class="bi bi-star-fill"></td>
                                                </c:if>

                                            </c:forEach>

                                            <c:forEach var="m" items="${listM}">
                                                <c:if test="${cv.mentorId == m.id}">
                                                    <c:if test="${m.getStatus().getId() == 1}">
                                                        <td >
                                                            <button type="button" onclick="window.location.href = 'update-status-mentor?id=${cv.mentorId}&sid=${m.getStatus().getId()}'" style="background-color: green; color: white;">
                                                                <i class="fas fa-check-circle" ></i> Active</button>
                                                        </td>
                                                    </c:if>
                                                    <c:if test="${m.getStatus().getId() == 2}">
                                                        <td>
                                                            <button type="button" onclick=" window.location.href = 'update-status-mentor?id=${cv.mentorId}&sid=${m.getStatus().getId()}'" style="background-color: gray; color: white;">
                                                                <i class='fa-solid fa-circle-xmark'></i> Inactive</button>
                                                        </td>
                                                    </c:if>
                                                </c:if>
                                            </c:forEach>
                                        </tr>

                                    </c:forEach>
                                </c:if>
                                     

                                </tbody>
                            </table>
                        </div>
                         <c:if test="${listCV.size() == 0}">
                                    <div class="col-md-12 empty-page" style="text-align: center;margin-top: 100px;">
                                        <div class="empty-image"></div>
                                        <h2>Danh sách trống</h2>
                                        <p style="margin-bottom: 285px;">Không có mentor nào được tìm thấy</p>
                                    </div>
                                </c:if>  

                        <div class="pagination">
                            <c:if test="${tag  == 1}">
                            <a href="list-all-mentor?index=${tag}"><i class="fas fa-angle-double-left"></i></a>
                            </c:if>
                            <c:if test="${tag  != 1}">
                            <a href="list-all-mentor?index=${tag-1}"><i class="fas fa-angle-double-left"></i></a>
                            </c:if>
                            <c:forEach begin="1" end="${endP}" var="o" >
                            <a href="list-all-mentor?index=${o}" class="page-item ${tag == o?"active":""} "">${o}</a>
                            
                            </c:forEach>
                            <c:if test="${tag  == endP}">
                            <a href="list-all-mentor?index=${tag}"><i class="fas fa-angle-double-right"></i></a>
                            </c:if>
                            <c:if test="${tag  != endP}">
                            <a href="list-all-mentor?index=${tag+1}"><i class="fas fa-angle-double-right"></i></a>
                            </c:if>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>






