/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Context;

import Model.CVMentor;
import Model.MentorSkill;
import Model.Skill;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class MentorSkillDAO extends DBContext {

    // lấy ra các mentor có skill_id là  ?
    public List<CVMentor> getMentorbySkillID(int skill_id) {
        List<CVMentor> l = new ArrayList<>();
        try {
            String sql = "SELECT cvmentor.*, skill.Skill_id from cvmentor\n"
                    + "right join mentorskill ON mentorskill.mentor_id = cvmentor.mentor_id\n"
                    + "join skill ON skill.skill_id = mentorskill.Skill_id\n"
                    + "WHERE skill.skill_id = ?;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, skill_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                CVMentor m = new CVMentor(
                        rs.getInt(2), 
                        rs.getString(3), 
                        rs.getDate(4), 
                        rs.getString(5),
                        rs.getBoolean(6), 
                        rs.getString(7), 
                        rs.getString(8), 
                        rs.getString(9), 
                        rs.getString(10), 
                        rs.getString(11), 
                        rs.getString(12), 
                        rs.getString(13));
                l.add(m);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MentorSkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return l;
    }
    public List<Skill> getSkillMentor(int mentor_id) {
        List<Skill> l = new ArrayList<>();
        try {
            String sql = "SELECT skill.* from Skill\n"
                    + "right join MentorSkill ON Skill.skill_id = MentorSkill.skill_id\n"
                    + "JOIN Mentor ON Mentor.mentor_id = MentorSkill.mentor_id\n"
                    + "WHERE Mentor.mentor_id = ?;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, mentor_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Skill s = new Skill(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getString(2));
                l.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MentorSkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return l;
    }
    public List<MentorSkill> getAllMentorSkill() {
        
        List<MentorSkill> l = new ArrayList<>();
        try {
            String sql = "SELECT * FROM mentorskill;";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                l.add(new MentorSkill(rs.getInt(1), rs.getInt(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MentorSkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return l;
    }
    public void insertMentorSkill(int mentor_id, int skill_id) {
        try {
            String xSql = "INSERT INTO mentorskill (mentor_id,skill_id) VALUES (?,?)";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, mentor_id);
            ps.setInt(2, skill_id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MentorSkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void deleteMentorskill(int mentor_id) {
        
        try {
            String xSql = "delete from mentorskill where mentor_id = ? ;";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, mentor_id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MentorSkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public static void main(String[] args) {
        MentorSkillDAO m = new MentorSkillDAO();
        List<CVMentor> l = m.getMentorbySkillID(1);
        System.out.println(l);
   
    }
}
