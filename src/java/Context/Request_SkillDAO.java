/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Context;

import Model.Request1;
import Model.Request_Skill;
import Model.Skill;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class Request_SkillDAO extends DBContext {

    public void insert(Request_Skill reqSkill) {
        try {
            String sql = "INSERT INTO `request_skill`\n"
                    + "(`request_id`,\n"
                    + "`skill_id`)\n"
                    + "VALUES\n"
                    + "(?,\n"
                    + "?);";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, reqSkill.getRequest().getId());
            stm.setInt(2, reqSkill.getSkill().getId());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RequestMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete(Request1 rq) {
        try {
            String sql = "Delete FROM request_skill where request_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, rq.getId());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public ArrayList<Request_Skill> getAll() {
        RequestDAO r = new RequestDAO();
        SkillDAO skDao = new SkillDAO();
        ArrayList<Request_Skill> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM request_skill ;";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            Request1 request = null;
            Skill skill = null;

            while (rs.next()) {
                request = r.getRequestByID(rs.getInt("request_id"));
                skill = skDao.getSkillByID(rs.getInt("skill_id"));

                list.add(new Request_Skill(request, skill));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    public int getTotalSkillRequest() {
        try {
            String sql = "SELECT count(DISTINCT  skill_id) as 'total' FROM request_skill";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
}
