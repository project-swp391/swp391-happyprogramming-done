/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Context;

import Model.Account;
import Model.CVMentor;
import Model.Mentor;
import Model.MyMentor;
import Model.StatusMentor;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class MyMentorDAO extends DBContext {

    public ArrayList<MyMentor> getAll() {
        AccountDAO aDao = new AccountDAO();
        StatusMentorDAO sDao = new StatusMentorDAO();
        SkillDAO skDao = new SkillDAO();
        ArrayList<MyMentor> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM mentor where status_id = 1";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            Account mentor = null;
            StatusMentor status = null;

            while (rs.next()) {
                mentor = aDao.getAccountByID(rs.getInt("user_id"));
                status = sDao.getStatusByID(rs.getInt("status_id"));

                list.add(new MyMentor(rs.getInt("mentor_id"),
                        mentor, status));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public ArrayList<MyMentor> getMenterbyStatus() {
        AccountDAO aDao = new AccountDAO();
        StatusMentorDAO sDao = new StatusMentorDAO();
        SkillDAO skDao = new SkillDAO();
        ArrayList<MyMentor> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM mentor where status_id = 1";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            Account mentor = null;
            StatusMentor status = null;

            while (rs.next()) {
                mentor = aDao.getAccountByID(rs.getInt("user_id"));
                status = sDao.getStatusByID(rs.getInt("status_id"));

                list.add(new MyMentor(rs.getInt("mentor_id"),
                        mentor, status));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public ArrayList<MyMentor> getAllMentor() {
        AccountDAO aDao = new AccountDAO();
        StatusMentorDAO sDao = new StatusMentorDAO();
        ArrayList<MyMentor> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM mentor ;";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            Account mentor = null;
            StatusMentor status = null;

            while (rs.next()) {
                mentor = aDao.getAccountByID(rs.getInt("user_id"));
                status = sDao.getStatusByID(rs.getInt("status_id"));

                list.add(new MyMentor(rs.getInt("mentor_id"),
                        mentor, status));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public MyMentor getMentorByID(int id) {
        AccountDAO aDao = new AccountDAO();
        StatusMentorDAO sDao = new StatusMentorDAO();
        try {
            String sql = "SELECT * FROM test.mentor where mentor_id = ? and status_id = 1";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();

            Account mentor = null;
            StatusMentor status = null;

            while (rs.next()) {
                mentor = aDao.getAccountByID(rs.getInt("user_id"));
                status = sDao.getStatusByID(rs.getInt("status_id"));

                return new MyMentor(rs.getInt("mentor_id"),
                        mentor, status);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Mentor getMentorbyUserID(int user_id) {

        try {
            String sql = "select * from Mentor where user_id = ? ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, user_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Mentor(rs.getInt(1), rs.getInt(3));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void updateImageMentor(int mentor_id, String imagePath) {

        try {
            String xSql = "UPDATE CVMentor SET avatar = ? WHERE mentor_id = ? ";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setString(1, imagePath);
            ps.setInt(2, mentor_id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void deleteCvMentor(int mentorId) {

        try {
            String xSql = "delete from cvmentor where mentor_id = ? ; ";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, mentorId);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<CVMentor> getAllCV() {
        List<CVMentor> list = new ArrayList<>();
        try {
            String xSql = "select * from CVMentor ; ";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new CVMentor(rs.getInt(2), rs.getString(3), rs.getDate(4), rs.getString(5), rs.getBoolean(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List<CVMentor> getCVByStatusID(int statusId) {
        List<CVMentor> list = new ArrayList<>();
        try {
            String xSql = "select cv.* from cvmentor cv \n"
                    + "join mentor m on cv.mentor_id = m.mentor_id \n"
                    + "where m.status_id = ? ;";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, statusId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new CVMentor(rs.getInt(2), rs.getString(3), rs.getDate(4), rs.getString(5), rs.getBoolean(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public CVMentor getCV(int mentorId) {

        try {
            String xSql = "select * from CVMentor where mentor_id = ? ";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, mentorId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new CVMentor(rs.getInt(2), rs.getString(3), rs.getDate(4), rs.getString(5), rs.getBoolean(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void insertCV(int mentor_id, String Fullname, Date dob, String email, boolean gender, String phone, String address, String job, String intro_job, String Service, String Achievement) {

        try {
            String xSql = "INSERT INTO CVMentor ( mentor_id, full_name ,dob,email, sex ,phone,address,profession,intro_job,Service,Achievement) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, mentor_id);
            ps.setString(2, Fullname);
            ps.setDate(3, dob);
            ps.setString(4, email);
            ps.setBoolean(5, gender);
            ps.setString(6, phone);
            ps.setString(7, address);
            ps.setString(8, job);
            ps.setString(9, intro_job);
            ps.setString(10, Service);
            ps.setString(11, Achievement);

            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void insertMentor(int user_id, int status) {
        try {
            String xSql = "INSERT INTO Mentor (user_id,status_id) VALUES (?,?)";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, user_id);
            ps.setInt(2, status);

            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateCVMentor(int mentor_id, String fullname, Date dob, String email, boolean gender, String phone, String address, String job, String intro_job, String Service, String Achievement) {
        String xSql = "UPDATE cvmentor\n"
                + "SET full_name = ? ,\n"
                + "dob = ?,\n"
                + "email = ?,\n"
                + "sex = ?,\n"
                + "phone = ?,\n"
                + "address = ?,\n"
                + "profession = ?,\n"
                + "intro_job = ?,\n"
                + "Service = ?,\n"
                + "Achievement = ?\n"
                + "WHERE mentor_id = ? ;";
        try {
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setString(1, fullname);
            ps.setDate(2, dob);
            ps.setString(3, email);
            ps.setBoolean(4, gender);
            ps.setString(5, phone);
            ps.setString(6, address);
            ps.setString(7, job);
            ps.setString(8, intro_job);
            ps.setString(9, Service);
            ps.setString(10, Achievement);
            ps.setInt(11, mentor_id);

            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void UpdateStatusMentor(int statusId, int mentorId) {
        try {
            String xSql = "update mentor "
                    + "set status_id = ? "
                    + "where mentor_id = ? ;";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, statusId);
            ps.setInt(2, mentorId);

            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int CountMentor() {
        try {
            String xSql = "SELECT count(*) FROM mentor";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
          
        } catch (SQLException ex) {
             Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public List<CVMentor> pagingCvMentor(int index) {
        List<CVMentor> t = new ArrayList<>();
        String xSql = "SELECT * FROM cvmentor \n"
                + "ORDER BY mentor_id  ASC\n"
                + "LIMIT 6 OFFSET ? ;";
        try{
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, (index - 1) * 6);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                t.add(new CVMentor(rs.getInt(2), rs.getString(3), rs.getDate(4), rs.getString(5), rs.getBoolean(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13)));
            }
        } catch (SQLException ex) {
             Logger.getLogger(MyMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (t);
    }

    public static void main(String[] args) {
        MyMentorDAO m = new MyMentorDAO();
        List<CVMentor> l = m.pagingCvMentor(1);
        System.out.println(l);

    }
}
