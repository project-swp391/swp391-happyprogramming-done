/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Context;

import Model.Framework;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FrameDAO extends DBContext{
    public List<Framework> getAllFamework() {
        List<Framework> list = new ArrayList<>();
        
        try {
            String sql = "SELECT * FROM famework ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Framework(rs.getInt(1), rs.getString(2)));
            }

        } catch (SQLException ex) {
            Logger.getLogger(FrameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    public List<Framework> getFameMentor(int mentor_id) {
        List<Framework> l = new ArrayList<>();
        try {
            String sql = "SELECT famework.*\n"
                    + "FROM famework \n"
                    + "right JOIN mentorfamework mf\n"
                    + "ON famework.fame_id = mf.fame_id\n"
                    + "JOIN Mentor  ON mentor.mentor_id = mf.mentor_id\n"
                    + "WHERE Mentor.mentor_id = ? ;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, mentor_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                l.add(new Framework(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(FrameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return l;
    }
    public void deleteMentorfame(int mentor_id) {
        String xSql = "delete from mentorfamework where mentor_id = ? ;";
        try {
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, mentor_id);
            ps.executeUpdate();
         } catch (SQLException ex) {
            Logger.getLogger(FrameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public void insertMentorFame(int mentor_id, int fame_id) {
        try {
            String xSql = "INSERT INTO mentorfamework (mentor_id,fame_id) VALUES (?,?)";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, mentor_id);
            ps.setInt(2, fame_id);
            ps.executeUpdate();
         } catch (SQLException ex) {
            Logger.getLogger(FrameDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
