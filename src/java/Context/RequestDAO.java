/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Context;

import Model.Account;
import Model.Request1;
import Model.StatusRequest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RequestDAO extends DBContext {

    public int countTotal() {
        try {
            String sql = "SELECT max(request_id) as \"total\" FROM requests;";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public int countTotal(int userID) {
        try {
            String sql = "SELECT count(*) as 'total' FROM requests where user_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, userID);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public void insert(Request1 request) {
        try {
            String sql = "INSERT INTO `requests`\n"
                    + "(`request_id`,\n"
                    + "`user_id`,\n"
                    + "`title`,\n"
                    + "`deadline_date`,\n"
                    + "`deadline_hour`,\n"
                    + "`create_date`,\n"
                    + "`content`,\n"
                    + "`status_id`)\n"
                    + "VALUES\n"
                    + "(?,\n"
                    + "?,\n"
                    + "?,\n"
                    + "?,\n"
                    + "?,\n"
                    + "?,\n"
                    + "?,\n"
                    + "?);";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, request.getId());
            stm.setInt(2, request.getFrom().getId());
            stm.setString(3, request.getTitle());
            stm.setDate(4, request.getDeadlineDate());
            stm.setDouble(5, request.getDeadlineHour());
            stm.setDate(6, request.getCreateDate());
            stm.setString(7, request.getContent());
            stm.setInt(8, 1);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Request1> getAll() {
        ArrayList<Request1> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM requests;";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            Account from = null;
            AccountDAO accDAO = new AccountDAO();

            StatusRequestDAO sqDAO = new StatusRequestDAO();
            StatusRequest sq = null;

            while (rs.next()) {
                from = accDAO.getAccountByID(rs.getInt("user_id"));
                sq = sqDAO.getSQByID(rs.getInt("status_id"));

                list.add(new Request1(rs.getInt("request_id"),
                        from,
                        rs.getString("title"),
                        rs.getDate("deadline_date"),
                        rs.getDouble("deadline_hour"),
                        rs.getDate("create_date"),
                        rs.getString("content"),
                        sq));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

//    public ArrayList<Request1> getAll(int userID) {
//        ArrayList<Request1> list = new ArrayList<>();
//        try {
//            String sql = "SELECT * FROM requests where user_id = ?;";
//            PreparedStatement stm = connection.prepareStatement(sql);
//            stm.setInt(1, userID);
//            ResultSet rs = stm.executeQuery();
//
//            Account from = null;
//            AccountDAO accDAO = new AccountDAO();
//
//            StatusRequestDAO sqDAO = new StatusRequestDAO();
//            StatusRequest sq = null;
//
//            while (rs.next()) {
//                from = accDAO.getAccountByID(rs.getInt("user_id"));
//                sq = sqDAO.getSQByID(rs.getInt("status_id"));
//
//                list.add(new Request1(rs.getInt("request_id"),
//                        from,
//                        rs.getString("title"),
//                        rs.getDate("deadline_date"),
//                        rs.getDouble("deadline_hour"),
//                        rs.getDate("create_date"),
//                        rs.getString("content"),
//                        sq));
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return list;
//    }
    public ArrayList<Request1> getAll(int userID) {
        ArrayList<Request1> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM requests r\n"
                    + "left join requestmentor rm\n"
                    + "on r.request_id = rm.request_id\n"
                    + " where r.user_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, userID);
            ResultSet rs = stm.executeQuery();

            Account from = null;
            AccountDAO accDAO = new AccountDAO();

            StatusRequestDAO sqDAO = new StatusRequestDAO();
            StatusRequest sq = null;

            Account to = null;

            while (rs.next()) {
                from = accDAO.getAccountByID(rs.getInt("user_id"));
                to = accDAO.getAccountByID(rs.getInt("mentor_id"));
                sq = sqDAO.getSQByID(rs.getInt("status_id"));

                list.add(new Request1(rs.getInt("request_id"),
                        from,
                        to,
                        rs.getString("title"),
                        rs.getDate("deadline_date"),
                        rs.getDouble("deadline_hour"),
                        rs.getDate("create_date"),
                        rs.getString("content"),
                        sq));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public Request1 getRequestByID(int id) {
        try {
            String sql = "SELECT * FROM requests where request_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();

            Account from = null;
            AccountDAO accDAO = new AccountDAO();

            StatusRequestDAO sqDAO = new StatusRequestDAO();
            StatusRequest sq = null;

            while (rs.next()) {
                from = accDAO.getAccountByID(rs.getInt("user_id"));
                sq = sqDAO.getSQByID(rs.getInt("status_id"));

                return new Request1(rs.getInt("request_id"),
                        from,
                        rs.getString("title"),
                        rs.getDate("deadline_date"),
                        rs.getDouble("deadline_hour"),
                        rs.getDate("create_date"),
                        rs.getString("content"),
                        sq);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void update(Request1 rq) {
        try {
            String sql = "UPDATE `requests`\n"
                    + "SET\n"
                    + "`title` = ?,\n"
                    + "`deadline_date` = ?,\n"
                    + "`deadline_hour` =?,\n"
                    + "`content` = ?\n"
                    + "WHERE `request_id` = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, rq.getTitle());
            stm.setDate(2, rq.getDeadlineDate());
            stm.setDouble(3, rq.getDeadlineHour());
            stm.setString(4, rq.getContent());
            stm.setInt(5, rq.getId());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateStatus(int requestId, int statusId) {
        try {
            String sql = "UPDATE `requests` SET `status_id` = ? WHERE (`request_id` = ? );";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, statusId);
            stm.setInt(2, requestId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete(Request1 rq) {
        try {
            String sql = "DELETE FROM `requests`\n"
                    + "WHERE request_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, rq.getId());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Integer> getMentors(int userID) {
        ArrayList<Integer> list = new ArrayList<>();
        try {
            String sql = "SELECT rm.mentor_id FROM requests r left join requestmentor rm\n"
                    + "on r.request_id = rm.request_id\n"
                    + "where user_id = ?\n"
                    + "group by rm.mentor_id;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, userID);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                list.add(rs.getInt("mentor_id"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public int getTotalRecord() {
        try {
            String sql = "SELECT count(*) as 'total' FROM requests\n";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
                return rs.getInt("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public int getTotalRequestByStatus(int statusID, int mentorID) {
        try {
            String sql = "SELECT COUNT(*) AS total_requests\n"
                    + "FROM `requests`\n"
                    + "join requestmentor on requests.request_id = requestmentor.request_id\n"
                    + "join mentor on mentor.mentor_id = requestmentor.mentor_id\n"
                    + "WHERE `requests`.`status_id` = ? AND `mentor`.`mentor_id` = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, statusID);
            stm.setInt(2, mentorID);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getInt("total_requests");
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public ArrayList<Request1> getAll(int page, int recordPerPage, int mentorID) {
        ArrayList<Request1> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM requests r\n"
                    + "left join requestmentor rm\n"
                    + "on r.request_id = rm.request_id\n"
                    + "where mentor_id = ?\n"
                    + "limit ?, ?";
            PreparedStatement stm = connection.prepareStatement(sql);

            stm.setInt(1, mentorID);
            stm.setInt(2, page);
            stm.setInt(3, recordPerPage);
            ResultSet rs = stm.executeQuery();

            Account from = null;
            AccountDAO accDAO = new AccountDAO();

            StatusRequestDAO sqDAO = new StatusRequestDAO();
            StatusRequest sq = null;

            while (rs.next()) {
                from = accDAO.getAccountByID(rs.getInt("user_id"));
                sq = sqDAO.getSQByID(rs.getInt("status_id"));

                list.add(new Request1(rs.getInt("request_id"),
                        from,
                        rs.getString("title"),
                        rs.getDate("deadline_date"),
                        rs.getDouble("deadline_hour"),
                        rs.getDate("create_date"),
                        rs.getString("content"),
                        sq));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public double getTotalHour(int userID) {
        try {
            String sql = "SELECT sum(deadline_hour) as 'total' from requests where user_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, userID);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getDouble("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public double getTotalHourRequest() {
        try {
            String sql = "SELECT sum(deadline_hour) as 'total' from requests";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return rs.getDouble("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public int getTotalRecord(int mentorID) {
        try {
            String sql = "SELECT count(*) as 'total' FROM requestmentor\n"
                    + "where mentor_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, mentorID);
            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
                return rs.getInt("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

}
