/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Context;

import Model.Account;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class AccountDAO extends MyDAO {

    public Account getAccountByID(int id) {
        try {
            String sql = "SELECT * FROM users where user_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return new Account(rs.getInt("user_id"),
                        rs.getString("user_name"),
                        rs.getString("email"),
                        rs.getInt("role_id"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Account login(String user, String pass) {

        try {
             xSql = "select * from Users where user_name= ? and password_user= ? ;";
           
            ps = con.prepareStatement(xSql);
            ps.setString(1, user);
            ps.setString(2, pass);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5));

            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Account checkExit(String user, String emai) {

        try {
            String xSql = "select * from Users where user_name=? or email =?";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setString(1, user);
            ps.setString(2, emai);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Account(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void insertUser(String user, String pass, String email, int role) {
        try {
            String xSql = "INSERT INTO Users (user_name,password_user,email,role_id) VALUES (?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setString(1, user);
            ps.setString(2, pass);
            ps.setString(3, email);
            ps.setInt(4, role);
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updatePass(String newpass, String user) {
        try {
            String xSql = "UPDATE Users\n"
                    + "SET password_user = ?\n"
                    + "WHERE user_name = ? ";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setString(1, newpass);
            ps.setString(2, user);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void insertProfile(int userid, String avatar, String Fullname, boolean gender, String phone, Date dob, String address) {

        try {
            String xSql = "INSERT INTO Profile ( user_id, avatar, full_name , sex ,phone,dob,address) VALUES (?,?,?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, userid);
            ps.setString(2, avatar);
            ps.setString(3, Fullname);
            ps.setBoolean(4, gender);
            ps.setString(5, phone);
            ps.setDate(6, dob);
            ps.setString(7, address);

            ps.executeUpdate();

         } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void updateRole(int user_id, int role_id) {
        String xSql = "UPDATE Users SET role_id = ? WHERE user_id = ? ";
        try {
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, role_id);
            ps.setInt(2, user_id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
     public void updatePassword(String username, String newPassword) {

        try {
            String xSql = "UPDATE Users SET password_user = ? WHERE user_name = ?";
            PreparedStatement ps = connection.prepareStatement(xSql);

            ps.setString(1, newPassword);
            ps.setString(2, username);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public Account getAccountByEmail(String email) {
        try {
            String xSql = "SELECT * FROM Users WHERE email = ?";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new Account(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5));
            }
        } catch (SQLException e) {
            System.out.println(e);
            // Xử lý ngoại lệ
        }
        return null;
    }
      public Account getUserByMentorId(int mentorId) {

    try {
        String sql = "SELECT u.user_id, u.user_name, u.password_user, u.email, u.role_id " +
                     "FROM users u " +
                     "JOIN mentor m ON u.user_id = m.user_id " +
                     "WHERE m.mentor_id = ? AND u.role_id = 2";

        PreparedStatement stm = connection.prepareStatement(sql);
        stm.setInt(1, mentorId);
        ResultSet resultSet = stm.executeQuery();

        if (resultSet.next()) {
            int id = resultSet.getInt("user_id");
            String userName = resultSet.getString("user_name");
            String password = resultSet.getString("password_user");
            String email = resultSet.getString("email");
            int roleId = resultSet.getInt("role_id");

            return new Account(id, userName, password, email, roleId);
        }

        resultSet.close();
        stm.close();
    } catch (SQLException e) {
        // Handle exception if needed
    }

    return null;
    
    
}
     public ArrayList<Account> getAllByRoleID(int roleID, int page, int recordPerPage) {
        ArrayList<Account> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM users\n"
                    + "where role_id = ?\n"
                    + "limit ? , ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, roleID);
            stm.setInt(2, page);
            stm.setInt(3, recordPerPage);

            ResultSet resultSet = stm.executeQuery();

            while (resultSet.next()) {
                list.add(new Account(resultSet.getInt("user_id"),
                        resultSet.getString("user_name"),
                        resultSet.getString("email"),
                        roleID));
            }
            resultSet.close();
            stm.close();
        } catch (SQLException e) {
            // Handle exception if needed
        }
        return list;
    }

    public int totalRecord(int roleID) {
        try {
            String sql = "SELECT count(*) as 'total' FROM users\n"
                    + "where role_id = ?\n";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, roleID);

            ResultSet resultSet = stm.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt("total");
            }
            resultSet.close();
            stm.close();
        } catch (SQLException e) {
            // Handle exception if needed
        }
        return 0;
    }
       public int getTotalAccount(int roleID) {
        try {
            String sql = "SELECT count(*) as 'total' FROM users where role_id = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, roleID);
            ResultSet resultSet = stm.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt("total");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
//     public static void main(String[] args) {
//         AccountDAO a = new AccountDAO();
//         Account acc =  a.login("kute251", "123456");
//         System.out.println(acc);
//    }

}
