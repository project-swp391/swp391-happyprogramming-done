/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Context;

import Model.Skill;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class SkillDAO extends DBContext {

    // get all skill by mentorID
    public List<Skill> listAllSkillByMentorID(int id) {
        List<Skill> list = new ArrayList<>();
        try {
            String sql = "select * from skill join mentorskill on skill.skill_id = mentorskill.Skill_id where status_skill = 'active' and mentor_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                list.add(new Skill(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getString(6)
                ));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    // get all skill 
    public List<Skill> listAllSkill() {
        List<Skill> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM skill";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                list.add(new Skill(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getString(6)
                ));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List<Skill> listTop4Skill() {
        List<Skill> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM skill\n"
                    + "WHERE status_skill = 'active'\n"
                    + "ORDER BY skill_price DESC LIMIT 4;";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                list.add(new Skill(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getString(6)
                ));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    // get all skill with status active by admin
    public List<Skill> listAllSkillbyStatus(String status) {
        List<Skill> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM skill where status_skill = ? \n"
                    + "order by skill_name asc;";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, status);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                list.add(new Skill(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDouble(4),
                        rs.getString(5),
                        rs.getString(6)
                ));
            }

        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public Skill getSkillByID(int id) {
        try {
            String sql = "SELECT * FROM skill where skill_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return new Skill(rs.getInt("skill_id"),
                        rs.getString("skill_name"),
                        rs.getString("decription"),
                        rs.getDouble("skill_price"),
                        rs.getString("image"),
                        rs.getString("status_skill")
                );
            }
        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void insertSkill(String name, String decription, double price, String image, String status) {
        try {
            String xSql = "INSERT INTO Skill(skill_name,decription,skill_price,image,status_skill) VALUES (?,?,?,?,?)";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setString(1, name);
            ps.setString(2, decription);
            ps.setDouble(3, price);
            ps.setString(4, image);
            ps.setString(5, status);

            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateSkill(int id, String name, double price, String decription, String image, String status) {
        try {
            String xSql = "UPDATE  Skill "
                    + "SET skill_name = ? ,"
                    + "decription = ? ,"
                    + "skill_price = ? ,"
                    + "image = ? ,"
                    + "status_skill = ?  "
                    + "WHERE skill_id = ?;";
            PreparedStatement ps = connection.prepareStatement(xSql);

            ps.setString(1, name);
            ps.setString(2, decription);
            ps.setDouble(3, price);
            ps.setString(4, image);
            ps.setString(5, status);
            ps.setInt(6, id);
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateStatusSkill(String status, int id) {
        try {
            String xSql = "Update skill\n"
                    + "set status_skill = ?\n"
                    + "where skill_id = ? ;";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setString(1, status);
            ps.setInt(2, id);
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Skill> getSkillByRequestID(int requestId) {
        ArrayList<Skill> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM request_skill\n"
                    + "where request_id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, requestId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Skill s = getSkillByID(rs.getInt("skill_id"));
                list.add(s);
            }

        } catch (Exception e) {
        }
        return list;
    }
    public static void main(String[] args) {
        SkillDAO s = new SkillDAO();
        List<Skill> l = s.listTop4Skill();
        System.out.println(l);
    }

}
