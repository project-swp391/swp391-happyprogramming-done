/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Context;

import Model.StatusRequest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class StatusRequestDAO extends DBContext{
    public StatusRequest getSQByID(int reqID){
        try {
            String sql = "SELECT * FROM requeststatus where status_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, reqID);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                return new StatusRequest(rs.getInt("status_id"), rs.getString("status_name"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public List<StatusRequest>  getAllStatus(){
        List<StatusRequest> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM requeststatus ;";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while(rs.next()) {
                 list.add(new StatusRequest(rs.getInt(1), rs.getString(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
  
}
