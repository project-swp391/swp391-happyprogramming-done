/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Context;

import Model.Account;
import Model.Request1;
import Model.RequestMentor;
import Model.StatusRequest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class RequestMentorDAO extends DBContext {

    public void insert(RequestMentor reqMentor) {
        try {
            String sql = "INSERT INTO `requestmentor`\n"
                    + "(`request_id`,\n"
                    + "`mentor_id`)\n"
                    + "VALUES\n"
                    + "(?,\n"
                    + "?);";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, reqMentor.getRequest().getId());
            stm.setInt(2, reqMentor.getMentor().getId());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RequestMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<RequestMentor> getAll() {
        ArrayList<RequestMentor> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM requestmentor;";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            RequestDAO rDao = new RequestDAO();
            Request1 request = new Request1();
            AccountDAO aDao = new AccountDAO();
            Account mentor = null;
            while (rs.next()) {
                request = rDao.getRequestByID(rs.getInt("request_id"));
                mentor = aDao.getAccountByID(rs.getInt("mentor_id"));
                list.add(new RequestMentor(request, mentor, rs.getInt("mentor_id")));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public void update(RequestMentor rm) {
        try {
            String sql = "UPDATE `requestmentor`\n"
                    + "SET `mentor_id` = ?\n"
                    + "WHERE `request_id` = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, rm.getMentor().getId());
            stm.setInt(2, rm.getRequest().getId());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RequestMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void delete(RequestMentor rm) {
        try {
            String sql = "DELETE FROM `requestmentor`\n"
                    + "WHERE request_id = ?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, rm.getRequest().getId());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Request1> getRequestByMentor(int mentorId) {
        ArrayList<Request1> list = new ArrayList<>();
        try {
            String sql = "SELECT r.* FROM requests r \n"
                    + "join requestmentor rm on r.request_id = rm.request_id\n"
                    + "where rm.mentor_id = ?  ;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, mentorId);

            ResultSet rs = ps.executeQuery();
            Account from = null;
            AccountDAO accDAO = new AccountDAO();
            StatusRequestDAO sqDAO = new StatusRequestDAO();
            StatusRequest sq = null;
            while (rs.next()) {
                from = accDAO.getAccountByID(rs.getInt("user_id"));
                sq = sqDAO.getSQByID(rs.getInt("status_id"));
                list.add(new Request1(rs.getInt("request_id"),
                        from,
                        rs.getString("title"),
                        rs.getDate("deadline_date"),
                        rs.getDouble("deadline_hour"),
                        rs.getDate("create_date"),
                        rs.getString("content"),
                        sq));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List<RequestMentor> countReqest(int statusId) {
        List<RequestMentor> list = new ArrayList<>();
        try {
            String sql = "SELECT m.mentor_id, COALESCE(COUNT(r.request_id), 0) AS totalRequest\n"
                    + "   FROM mentor m \n"
                    + "   left join requestmentor rm on m.mentor_id = rm.mentor_id \n"
                    + "   LEFT JOIN requests r ON r.request_id = rm.request_id  AND r.status_id = ?\n"
                    + "   GROUP BY m.mentor_id";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, statusId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new RequestMentor(rs.getInt(1), rs.getInt(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List<RequestMentor> getCountReqestTop10() {
        List<RequestMentor> list = new ArrayList<>();
        try {
            String sql = "select mentor_id,count(*) as total from requestmentor \n"
                    + "group by mentor_id\n"
                    + "order by total desc limit 10;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new RequestMentor(rs.getInt(1), rs.getInt(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List<RequestMentor> PercentReqest() {
        List<RequestMentor> list = new ArrayList<>();
        try {
            String sql = "SELECT t1.mentor_id,\n"
                    + "  CASE\n"
                    + "    WHEN t1.totalRequest = 0 or t2.totalRequest = 0 THEN 0 \n"
                    + "    ELSE (round(t1.totalRequest / t2.totalRequest,4) )* 100\n"
                    + "  END AS division_result\n"
                    + "FROM\n"
                    + "  (SELECT m.mentor_id, COALESCE(COUNT(r.request_id), 0) AS totalRequest\n"
                    + "   FROM mentor m \n"
                    + "   left join requestmentor rm on m.mentor_id = rm.mentor_id \n"
                    + "   LEFT JOIN requests r ON r.request_id = rm.request_id  AND r.status_id = 4\n"
                    + "   GROUP BY m.mentor_id) AS t1\n"
                    + "JOIN\n"
                    + "  (SELECT m.mentor_id, COALESCE(COUNT(r.request_id), 0) AS totalRequest\n"
                    + "   FROM mentor m \n"
                    + "   left join requestmentor rm on m.mentor_id = rm.mentor_id \n"
                    + "   LEFT JOIN requests r ON r.request_id = rm.request_id  AND(r.status_id = 2 or r.status_id = 4 )\n"
                    + "   GROUP BY m.mentor_id) AS t2\n"
                    + "ON t1.mentor_id = t2.mentor_id;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new RequestMentor(rs.getInt(1), rs.getDouble(2)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(RequestDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static void main(String[] args) {
        RequestMentorDAO r = new RequestMentorDAO();
        List<RequestMentor> l = r.PercentReqest();
        System.out.println(l);
    }

//    public List<Request> getAllRequestbyMentor(){
//        List<Request> list = new ArrayList<>();
//        try {
//            String sql = "";
//            PreparedStatement ps = connection.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {                
//                //Request r = new Request(rs.getInt(1), , 0, sql, deadline_date, 0, sql, 0, 0);
//            }
//            
//        } catch (SQLException ex) {
//             Logger.getLogger(RequestMentorDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return  list;
//        }
}
