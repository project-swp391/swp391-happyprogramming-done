/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Validate;

import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author Admin
 */
public class Validation {

    public static String checkPassword(String password) {
        if (password.isEmpty()) {
            return "Password not empty.";
        }
        if (password.length() < 6 || password.length() > 100) {
            return "Password must between 6 to 100 character";
        }
        if (validatePassword(password)) {
            return password;
        } else {
            return "Password not format";
        }

    }

    public static boolean validatePassword(String password) {
        String pattern = "^(?=.*[a-zA-Z]).{6,}$";
        return Pattern.matches(pattern, password);
    }

    public static String checkUsername(String Username) {
        if (Username.isEmpty()) {
            return "Username not Empty";
        }
        if (Username.length() < 6 || Username.length() > 20) {
            return "Username must between 6 to 20 character";
        }
        String pattern = "^[a-zA-Z0-9_]+$";
        Pattern regex = Pattern.compile(pattern);
        Matcher matcher = regex.matcher(Username);
        return Username;

    }

    public static boolean isDateEmptyOrPastToday(String dateString) {
        if (dateString.isEmpty()) {
            return false; // Ngày rỗng
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = null;

        try {
            date = (Date) dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) {
            return false; // Ngày không hợp lệ
        }

        Date currentDate = new Date();

        if (date.after(currentDate)) {
            return true; // Ngày không vượt quá ngày hôm nay
        }
        return true; // Ngày đã qua hoặc bằng ngày hôm nay
    }

    public static String Checkphone(String phone) {
        if (phone.isEmpty()) {
            return "Phone can't be blank";
        }
        if (validatePhoneNumber(phone)) {
            return phone;
        } else {
            return "Phone is not true format";
        }
    }

    public static boolean validatePhoneNumber(String phoneNumber) {
        String pattern = "^0\\d{9}$";
        return Pattern.matches(pattern, phoneNumber);
    }

    public static String CheckEmail(String email) {
        if (email.isEmpty()) {
            return "Email can't be blank";
        }
        if (validateEmail(email)) {
            return email;
        } else {
            return "Email is not true format";
        }
    }
  

    public static boolean validateEmail(String email) {
        String regex = "^[a-zA-Z]{1}[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }
    public static String isDateValid(String dob) {
        if (dob == null || dob.isEmpty()) {
            
            return "Date cannot be blank.";
        }
       
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            LocalDate date = LocalDate.parse(dob, formatter);
            LocalDate today = LocalDate.now();

            if (date.isAfter(today)) {
                return "The day cannot pass today.";
            }
        } catch (Exception e) {
          
            return "Invalid date.(dd/MM/yyyy)";
        }
        return dob;
    }

}
