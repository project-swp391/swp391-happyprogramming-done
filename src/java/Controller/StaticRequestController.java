/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Context.RequestDAO;
import Context.RequestMentorDAO;
import Model.Account;
import Model.Request1;
import Model.RequestMentor;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 *
 * @author dell
 */
public class StaticRequestController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet StaticRequestController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet StaticRequestController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        RequestMentorDAO rmDao = new RequestMentorDAO();
        RequestDAO rDao = new RequestDAO();
        
//        ArrayList<Request> listReq = rDao.getAll();
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("acc");
//        ArrayList<Request> listReq = rDao.getAll(account.getId());
        ArrayList<RequestMentor> listRM = rmDao.getAll();
        ArrayList<Request1> listReq = rDao.getAll(account.getId());
        
        HttpSession sesson = request.getSession();
        Account acc = new Account();
        //int totalRequest = rDao.countTotal(acc.getId());

        ArrayList<Integer> Mentors = rDao.getMentors(account.getId());
        
        int totalRequest = rDao.countTotal(account.getId());

        double totalHour = rDao.getTotalHour(account.getId());

        request.setAttribute("totalRequest", totalRequest);
        request.setAttribute("totalMentor", Mentors.size());
        request.setAttribute("totalHour", totalHour);
        request.setAttribute("listRM", listRM);
        request.setAttribute("listReq", listReq);
        
        request.getRequestDispatcher("statisticRequestByMe.jsp").forward(request, response);
    }
//
//    public static void main(String[] args) {
//        RequestDAO rDao = new RequestDAO();
//        double totalHour = rDao.getTotalHour(4);
//        System.out.println(totalHour);
//    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("statisticRequestByMe.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
