/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controller;


import Context.AccountDAO;
import Model.Account;
import Model.DAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;



/**
 *
 * @author PC
 */
@WebServlet(name="FogotServlet", urlPatterns={"/FogotServlet"})
public class FogotServlet extends HttpServlet {
   
   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       
    } 

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
try {
            String username = request.getParameter("user_name");
            String email = request.getParameter("email");
  
            AccountDAO aDao = new AccountDAO();
            Account acc = aDao.getAccountByEmail(email);

            if (acc != null && acc.getUser_name().equals(username)) {
                // Tạo mật khẩu mới
                String newPassword = generateNewPassword();
               LocalDateTime expirationDateTime = LocalDateTime.now().plusMinutes(5);
               DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
               String expirationTimeString = expirationDateTime.format(formatter);

                // Cập nhật mật khẩu mới vào cơ sở dữ liệu
                aDao.updatePassword(username, newPassword);
                

                // Gửi mật khẩu mới đến email của người dùng
                sendEmail(acc.getEmail(), newPassword, expirationTimeString);
                request.getSession().setAttribute("resetUserId", acc.getId());
                request.getSession().setAttribute("passwordExpiration", expirationDateTime);

                // Chuyển hướng đến trang thông báo khôi phục mật khẩu thành công
              request.setAttribute("su", "Mật khẩu mới đã được gửi đến email của bạn.");
              request.getRequestDispatcher("fogotpass.jsp").forward(request, response);
            } else {

                // Người dùng không hợp lệ, chuyển hướng đến trang thông báo lỗi
               request.setAttribute("err", "Username or email không đúng.");
              request.getRequestDispatcher("fogotpass.jsp").forward(request, response);
            }
        } catch (ServletException | IOException e) {
        }
    }
    
    private String generateNewPassword() {
         String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
         int passwordLength = 8;
         Random random = new Random();
         StringBuilder newPassword = new StringBuilder();

         for (int i = 0; i < passwordLength; i++) {
             int randomIndex = random.nextInt(characters.length());
              newPassword.append(characters.charAt(randomIndex));
        }
       
    return newPassword.toString();
   }
    
    

private void sendEmail(String email, String newPassword, String expirationTimeString) {

    String username = "mp16082345@gmail.com";
    String pass = "muzg vbzj zdmv mblp";
    Properties props = new Properties();
    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.port", "587");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", "true");

    // Tạo đối tượng Authenticator để xác thực
    Authenticator auth = new Authenticator() {
        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(username, pass);
        }
    };

    // Tạo đối tượng Session với thông tin xác thực và thuộc tính
    Session session = Session.getInstance(props, auth);

    try {
        // Tạo đối tượng MimeMessage
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(username));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
        message.setSubject("Reset Password");
        message.setText("Your new password is: " + newPassword + "\n New password will expire in 5 minutes:" + expirationTimeString);
       

        // Gửi email
        Transport.send(message);
              
        System.out.println("Một email chứa mật khẩu mới đã được gửi đến địa chỉ email của bạn.");
    } catch (MessagingException e) {
        
    }
    
}

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
