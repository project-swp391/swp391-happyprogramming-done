/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.admin;

import Context.MyMentorDAO;
import Context.RequestMentorDAO;
import Context.ReviewDAO;
import Model.CVMentor;
import Model.MyMentor;
import Model.RequestMentor;
import Model.Review;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
@WebServlet(name = "ListAllMentor", urlPatterns = {"/list-all-mentor"})
public class ListAllMentorController extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         
        
        try ( PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListAllMentor</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListAllMentor at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        MyMentorDAO md = new MyMentorDAO();
        RequestMentorDAO rm = new RequestMentorDAO();
        ReviewDAO rd = new ReviewDAO();
        List<Review> listRV = rd.getRateStar();
        List<RequestMentor> listCR = rm.countReqest(2);
        List<RequestMentor> listPR = rm.PercentReqest();
       
        int total = md.CountMentor();
        int  endpage = total / 6 ;
        if (total % 6 != 0) {
            endpage ++;
        }
        String index = request.getParameter("index");
        if (index == null) {
          index = "1" ;
        }
        int ind = Integer.parseInt(index);
        List<CVMentor> listCV = md.pagingCvMentor(ind);
        ArrayList<MyMentor> listM = md.getAllMentor();
        request.setAttribute("page", total);
        request.setAttribute("endP", endpage);
        request.setAttribute("tag", ind);
        request.setAttribute("listRV", listRV);
        request.setAttribute("listPR", listPR);
        request.setAttribute("listCR", listCR);
        request.setAttribute("listM", listM);
        request.setAttribute("listCV", listCV);
        request.getRequestDispatcher("ListAllMentor.jsp").forward(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
