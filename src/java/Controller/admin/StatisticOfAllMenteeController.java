/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.admin;

import Context.AccountDAO;
import Context.RequestDAO;
import Context.Request_SkillDAO;
import Model.Account;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class StatisticOfAllMenteeController extends BaseAdminController {

    final int menteeRoleID = 3;
    final int recordPerPage = 10;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet StatisticOfAllMenteeController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet StatisticOfAllMenteeController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void processGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            int page = 1;
            
            if (request.getParameter("page") != null) {
                page = Integer.parseInt(
                        request.getParameter("page"));
            }
            
            AccountDAO aDao = new AccountDAO();
            RequestDAO rDao = new RequestDAO();
            Request_SkillDAO rsDao = new Request_SkillDAO();
            
            int totalMentee = aDao.getTotalAccount(menteeRoleID);
            double totalH = rDao.getTotalHourRequest();
            int totalS = rsDao.getTotalSkillRequest();
            //ArrayList<Account> listMentee
            
            ArrayList<Account> listMentee = aDao.getAllByRoleID(menteeRoleID, (page - 1) * recordPerPage, recordPerPage);
            int totalRecord = aDao.totalRecord(menteeRoleID);
            int noOfPages = (int) Math.ceil((double) totalRecord / recordPerPage);
            
            if (request.getParameter("totalP") != null) {
                noOfPages = Integer.parseInt(
                        request.getParameter("totalP"));
            }
            
            request.setAttribute("curP", page);
            request.setAttribute("totalP", noOfPages);
            request.setAttribute("listMentee", listMentee);
            request.setAttribute("totalMentee", totalMentee);
            request.setAttribute("totalH", totalH);
            request.setAttribute("totalS", totalS);
            request.getRequestDispatcher("/StatisticOfAllMentee.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(StatisticOfAllMenteeController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StatisticOfAllMenteeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void processPost(HttpServletRequest request, HttpServletResponse response) {
        try {
            processRequest(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(StatisticOfAllMenteeController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StatisticOfAllMenteeController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
