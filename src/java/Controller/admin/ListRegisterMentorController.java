/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.admin;

import Context.MentorSkillDAO;
import Context.MyMentorDAO;
import Context.SkillDAO;
import Model.CVMentor;
import Model.MentorSkill;
import Model.Skill;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;


@WebServlet(name = "ListRegisterMentorController", urlPatterns = {"/list_register_mentor"})
public class ListRegisterMentorController extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListRegisterMentorController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListRegisterMentorController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MyMentorDAO md = new MyMentorDAO();
        List<CVMentor> listcv = md.getCVByStatusID(4);
        SkillDAO s = new SkillDAO();
        List<Skill> listS = s.listAllSkill();
        MentorSkillDAO msd = new MentorSkillDAO();
        List<MentorSkill> listMs = msd.getAllMentorSkill();
        request.setAttribute("listMs", listMs);
        request.setAttribute("listS", listS);
        request.setAttribute("listCV", listcv);
        request.getRequestDispatcher("ListRegistMentor.jsp").forward(request, response);
        
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
