/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.admin;

import Context.FrameDAO;
import Context.MentorSkillDAO;
import Context.MyMentorDAO;
import Context.SkillDAO;
import Model.CVMentor;
import Model.Framework;
import Model.MentorSkill;
import Model.Skill;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author Admin
 */
@WebServlet(name = "ViewCVRegisterController", urlPatterns = {"/viewCVRegister"})
public class ViewCVRegisterController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ViewCVRegisterController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ViewCVRegisterController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String mentorid = request.getParameter("mentorid");
        MyMentorDAO md = new MyMentorDAO();
        CVMentor CV = md.getCV(Integer.parseInt(mentorid));
        SkillDAO s = new SkillDAO();
        List<Skill> listS = s.listAllSkill();
        MentorSkillDAO msd = new MentorSkillDAO();
        List<MentorSkill> listMs = msd.getAllMentorSkill();
        FrameDAO f = new FrameDAO();
        List<Framework> listF = f.getAllFamework();
        List<Framework> listFm = f.getFameMentor(Integer.parseInt(mentorid));
        request.setAttribute("listFm", listFm);
        request.setAttribute("listF", listF);
        request.setAttribute("listMs", listMs);
        request.setAttribute("listS", listS);
        request.setAttribute("cv", CV);
        request.getRequestDispatcher("ViewRegistCV.jsp").forward(request, response);
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
