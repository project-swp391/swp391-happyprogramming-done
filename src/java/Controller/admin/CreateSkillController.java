/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.admin;

import Context.SkillDAO;
import Model.DAO;
import Model.Skill;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author Admin
 */
@WebServlet(name = "CreateSkill", urlPatterns = {"/createskill"})
public class CreateSkillController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        try ( PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateSkill</title>");
            out.println("</head>");
            out.println("<body>");
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SkillDAO sDAO = new SkillDAO();
        String name = request.getParameter("name");
        String price = request.getParameter("price");
        String url = request.getParameter("url");
        String description = request.getParameter("description");
        String status = request.getParameter("status");
        List<Skill> l = sDAO.listAllSkill();
        for (Skill s : l) {
            if(name.equalsIgnoreCase(s.getName())){
                request.setAttribute("mess", "Kỹ năng này đã tồn tại !");
                request.getRequestDispatcher("createskill.jsp").forward(request, response);
            }
        }
        sDAO.insertSkill(name, description, Double.parseDouble(price),url, status);
        response.sendRedirect("viewskill");

    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
