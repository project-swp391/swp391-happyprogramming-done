/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Context.AccountDAO;
import Context.MyMentorDAO;
import Model.Account;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;

@WebServlet(name = "RegisterControl", urlPatterns = {"/register"})
public class RegisterControl extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegisterControl</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegisterControl at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("getotp.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        int otp = (int) session.getAttribute("otp");
        String code = request.getParameter("otp");
        String user = (String) session.getAttribute("user");
        String pass = (String) session.getAttribute("pass");
        String email = (String) session.getAttribute("email");
        String fullname = (String) session.getAttribute("fullname");
        String phone = (String) session.getAttribute("phone");
        String dob = (String) session.getAttribute("dob");
        String address = (String) session.getAttribute("address");
        String sex = (String) session.getAttribute("gender");
        String roles = (String) session.getAttribute("role");

        boolean gender;
        if (sex.equals("0")) {
            gender = false;
        } else {
            gender = true;
        }
        long endtime = (long) session.getAttribute("endtime");
        long currenttime = System.currentTimeMillis();
        int code1 = Integer.parseInt(code.trim());
        if (endtime > currenttime) {
            if (otp == code1) {
                AccountDAO d = new AccountDAO();
                int role = Integer.parseInt(roles);
                if(role == 3) {
                    d.insertUser(user, pass, email, role);
                    Account a = d.login(user, pass);
                    String avatar = "https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg";
                    Date doB = Date.valueOf(dob);
                    d.insertProfile(a.getId(), avatar, fullname, gender, phone, doB, address);
                    response.sendRedirect("login");
                }else{
                MyMentorDAO mDAO = new MyMentorDAO();
                d.insertUser(user, pass, email, role);
                Account a = d.login(user, pass);
                mDAO.insertMentor(a.getId(), 3);
                String avatar = "https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg";
                Date doB = Date.valueOf(dob);
                d.insertProfile(a.getId(), avatar, fullname, gender, phone, doB, address);
                response.sendRedirect("login");
                }
            } else {
                request.setAttribute("message", "Nhập sai OTP !");
                request.getRequestDispatcher("getotp.jsp").forward(request, response);
            }
        } else {
            request.setAttribute("message", "OTP quá hạn!");
            request.getRequestDispatcher("getotp.jsp").forward(request, response);

        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
