package Controller.Mentee;

import Context.ProfileDAO;
import Model.Account;
import Model.Profile;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.time.LocalDate;

@WebServlet(name = "Edit_Profile_Controller", urlPatterns = {"/editprofile"})
public class Edit_Profile_Controller extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Edit_Profile_Controller</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1> </h1>");
            out.println("<h1> </h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("acc");
        if (a == null) {
            response.sendRedirect("login");
        } else {
            Date now = Date.valueOf(LocalDate.now());
            ProfileDAO pDAO = new ProfileDAO();
            Profile p = pDAO.getProfile(a.getId());
            request.setAttribute("now", now);
            request.setAttribute("p", p);
            request.getRequestDispatcher("updateprofilementee.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String fullname = request.getParameter("fullname");
        String phone = request.getParameter("phone");
        String doB = request.getParameter("dob");
        String sex = request.getParameter("gender");
        String address = request.getParameter("address");
        String discription = request.getParameter("description");
        Date dob = Date.valueOf(doB);
        boolean gender;
        if (sex.equals("0")) {
            gender = false;
        } else {
            gender = true;
        }
        ProfileDAO pDAO = new ProfileDAO();
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("acc");
        pDAO.updateProfileMentee(fullname, gender, phone, dob, address, discription, a.getId());
        response.sendRedirect("editprofile");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
