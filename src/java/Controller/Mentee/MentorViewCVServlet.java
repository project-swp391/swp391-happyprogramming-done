/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controller.Mentee;


import Context.AccountDAO;
import Context.MentorSkillDAO;
import Context.MyMentorDAO;
import Context.ReviewDAO;
import Model.Account;
import Model.CVMentor;
import Model.DAO;
import Model.Review;
import Model.Skill;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author PC
 */
@WebServlet(name = "MentorViewCVServlet", urlPatterns = {"/viewcvmentor"})
public class MentorViewCVServlet extends HttpServlet {
   
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MentorViewCVServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MentorViewCVServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    
 
@Override
protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        int mentorid;
        String action, success;
        MentorSkillDAO msDAO = new MentorSkillDAO();
        ReviewDAO rDAO = new ReviewDAO();
        MyMentorDAO mDAO = new MyMentorDAO();
        AccountDAO aDAO = new AccountDAO();
        HttpSession session = request.getSession();
        Account account = (Account)session.getAttribute("acc");
        mentorid = Integer.parseInt(request.getParameter("mentorid"));
        CVMentor cv = mDAO.getCV(mentorid); //thay 7 = mentorid
        List<Skill> mentorSkills = msDAO.getSkillMentor(mentorid);
        Account accoun = aDAO.getUserByMentorId(mentorid);

       
        action = (request.getParameter("action") == null) ? "" : request.getParameter("action");
        success = (request.getParameter("success") == null) ? "" : request.getParameter("success");
        if (cv == null) {
            response.sendRedirect("home1.jsp");
            return;
        }

        ArrayList<Integer> ratings = new ArrayList<>();
        for (int i = 5; i >= 1; i--) {
            ratings.add(rDAO.countRating(i, mentorid));
        }
        
        ArrayList<Review> reviewList = rDAO.getReview(mentorid);
      

            request.setAttribute("action", action);
            request.setAttribute("success", success);
            request.setAttribute("account", account);
            request.setAttribute("accoun", accoun);

            request.setAttribute("listS", mentorSkills);
            request.setAttribute("ratings", ratings);
            request.setAttribute("reviewList", reviewList);
            request.setAttribute("mentorid", mentorid);
            request.setAttribute("cv",cv);
            request.getRequestDispatcher("viewMentorCV.jsp").forward(request, response);           
         
}
    

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
