/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Mentor;

import Context.MyMentorDAO;
import Context.RequestMentorDAO;
import Context.Request_SkillDAO;
import Context.SkillDAO;
import Context.StatusMentorDAO;
import Context.StatusRequestDAO;
import Model.Account;
import Model.Mentor;
import Model.Request1;
import Model.Request_Skill;
import Model.Skill;
import Model.StatusMentor;
import Model.StatusRequest;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
@WebServlet(name = "ListInvitingRequest", urlPatterns = {"/listinviting"})
public class ListInvitingRequest extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListInvitingRequest</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListInvitingRequest at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        Account acc = (Account) session.getAttribute("acc");
        if (acc != null) {
            MyMentorDAO md = new MyMentorDAO();
            Mentor m = md.getMentorbyUserID(acc.getId());
            RequestMentorDAO rm = new RequestMentorDAO();
            Request_SkillDAO rs = new Request_SkillDAO();
            ArrayList<Request_Skill> listRS = rs.getAll();
            SkillDAO s = new SkillDAO();
            List<Skill> listS = s.listAllSkill();
            ArrayList<Request1> list = rm.getRequestByMentor(m.getId());
            StatusRequestDAO srDao = new StatusRequestDAO();
            List<StatusRequest> listSM = srDao.getAllStatus();
            request.setAttribute("listSM", listSM);
            request.setAttribute("listS", listS);
            request.setAttribute("listRS", listRS);
            request.setAttribute("listR", list);
            request.getRequestDispatcher("listInvitingRequest.jsp").forward(request, response);
        } else {
            response.sendRedirect("login");
        }
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
