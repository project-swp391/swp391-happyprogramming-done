/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Mentor;

import Context.FrameDAO;
import Context.MentorSkillDAO;
import Context.MyMentorDAO;
import Context.SkillDAO;
import Model.Account;
import Model.CVMentor;
import Model.Framework;
import Model.Mentor;
import Model.Skill;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;


/**
 *
 * @author Admin
 */
@WebServlet(name = "UpdateMentorCV", urlPatterns = {"/updatementor"})
public class UpdateMentorCV extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("acc");
        if (a == null) {
            response.sendRedirect("login");
        } else {
            SkillDAO sDAO = new SkillDAO();
            MyMentorDAO mDAO = new MyMentorDAO();
            MentorSkillDAO msDAO = new MentorSkillDAO();
            FrameDAO fDAO = new FrameDAO();
            Mentor m = mDAO.getMentorbyUserID(a.getId());
            Date now = Date.valueOf(LocalDate.now());
            CVMentor cv = mDAO.getCV(m.getId());
            request.setAttribute("cv", cv);
            List<Skill> l = msDAO.getSkillMentor(m.getId());
            List<Skill> list = sDAO.listAllSkill();
            List<Framework> listf = fDAO.getFameMentor(m.getId());
            List<Framework> listF = fDAO.getAllFamework();
             request.setAttribute("now", now);
            request.setAttribute("ListF", listF);
            request.setAttribute("Listf", listf);
            request.setAttribute("ListS", l);
            request.setAttribute("List", list);
            request.getRequestDispatcher("updateMentor.jsp").forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String fullname = request.getParameter("fullname");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        String doB = request.getParameter("dob");
        String sex = request.getParameter("gender");
        String address = request.getParameter("address");
        String job = request.getParameter("job");
        String introJob = request.getParameter("intro_job");
        String achievement = request.getParameter("achievement");
        String service = request.getParameter("service");
        String [] skill = request.getParameterValues("skill");
        String [] famework = request.getParameterValues("famework");
        boolean gender ;
        if(sex.equals("0")){
            gender = false  ;
         }else{
            gender = true;
        } 
        String profession;
        switch (job) {
            case "0":
                profession = "";
                break;
            case "1":
                profession = "Sinh viên";
                break;
            case "2":
                profession = "Giáo viên";
                break;
            default:
                profession = "Đã tốt nghiệp";
                break;
        }
       
        Date dob = Date.valueOf(doB);
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("acc");
        MyMentorDAO mDAO = new MyMentorDAO();
        MentorSkillDAO msDAO = new MentorSkillDAO();
        FrameDAO fDAO = new FrameDAO();
        Mentor m = mDAO.getMentorbyUserID(a.getId());
        mDAO.updateCVMentor(m.getId(), fullname, dob, email, gender, phone, address, profession, introJob, achievement, service);
        fDAO.deleteMentorfame(m.getId());
        msDAO.deleteMentorskill(m.getId());
        for (String o : skill) {
            int s = Integer.parseInt(o);
            msDAO.insertMentorSkill(m.getId(), s);
        }
        for (String o : famework) {
            int f = Integer.parseInt(o);
            fDAO.insertMentorFame(m.getId(), f);
        }
        response.sendRedirect("updatementor");
        
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
