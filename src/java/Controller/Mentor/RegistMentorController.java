 /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Mentor;


import Context.FrameDAO;
import Context.MentorSkillDAO;
import Context.MyMentorDAO;
import Context.ProfileDAO;
import Context.SkillDAO;
import Model.Account;
import Model.Framework;
import Model.Mentor;
import Model.Profile;
import Model.Skill;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.util.List;

/**
 *
 * @author Admin
 */
@WebServlet(name = "RegistMentorController", urlPatterns = {"/registmentor"})
public class RegistMentorController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        SkillDAO d = new SkillDAO();
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("acc");
        if (a == null) {
            response.sendRedirect("login");
        } else {
            ProfileDAO pDAO = new ProfileDAO();
            FrameDAO fDAO = new FrameDAO();
            List<Skill> l = d.listAllSkill();
            Profile p = pDAO.getProfile(a.getId());
            List<Framework> list = fDAO.getAllFamework();
            request.setAttribute("ListF", list);
            request.setAttribute("p", p);
            request.setAttribute("ListS", l);
            request.getRequestDispatcher("registMentor.jsp").forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String fullname = request.getParameter("fullname");
        String phone = request.getParameter("phone");
        String email = request.getParameter("email");
        String doB = request.getParameter("dob");
        String sex = request.getParameter("gender");
        String address = request.getParameter("address");
        String job = request.getParameter("job");
        String introJob = request.getParameter("intro_job");
        String achievement = request.getParameter("achievement");
        String service = request.getParameter("service");
        String[] skill = request.getParameterValues("skill");
        String[] famework = request.getParameterValues("famework");
        MyMentorDAO mDAO = new MyMentorDAO();
        MentorSkillDAO msDAO = new MentorSkillDAO();
        FrameDAO fDAO = new FrameDAO();
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("acc");
        Mentor m = mDAO.getMentorbyUserID(a.getId());
        Date dob = Date.valueOf(doB);
        boolean gender;
        if (sex.equals("0")) {
            gender = false;
        } else {
            gender = true;
        }
        String profession;
        switch (job) {
            case "0":
                profession = "";
                break;
            case "1":
                profession = "Sinh viên";
                break;
            case "2":
                profession = "Giáo viên";
                break;
            default:
                profession = "Đã tốt nghiệp";
                break;
        }
       
        mDAO.insertCV(m.getId(), fullname, dob, email, gender, phone, address, profession, introJob, service, achievement);
        mDAO.UpdateStatusMentor(4, m.getId());
        for (String o : skill) {
            int s = Integer.parseInt(o);
            msDAO.insertMentorSkill(m.getId(), s);
        }
        for (String o : famework) {
            int f = Integer.parseInt(o);
            fDAO.insertMentorFame(m.getId(), f);
        }

        response.sendRedirect("editprofile");

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
