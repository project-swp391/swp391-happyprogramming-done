/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controller.Mentor;

import Model.Account;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author dell
 */
public abstract class BaseMentorController extends HttpServlet {
 
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Account acc = (Account) req.getSession(false).getAttribute("acc");
        if (acc.getRole_id() == 1 || acc.getRole_id() == 2) {
            processGet(req,resp);
        } else {
            resp.getWriter().print("You are not in our staff! Contact admin to support this page!");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Account acc = (Account) req.getSession(false).getAttribute("acc");
        if (acc.getRole_id() == 1 || acc.getRole_id() == 2) {
            processPost(req,resp);
        } else {
            resp.getWriter().print("You are not in our staff! Contact admin to support this page!");
        }
    }

    public abstract void processGet(HttpServletRequest req, HttpServletResponse resp);

    public abstract void processPost(HttpServletRequest req, HttpServletResponse resp);  
}