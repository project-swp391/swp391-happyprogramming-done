/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Mentor;

import Context.RateDAO;
import Context.RequestDAO;
import Context.RequestInvitedDAO;
import Context.Request_SkillDAO;
import Context.SkillDAO;
import Model.Account;
import Model.Request1;
import Model.Skill;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class ViewAllStatisticRequestMentorController extends BaseMentorController {

    
    final int recordPerPage = 6;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    public void processGet(HttpServletRequest request, HttpServletResponse response) {

        try {
            int page = 1;

            HttpSession session = request.getSession();
            Account acc = (Account) session.getAttribute("acc");

            //
            if (request.getParameter("page") != null) {
                page = Integer.parseInt(
                        request.getParameter("page"));
            }

            RequestDAO rDao = new RequestDAO();
            SkillDAO sDao = new SkillDAO();
            RateDAO raDao = new RateDAO();
            RequestInvitedDAO riDao = new RequestInvitedDAO();

            //get all request
            ArrayList<Request1> listR = rDao.getAll((page - 1) * recordPerPage, recordPerPage, acc.getId());
            int totalRecord = rDao.getTotalRecord(acc.getId());
            
            int noOfPages = (int) Math.ceil((double) totalRecord / recordPerPage);

            //get all skill belong to request
            for (Request1 r : listR) {
                ArrayList<Skill> listS = sDao.getSkillByRequestID(r.getId());
                r.setSkill(listS);
            }

            int totalAccept = rDao.getTotalRequestByStatus(2,acc.getId());
            int totalCancel = rDao.getTotalRequestByStatus(3, acc.getId());
            int totalRequest = rDao.getTotalRecord();
            int totalInvited = rDao.getTotalRecord(acc.getId());
            double rate = raDao.getAvarageRate();

            request.setAttribute("invited", totalInvited);
            request.setAttribute("rate", rate);
            request.setAttribute("totalAccept", totalAccept);
            request.setAttribute("totalCancel", totalCancel);
            request.setAttribute("percentAccept", totalAccept / (double) totalRequest * 100);
            request.setAttribute("percentCancel", totalCancel / (double) totalRequest * 100);
            request.setAttribute("currentPage", page);
            request.setAttribute("noOfPages", noOfPages);
            request.setAttribute("listR", listR);
            request.getRequestDispatcher("/ViewAllStatisticRequestMentor.jsp").forward(request, response);
        } catch (ServletException ex) {
            Logger.getLogger(ViewAllStatisticRequestMentorController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ViewAllStatisticRequestMentorController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
//

//    public static void main(String[] args) {
//        RequestDAO rDao = new RequestDAO();
//
//        SkillDAO sDao = new SkillDAO();
//        int totalAccept = rDao.getTotalRequestByStatus(4);
//        System.out.println(totalAccept);
//    }

    @Override
    public void processPost(HttpServletRequest req, HttpServletResponse resp) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
