/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Context.MyMentorDAO;
import Context.RequestDAO;
import Context.RequestMentorDAO;
import Context.Request_SkillDAO;
import Context.SkillDAO;
import Model.Account;
import Model.MyMentor;
import Model.Request1;
import Model.RequestMentor;
import Model.Request_Skill;
import Model.Skill;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author dell
 */
public class CreateRequestController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateRequestController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateRequestController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        if (request.getParameter("mentorID") == null) {
            response.sendRedirect("listallmentor");
        } else {
            int mentorID = Integer.parseInt(request.getParameter("mentorID"));
            request.getSession().setAttribute("mentorID", mentorID);

            SkillDAO sDao = new SkillDAO();
            List<Skill> listSkill = sDao.listAllSkillByMentorID(mentorID);

            //get current date
            Date now = Date.valueOf(LocalDate.now());

            request.setAttribute("now", now);
            request.setAttribute("listSkill", listSkill);
            request.getRequestDispatcher("createrequest.jsp").forward(request, response);
        }

    }

//    public static void main(String[] args) {
//        RequestDAO rqDao = new RequestDAO();
//
//        Date now = Date.valueOf(LocalDate.now());
//        Date deadlineDate = Date.valueOf("2023-7-6");
//        Account from = new Account();
//        from.setId(4); 
//        Request rq = new Request();
//        rq.setId(3);
//        rq.setTitle("ffffffffffffffffffff");
//        rq.setContent("ffffffffffffffffffffffffffffffffffffff");
//        rq.setDeadlineDate(deadlineDate);
//        rq.setDeadlineHour(4.5);
//        rq.setFrom(from);
//        rq.setCreateDate(now);
//
//        //insert request to database
//        rqDao.insert(rq);
//        System.out.println(requestID);
//    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        RequestDAO rqDao = new RequestDAO();
        RequestMentorDAO rmDAO = new RequestMentorDAO();
        Request_SkillDAO rsDao = new Request_SkillDAO();

        //get account is logging
        HttpSession sesson = request.getSession();
        Account acc = (Account) sesson.getAttribute("acc");

        //get data send from client
        String title = request.getParameter("title");
        String content = request.getParameter("content");
        Date deadlineDate = Date.valueOf(request.getParameter("date"));
        String time = request.getParameter("hour");
        Double deadlineHour = Double.parseDouble(time);

        String skillID1 = request.getParameter("skill_1");
        String skillID2 = request.getParameter("skill_2");
        String skillID3 = request.getParameter("skill_3");

        int skill_1 = -1;
        int skill_2 = -1;
        int skill_3 = -1;

        int ac = acc.getId();
        Account from = new Account();
        from.setId(ac);
        //get current date
        Date now = Date.valueOf(LocalDate.now());
        

        //count request int DB
        int count = rqDao.countTotal();
        int requestID = ++count;

        Request1 rq = new Request1();
        rq.setId(requestID);
        rq.setTitle(title);
        rq.setContent(content);
        rq.setDeadlineDate(deadlineDate);
        rq.setDeadlineHour(deadlineHour);
        rq.setFrom(from);
        rq.setCreateDate(now);

        //insert request to database
        rqDao.insert(rq);

        int mentorID = -1;
        
        if (request.getSession(false).getAttribute("mentorID") != null) {
            mentorID = (int) request.getSession(false).getAttribute("mentorID");
        }
        
        Account mentor = new Account();
        mentor.setId(mentorID);

        //create mentor belong to request
        RequestMentor reqMentor = new RequestMentor(rq, mentor);
        rmDAO.insert(reqMentor);

        //set mentorID session to null after create mentor
        request.getSession().setAttribute("mentorID", null);

        Request_Skill reqSkill = new Request_Skill();
        Skill skill = new Skill();
        
        //insert skill for request
   
        if (!skillID1.equalsIgnoreCase("-1")) {
            skill_1 = Integer.parseInt(skillID1);
            skill.setId(skill_1);
            reqSkill.setRequest(rq);
            reqSkill.setSkill(skill);
            rsDao.insert(reqSkill);
        }

        //kiem tra skill bi trung lap khong? neu khong thi insert
        if (!skillID2.equalsIgnoreCase("-1") && !skillID2.equalsIgnoreCase(skillID1)) {
            skill_2 = Integer.parseInt(skillID2);
            skill.setId(skill_2);
            reqSkill.setRequest(rq);
            reqSkill.setSkill(skill);
            rsDao.insert(reqSkill);
        }

        if (!skillID3.equalsIgnoreCase("-1") && !skillID3.equalsIgnoreCase(skillID1) && !skillID3.equalsIgnoreCase(skillID2)) {
            skill_3 = Integer.parseInt(skillID3);
            skill.setId(skill_3);
            reqSkill.setRequest(rq);
            reqSkill.setSkill(skill);
            rsDao.insert(reqSkill);
        }
     
        response.sendRedirect("listRequest");

    }

//    public static void main(String[] args) {
//        Request_SkillDAO rsDao = new Request_SkillDAO();
//        Request_Skill reqSkill = new Request_Skill();
//        Skill skill = new Skill();
//        Request1 rq = new Request1();
//        skill.setId(2);
//        rq.setId(23);
//
//        reqSkill.setRequest(rq);
//        reqSkill.setSkill(skill);
//        rsDao.insert(reqSkill);
//    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
