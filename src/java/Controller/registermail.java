/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Context.AccountDAO;
import Model.Account;

import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Pattern;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Admin
 */
@WebServlet(name = "registermail", urlPatterns = {"/registermail"})
public class registermail extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String user = request.getParameter("username");
        String pass = request.getParameter("password");
        String repass = request.getParameter("repassword");
        String email = request.getParameter("email");
        String fullname = request.getParameter("fullname");
        String phone = request.getParameter("phone");
        String dob = request.getParameter("dob");
        String address = request.getParameter("address");
        String gender = request.getParameter("gender");
        String role = request.getParameter("role");
        boolean check = false;
        if (user.length() < 6 || user.length() > 20) {
            request.setAttribute("mess", "Tên đăng nhập phải có từ 6 - 20 ký tự.");
            check = true;
        }

        if (pass.length() < 6) {
            request.setAttribute("mess1", "Mật khẩu phải có ít nhất 6 ký tự.");
            check = true;
        }

        if (!pass.equals(repass)) {
            request.setAttribute("mess2", "Mật khẩu không trùng nhau.");
            check = true;
        }

        String emailRegex = "^[a-zA-Z]{1}[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
        Pattern emailPattern = Pattern.compile(emailRegex);
        if (!emailPattern.matcher(email).matches()) {
            request.setAttribute("mess4", "Định dạng email không hợp lệ.");
            check = true;        
        }
        Date DoB = Date.valueOf(dob);
        LocalDate DobLocal = DoB.toLocalDate();
        LocalDate today = LocalDate.now();
        if (DobLocal.isAfter(today)) {
            request.setAttribute("mess5", "Ngày này vẫn chưa tới !");
            check = true;
        }
        String phoneRegex = "^0[0-9]{9}";
        Pattern phonePattern = Pattern.compile(phoneRegex);
        if (!phonePattern.matcher(phone).matches()) {
            request.setAttribute("mess3", "Định dạng phone không hợp lệ.");
            check = true;
        }
        AccountDAO d = new AccountDAO();
        Account a = d.checkExit(user, email);
        if (check) {
            request.getRequestDispatcher("register.jsp").forward(request, response);
        } else {
            if (a != null) {
                request.setAttribute("mess", "Tên đăng nhập hoặc email đã tồn tại");
                request.getRequestDispatcher("register.jsp").forward(request, response);
            } else {
                // send data -> page register 
                HttpSession sessionn = request.getSession();

                sessionn.setAttribute("user", user);
                sessionn.setAttribute("pass", pass);
                sessionn.setAttribute("email", email);
                sessionn.setAttribute("fullname", fullname);
                sessionn.setAttribute("phone", phone);
                sessionn.setAttribute("dob", dob);
                sessionn.setAttribute("address", address);
                sessionn.setAttribute("gender", gender);
                sessionn.setAttribute("role", role);
                sessionn.setMaxInactiveInterval(60);
                // send mail 
                String username = "hoangtuan69a1@gmail.com";
                String password = "wwmuixnwnzxtzgqp";
                Properties props = new Properties();
                String subject = "Confirm account information";
                int duration = 120;
                Random random = new Random();
                int otp = 100000 + random.nextInt(900000); // Số OTP gồm 6 chữ số
                sessionn.setAttribute("otp", otp);
                long currentTime = System.currentTimeMillis();
                long endTime = currentTime + (duration * 1000);
                sessionn.setAttribute("endtime", endTime);
                String body = "OTP: " + otp + " OTP End Time: " + new java.util.Date(endTime);

                props.put("mail.smtp.auth", "true"); // Yêu cầu xác thực khi kết nối với máy chủ SMTP.
                props.put("mail.smtp.starttls.enable", "true");//Sử dụng TLS để bảo mật kết nối.
                props.put("mail.smtp.host", "smtp.gmail.com");// Địa chỉ máy chủ SMTP của Gmail.
                props.put("mail.smtp.port", "587");// Cổng của máy chủ SMTP.
                props.put("mail.smtp.charset", "UTF-8"); //  Đặt bộ mã hóa cho email là UTF-8.
                // Đăng nhập vào tài khoản email của bạn   
                Session session = Session.getInstance(props, new javax.mail.Authenticator() {

                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

                try {
                    // Tạo một message mới
                    Message message = new MimeMessage(session);
                    message.setFrom(new InternetAddress(username));
                    message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
                    message.setSubject(subject);
                    message.setText("text/html; charset=utf-8");
                    message.setText(body);

                    // Gửi message
                    Transport.send(message);

                } catch (MessagingException e) {
                    throw new RuntimeException(e);
                }

                response.sendRedirect("register");

            }
        }
    }
}
