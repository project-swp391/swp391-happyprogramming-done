/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Context.RequestDAO;
import Context.RequestMentorDAO;
import Context.SkillDAO;
import Model.Account;
import Model.Request1;
import Model.RequestMentor;
import Model.Skill;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 *
 * @author dell
 */
@WebServlet(name = "ListRequestController", urlPatterns = {"/listRequest"})
public class ListRequestController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestMentorDAO rmDao = new RequestMentorDAO();
        SkillDAO sDao = new SkillDAO();
        RequestDAO rDao = new RequestDAO();
//        ArrayList<Request> listReq = rDao.getAll();
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("acc");
        ArrayList<RequestMentor> listRM = rmDao.getAll();
        ArrayList<Request1> listReq = rDao.getAll(account.getId());
        
        //get all skill belong to request
            for (Request1 r : listReq) {
                ArrayList<Skill> listS = sDao.getSkillByRequestID(r.getId());
                r.setSkill(listS);
            }
        request.setAttribute("listRM", listRM);
        request.setAttribute("listReq", listReq);
        request.getRequestDispatcher("listrequestbyme.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

//    public static void main(String[] args) {
//        RequestMentorDAO rmDao = new RequestMentorDAO();
//
////        ArrayList<Request> listReq = rDao.getAll(account.getId());
//        ArrayList<RequestMentor> listRM = rmDao.getAll();
//        for (RequestMentor requestMentor : listRM) {
//            System.out.println(requestMentor.getRequest().getId());   
//        }
//    }
}
