/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;
import java.util.ArrayList;


/**
 *
 * @author dell
 */
public class Request1 {
    private int id;
    private Account from;
    private Account to;
    private String title;
    private Date deadlineDate;
    private Double deadlineHour;
    private Date createDate;
    private String content;
    private int statusId;
    private StatusRequest status;
    private ArrayList<Skill> skill = new ArrayList<>();
    
    public Request1() {
    }

    public Request1(int id, Account from, String title, Date deadlineDate, Double deadlineHour, Date createDate, String content, StatusRequest status) {
        this.id = id;
        this.from = from;
        this.title = title;
        this.deadlineDate = deadlineDate;
        this.deadlineHour = deadlineHour;
        this.createDate = createDate;
        this.content = content;
        this.status = status;
    }

    public Request1(int id, Account from, String title, Date deadlineDate, Double deadlineHour, Date createDate, String content, int statusId, StatusRequest status) {
        this.id = id;
        this.from = from;
        this.title = title;
        this.deadlineDate = deadlineDate;
        this.deadlineHour = deadlineHour;
        this.createDate = createDate;
        this.content = content;
        this.statusId = statusId;
        this.status = status;
    }
    public Request1(int id, Account from, Account to, String title, Date deadlineDate, Double deadlineHour, Date createDate, String content, StatusRequest status) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.title = title;
        this.deadlineDate = deadlineDate;
        this.deadlineHour = deadlineHour;
        this.createDate = createDate;
        this.content = content;
        this.status = status;
    }
   
    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getFrom() {
        return from;
    }

    public void setFrom(Account from) {
        this.from = from;
    }
     public Account getTo() {
        return to;
    }

    public void setTo(Account to) {
        this.to = to;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDeadlineDate() {
        return deadlineDate;
    }

    public void setDeadlineDate(Date deadlineDate) {
        this.deadlineDate = deadlineDate;
    }

    public Double getDeadlineHour() {
        return deadlineHour;
    }

    public void setDeadlineHour(Double deadlineHour) {
        this.deadlineHour = deadlineHour;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public StatusRequest getStatus() {
        return status;
    }

    public void setStatus(StatusRequest status) {
        this.status = status;
    }
    
      public ArrayList<Skill> getSkill() {
        return skill;
    }

    public void setSkill(ArrayList<Skill> skill) {
        this.skill = skill;
    }
    @Override
    public String toString() {
        return "Request1{" + "id=" + id + ", from=" + from + ", to=" + to + ", title=" + title + ", deadlineDate=" + deadlineDate + ", deadlineHour=" + deadlineHour + ", createDate=" + createDate + ", content=" + content + ", statusId=" + statusId + ", status=" + status + ", skill=" + skill + '}';
    }
    
    
}
