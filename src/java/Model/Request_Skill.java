/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author dell
 */
public class Request_Skill {
    private Request1 request;
    private Skill skill;

    public Request_Skill(Request1 request, Skill skill) {
        this.request = request;
        this.skill = skill;
    }

    public Request_Skill() {
    }

    public Request1 getRequest() {
        return request;
    }

    public void setRequest(Request1 request) {
        this.request = request;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }
    
    
}
