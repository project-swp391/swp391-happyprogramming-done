/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;

/**
 *
 * @author quanr
 */
public class Request {
    private int request_id;
    private int user_id;
    private int skill_id;
    private String title;
    private Date deadline_date;
    private int deadline_hour;
    private String Content;
    private int status_id;
    private int mentotID;

    public Request() {
    }

    public Request(int request_id, int user_id, int skill_id, String title, Date deadline_date, int deadline_hour, String Content, int status_id, int mentotID) {
        this.request_id = request_id;
        this.user_id = user_id;
        this.skill_id = skill_id;
        this.title = title;
        this.deadline_date = deadline_date;
        this.deadline_hour = deadline_hour;
        this.Content = Content;
        this.status_id = status_id;
        this.mentotID = mentotID;
    }

    public int getMentotID() {
        return mentotID;
    }

    public void setMentotID(int mentotID) {
        this.mentotID = mentotID;
    }

    

    public int getRequest_id() {
        return request_id;
    }

    public void setRequest_id(int request_id) {
        this.request_id = request_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getSkill_id() {
        return skill_id;
    }

    public void setSkill_id(int skill_id) {
        this.skill_id = skill_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDeadline_date() {
        return deadline_date;
    }

    public void setDeadline_date(Date deadline_date) {
        this.deadline_date = deadline_date;
    }

    public int getDeadline_hour() {
        return deadline_hour;
    }

    public void setDeadline_hour(int deadline_hour) {
        this.deadline_hour = deadline_hour;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String Content) {
        this.Content = Content;
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    @Override
    public String toString() {
        return "Request{" + "request_id=" + request_id + ", user_id=" + user_id + ", skill_id=" + skill_id + ", title=" + title + ", deadline_date=" + deadline_date + ", deadline_hour=" + deadline_hour + ", Content=" + Content + ", status_id=" + status_id + '}';
    }

    
}
