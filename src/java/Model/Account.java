
package Model;


public class Account {
    private int id;
    private String user_name;
    private String password_user;
    private String email;
    private int role_id;
    public Account() {
    }

    public Account(int id, String user_name, String password_user, String email, int role_id) {
        this.id = id;
        this.user_name = user_name;
        this.password_user = password_user;
        this.email = email;
        this.role_id = role_id;
    }
    public Account(int id, String user_name, String email, int role_id) {
        this.id = id;
        this.user_name = user_name;
        this.email = email;
        this.role_id = role_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPassword_user() {
        return password_user;
    }

    public void setPassword_user(String password_user) {
        this.password_user = password_user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    @Override
    public String toString() {
        return "Account{" + "id=" + id + ", user_name=" + user_name + ", password_user=" + password_user + ", email=" + email + ", role_id=" + role_id + '}';
    }
    
    
}
