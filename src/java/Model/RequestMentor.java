/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author dell
 */
public class RequestMentor {
    
   private Request1 request;
   private Account mentor;
   private int mentorId;
   private int count ;
   private double percent;
   

    public RequestMentor() {
    }

    public RequestMentor( int mentorId, int count) {
        this.mentorId = mentorId;
        this.count = count;
    }

    public RequestMentor( int mentorId, double percent) {
        this.mentorId = mentorId;
        this.percent = percent;
    }
    
    
    public RequestMentor(Request1 request, Account mentor) {
        this.request = request;
        this.mentor = mentor;
    }

    public RequestMentor(Request1 request, Account mentor, int mentorId) {
        this.request = request;
        this.mentor = mentor;
        this.mentorId = mentorId;
    }

    public double getPercent() {
        return percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }
    
     public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
    

    public int getMentorId() {
        return mentorId;
    }

    public void setMentorId(int mentorId) {
        this.mentorId = mentorId;
    }
    

    public Request1 getRequest() {
        return request;
    }

    public void setRequest(Request1 request) {
        this.request = request;
    }

    public Account getMentor() {
        return mentor;
    }

    public void setMentor(Account mentor) {
        this.mentor = mentor;
    }

    @Override
    public String toString() {
        return "RequestMentor{" + "mentorId=" + mentorId + ", percent=" + percent + '}';
    }

   

   
   
}
