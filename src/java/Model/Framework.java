/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;


public class Framework {
    private int fameID ;
    private String fameName;

    public Framework() {
    }

    public Framework(int fameID, String fameName) {
        this.fameID = fameID;
        this.fameName = fameName;
    }

    public int getFameID() {
        return fameID;
    }

    public void setFameID(int fameID) {
        this.fameID = fameID;
    }

    public String getFameName() {
        return fameName;
    }

    public void setFameName(String fameName) {
        this.fameName = fameName;
    }

    @Override
    public String toString() {
        return "Famework{" + "fameID=" + fameID + ", fameName=" + fameName + '}';
    }
    
    
    
}
