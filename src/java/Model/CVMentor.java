/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.util.Date;

public class CVMentor {
    private int mentorId;
    private String fullname ;
    private Date dob ;
    private String email;
    private boolean sex ;
    private String phone ;
    private String address ;
    private String job ;
    private String introJob ;
    private String avatar ;
    private String achievement ;
    private String service ;
 
    
    public CVMentor() {
    }

    public CVMentor(int mentorId, String fullname, Date dob, String email, boolean sex, String phone, String address, String job, String introJob, String avatar, String achievement, String service) {
        this.mentorId = mentorId;
        this.fullname = fullname;
        this.dob = dob;
        this.email = email;
        this.sex = sex;
        this.phone = phone;
        this.address = address;
        this.job = job;
        this.introJob = introJob;
        this.avatar = avatar;
        this.achievement = achievement;
        this.service = service;
    }

    
    

    public int getMentorId() {
        return mentorId;
    }

    public void setMentorId(int mentorId) {
        this.mentorId = mentorId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    

    public String getIntroJob() {
        return introJob;
    }

    public void setIntroJob(String introJob) {
        this.introJob = introJob;
    }

    public String getAchievement() {
        return achievement;
    }

    public void setAchievement(String achievement) {
        this.achievement = achievement;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return "CVMentor{" + "mentorId=" + mentorId + ", fullname=" + fullname + ", dob=" + dob + ", email=" + email + ", sex=" + sex + ", phone=" + phone + ", address=" + address + ", job=" + job + ", introJob=" + introJob + ", avatar=" + avatar + ", achievement=" + achievement + ", service=" + service + '}';
    }
    

    
}
 