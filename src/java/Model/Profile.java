
package Model;

import java.util.Date;


public class Profile {
    private String avatar ;
    private String fullname ;
    private boolean sex ;
    private String phone ;
    private Date dob ;
    private String address ;
    private String description ;

    public Profile() {
    }

    public Profile(String avatar, String fullname, boolean sex, String phone, Date dob, String address, String description) {
        this.avatar = avatar;
        this.fullname = fullname;
        this.sex = sex;
        this.phone = phone;
        this.dob = dob;
        this.address = address;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    @Override
    public String toString() {
        return "Profile{" + "avatar=" + avatar + ", fullname=" + fullname + ", sex=" + sex + ", phone=" + phone + ", dob=" + dob + ", address=" + address + ", description=" + description + '}';
    }

    
    
    
}
