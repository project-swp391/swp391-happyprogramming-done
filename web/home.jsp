<%-- 
    Document   : home
    Created on : May 16, 2023, 8:42:44 AM
    Author     : quanr
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <title>Learn program with Quanlailaptrinh</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/home.css">
        <link rel="shortcut icon" href="../images/favicon.png" type="image/x-icon">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>

    <body>


        <div class="header">
            <!-- top-bar-header -->
            <div class="top-bar">
                <div class="container d-flex justify-content-between">
                    <!-- topbar-left -->
                    <div class="topbar-left">
                        <ul class="topbar-left-infor">
                            <li><a href=""><i class="fa fa-question-circle" aria-hidden="true"></i>Ask a Question</a></li>
                            <span>/</span>
                            <li><a href=""><i class="fa fa-envelope-o" aria-hidden="true"></i>quantbhe163724@fpt.edu.vn</a></li>
                        </ul>
                    </div>
                    <!-- topbar-right -->
                    <div class="topbar-right">
                        <ul class="topbar-right-infor">
                            <li><button id="register-header" class="btn btn-Login"><a href="login.jsp">Login</a></button></li>

                            <li><button id="register-header" class="btn btn-register"><a href="register.jsp">Register</a></button></li>
                        </ul>
                    </div>
                </div>
            </div>
            <hr style="margin-top: -4px;">
        </div>

        <!-- top-header-menu -->
        <div class="header-top">
            <div class="container-fluid has-bg-img w-100 vh-100 ">
                <div class="container-fluid position-absolute p-2 d-flex header-top-menu position-fixed" style="z-index: 5;">
                    <div class="container-fluid logo w-75 m-0 mr-5">
                        <a href=""><img src="https://cortex.qodeinteractive.com/wp-content/uploads/2017/07/logo.png" class="img-fluid"
                                        style="width: 45px; height:45px" alt="Responsive image"></a>
                    </div>
                    <div class="container-fluid w-100 menuList ml-5 mr-5 mt-2">
                        <div class="nav d-xl-flex justify-content-between d-none ">
                            <li class="nav-item">
                                <div class="dropdown">
                                    <a class="nav-link text-decoration-none text-dark font-weight-bold p-0" href=""
                                       data-toggle="dropdown">HOME</a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">HOME 1</a>
                                        <a class="dropdown-item" href="#">HOME 2</a>
                                        <a class="dropdown-item" href="#">HOME 3</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <div class="dropdown">
                                    <a class="nav-link text-decoration-none text-dark font-weight-bold p-0" href=""
                                       data-toggle="dropdown">PAGES</a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">PAGE 1</a>
                                        <a class="dropdown-item" href="#">PAGE 2</a>
                                        <a class="dropdown-item" href="#">PAGE 3</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <div class="dropdown">
                                    <a class="nav-link text-decoration-none text-dark font-weight-bold p-0" href=""
                                       data-toggle="dropdown">PORTFOLIO</a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">PORTFOLIO 1</a>
                                        <a class="dropdown-item" href="#">PORTFOLIO 2</a>
                                        <a class="dropdown-item" href="#">PORTFOLIO 3</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <div class="dropdown">
                                    <a class="nav-link text-decoration-none text-dark font-weight-bold p-0" href=""
                                       data-toggle="dropdown">BLOG</a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">BLOG 1</a>
                                        <a class="dropdown-item" href="#">BLOG 2</a>
                                        <a class="dropdown-item" href="#">BLOG 3</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <div class="dropdown">
                                    <a class="nav-link text-decoration-none text-dark font-weight-bold p-0" href=""
                                       data-toggle="dropdown">SHOP</a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">SHOP 1</a>
                                        <a class="dropdown-item" href="#">SHOP 2</a>
                                        <a class="dropdown-item" href="#">SHOP 3</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <div class="dropdown">
                                    <a class="nav-link text-decoration-none text-dark font-weight-bold p-0" href=""
                                       data-toggle="dropdown">ELEMENTS</a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">ELEMENT 1</a>
                                        <a class="dropdown-item" href="#">ELEMENT 2</a>
                                        <a class="dropdown-item" href="#">ELEMENT 3</a>
                                    </div>
                                </div>
                            </li>
                        </div>
                    </div>
                    <div class="container-fluid icon w-50">
                        <form class="d-flex">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search">
                                <button type="button" class="btn btn-secondary"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <!--top-header -->
        <section id="top-header" class="landing-section">
            <div class="container ">
                <div class="row">
                    <div class="col-xs-12 col-lg-8 ">
                        <div class="wrap-block-head-text">
                            <div class="block-head-text">
                                <h1>
                                    Learn to code:
                                    <br>
                                    From zero to hero
                                </h1>
                                <p>Easier to get started with coding on Quanlailaptrinh</p>
                            </div>
                        </div>
                    </div>
                    <style>
                        .login_btn{
                            color: black;
                            background-color: #FFC312;
                            width: 100px;
                        }

                        .login_btn:hover{
                            color: white;
                            background-color: green;

                        }

                    </style>
                    <div class="col-xs-12 col-lg-4" >
                        <div class="wrap-form-head-register">
                            <div class="form-head-register">
                                <h4 class="title-block">Start now</h4>
                                <div>
                                    <form method="POST" action="login">
                                        <p class="text-danger">${mess}</p>
                                        <div class="form-group">
                                            <input name="user" type="text" value="${username}" class="form-control" placeholder="Username" autocomplete="username">
                                        </div>

                                        <div class="form-group">
                                            <input name="pass" type="password" value="${password}" class="form-control" placeholder="Password"
                                                   autocomplete="current-password">
                                        </div>
                                        <div class="form-check">
                                            <div class="row align-items-center remember">
                                                <input name="remember"   type="checkbox" checked>Remember Me
                                            </div>
                                            <div id="blockcbAgree-error"></div>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" value="login" class="btn float-right login_btn">
                                        </div>
                                    </form>
                                </div>

                                <div class="cuoi" style="margin-top: 70px;">
                                    <div class=" links" style="text-align: center;">
                                        Don't have an account?<a href="register.jsp">Sign Up</a>
                                    </div>
                                    <div class="d-flex justify-content-center">
                                        <a href="fogotpass.jsp">Forgot your password?</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section-1 -->
        <div class="container bg-info">

        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    </body>

</html>