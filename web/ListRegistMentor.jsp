<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>List Register Mentor</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    </head>
    <style>
        body {
            overflow: hidden;
            padding: 15px;
            font-family: Arial, sans-serif;
            background-color: #f8f8f8;
            background-image: url('https://haycafe.vn/wp-content/uploads/2021/12/Hinh-nen-trang-den-dep-nhat-2022.jpg ');
        }

        .menu {
            height: 100vh;
            background-color: #545b62;
            padding: 20px;
        }
        .menu .list-group-item {
            transition: background-color 0.3s;
        }
        .avatar {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-bottom: 20px;
        }

        .avatar-img {
            width: 150px;
            height: 150px;
            border-radius: 50%;
        }

        .username {
            margin-top: 10px;
            font-weight: bold;
            color:white;
        }


        .menu .list-group-item:hover {
            background-color: #e9ecef;
        }

        .menu .list-group-item.active {
            background-color: #007bff;
            color: blue;
        }


        .content {
            height: 100vh;
            overflow-y: scroll;

        }

        .scrollable-content {
            height: 100%;
        }
        .table{
            padding-bottom: 45px;
        }
        

        .empty-page h2 {
            font-size: 24px;
            margin-bottom: 10px;
        }

        .empty-page p {
            font-size: 16px;
            color: #777;
        }
        .list-group li {
            padding: 0;
        }
        .pagination {
            padding-bottom: 23px;
            display: flex;
            justify-content: center;
            margin-top: 150px;
            margin-left: 330px;
        }

        .pagination a {
            color: #333;
            background-color: #ddd;
            padding: 8px 16px;
            text-decoration: none;
            border: 1px solid #ddd;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        .pagination a:hover:not(.active) {
            background-color: #ddd;
            color: white;
        }


    </style>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 menu">
                    <div class="avatar">
                        <img src="https://antimatter.vn/wp-content/uploads/2022/10/hinh-anh-gai-xinh-de-thuong.jpg" alt="Avatar" class="avatar-img">
                        <div class="username">${sessionScope.acc.user_name}</div>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item ">
                            <a class="nav-link " href="home">
                                <i class="fas fa-home"></i> Trang chủ
                            </a>
                        </li>
                      
                        <li class="nav-item " >
                            <a class="list-group-item" href="StatisticOfAllMentee" >
                                <i class="fas fa-plus-square"></i> Statistic Of All Mentee
                            </a>
                        </li>
                        <li class="list-group-item " >
                            <a class="nav-link" href="createskill.jsp" >
                                <i class="fas fa-plus-square"></i> Create Skill
                            </a>
                        </li>
                        <li class="list-group-item ">
                            <a class="nav-link " href="list-all-mentor">
                                <i class="fas fa-users "></i> Manager mentor
                            </a>
                        </li>
                        <li class="list-group-item ">
                            <a class="nav-link " href="listskill">
                                <i class="fas fa-book"></i> Manager skills
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a class="nav-link text-white" href="list_register_mentor">
                                <i class="fas fa-book"></i> List register mentor
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-cogs"></i> Cài đặt
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9 content">
                    <div class="scrollable-content">
                        <div class="container" >


                            <div class="row">
                                <c:if test="${listCV.size() == 0}">
                                    <div class="col-md-12 empty-page" style="text-align: center;margin-top: 250px;padding-bottom: 50px;">
                                        <div class="empty-image"></div>
                                        <h2>Danh sách trống</h2>
                                        <p style="color: black ; font-family: serif ;font-size: 17px;">Không có yêu cầu nào hiển thị</p>
                                    </div>
                                </c:if>
                                <c:if test="${listCV.size() != 0}">
                                    <c:forEach var="cv" items="${listCV}"  >
                                        <div class="col-md-12" style="margin-bottom: 10px;">
                                            <div class="card request-card">
                                                <div class="card-header">
                                                    <h5 class="card-title"><a href="viewCVRegister?mentorid=${cv.mentorId}">CV ${cv.mentorId}</a></h5>
                                                </div>
                                                <div class="card-body">
                                                    <p class="card-text">Fullname : ${cv.fullname} </p>
                                                    <p class="card-text">Email : ${cv.email}</p>
                                                    <p class="card-text">Profession : ${cv.job} </p>
                                                    <p class="card-text">Skill : 
                                                        <c:forEach var="ms" items="${listMs}" >
                                                            <c:if test="${cv.mentorId == ms.mentorId}">
                                                                <c:forEach var="s" items="${listS}">
                                                                    <c:if test="${s.id == ms.skillId}">
                                                                        ${s.name}, 
                                                                    </c:if>
                                                                </c:forEach>
                                                            </c:if>
                                                        </c:forEach>

                                                    </p>

                                                    <button class="btn btn-success btn-action"  onclick="window.location.href = 'acceptRegist?id=${cv.mentorId}'">Accept</button>
                                                    <button class="btn btn-danger btn-action" onclick="window.location.href = 'rejectRegist?id=${cv.mentorId}'">Reject</button>
                                                </div>
                                            </div>
                                        </div>

                                    </c:forEach>

                                </c:if>
                                <div class="pagination">
                                    <a href="#"><i class="fas fa-angle-double-left"></i></a>
                                    <a href="#" class="active">1</a>
                                    <a href="#" >2</a>
                                    <a href="#">3</a>
                                    <a href="#"><i class="fas fa-angle-double-right"></i></a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>






