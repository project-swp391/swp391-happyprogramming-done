<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="zxx">



    <head>
        <meta charset="utf-8">
        <title>CV Mentor</title>

        <link rel="stylesheet" href="./bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">  
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <style>
            .row{
                width: 100%
            }
            img{
                object-fit: cover;
            }
            .main{
                margin-top:20px;
                color: #1a202c;
                text-align: left;
                background-color: #fff;
            }
            .main-body {
                padding: 15px;
            }
            .card {
                box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
            }

            .card {
                position: relative;
                display: flex;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 0 solid rgba(0,0,0,.125);
                border-radius: .25rem;
            }

            .card-body {
                flex: 1 1 auto;
                min-height: 1px;
                padding: 1rem;
            }

            .gutters-sm {
                margin-right: -8px;
                margin-left: -8px;
            }

            .gutters-sm>.col, .gutters-sm>[class*=col-] {
                padding-right: 8px;
                padding-left: 8px;
            }
            .mb-3, .my-3 {
                margin-bottom: 1rem!important;
            }

            .bg-gray-300 {
                background-color: #e2e8f0;
            }
            .h-100 {
                height: 100%!important;
            }
            .shadow-none {
                box-shadow: none!important;
            }
            .list-unstyled {
                font-size: 20px;
            }
            .list-unstyled li{
                padding-right: 30px
            }
            .list-unstyled li i{
                color: orange;
            }
            .col-sm-9 {
                font-size: 20px
            }
            .mb-0 {
                font-size: 20px;
                font-family: serif
            }
            .mb-3 a .btn{
                height: 40px;
                color: white;
                height: 40px;
            }
            .bi-star-fill{
                color: #ffc107;
            }
            .col-6 .form-control{
                height: 100px;
                width: 480px
            }

            .circular-image {
                margin: 15px
            }
            .circular-image {
                display: inline-block;
            }

            .image-frame {
                padding: 2px;
                border-radius: 5px;
                box-shadow: 2px 2px 2px 2px
            }
            .rating {
                display: inline-block;
                border: 1px solid #cccccc;
                padding: 4px;
                margin: 5px;
            }
            #message {
                position: fixed;
                top: 0;
                left: 0;
                z-index: 10000;
            }
            .pagination {
                padding-bottom: 23px;
                display: flex;
                justify-content: center;
                margin-top: 20px;
            }

            .pagination a {
                color: #333;
                background-color: #ddd;
                padding: 8px 16px;
                text-decoration: none;
                border: 1px solid #ddd;
                margin: 0 4px;
            }

            .pagination a.active {
                background-color: #4CAF50;
                color: white;
                border: 1px solid #4CAF50;
            }

            .pagination a:hover:not(.active) {
                background-color: #ddd;
                color: white;
            }

        </style>
    </head>
    <body> 

        <!-- header -->
        <jsp:include page="menu.jsp"></jsp:include>
            <section class="main" style="padding-top: 125px;">
                <div id="content" class=" row text-center justify-content-center align-content-center " >


                    <div class="col-9" style="padding-top: 30px;">
                        <div class="main-body">
                            <div class="row gutters-sm">
                                <div class="col-md-4 mb-3">
                                    <div class="d-flex flex-column align-items-center text-center">
                                        <div  class="image-frame">
                                            <img src="${cv.avatar}" alt="Admin" width="300" height="350">
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="card mb-3">
                                    <div class="card-body">
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Full Name</h6>
                                            </div>
                                            <div class="col-sm-9">

                                                ${cv.fullname}
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Account</h6>
                                            </div>
                                            <div class="col-sm-9">
                                                ${accoun.user_name}
                                            </div>
                                        </div>

                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">DOB</h6>
                                            </div>
                                            <div class="col-sm-9">
                                                ${cv.dob}
                                            </div>
                                        </div>

                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Address</h6>
                                            </div>
                                            <div class="col-sm-9">
                                                ${cv.address}
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Profession </h6>
                                            </div>
                                            <div class="col-sm-9">
                                                ${cv.job}
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Contact</h6>
                                            </div>
                                            <div class="col-sm-9">
                                                <ul class="list-unstyled">
                                                    <li class="list-inline-item"><a href="https://www.facebook.com/themefisher"><i class="bi bi-facebook"></i></a></li>
                                                    <li class="list-inline-item"><a href="https://www.twitter.com/themefisher"><i class="bi bi-twitter"></i></a></li>
                                                    <li class="list-inline-item"><a href="#"><i class="bi bi-instagram"></i></a></li>
                                                    <li class="list-inline-item"><a href="https://dribbble.com/themefisher"><i class="bi bi-dribbble"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <hr>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div>
                            <div class="row" >
                                <div class="col-8 text-left" >
                                    <h4 class="mb-4">SERVICE DESCRIPTION</h4>
                                    <p>
                                        ${cv.service}
                                    </p>
                                </div>
                                <hr width="70%" color="#fff" align="left" size="3px">
                                <div class="col-8 text-left" >
                                    <h4 class="mb-4">INTRODUCTION</h4>
                                    <p>
                                        ${cv.introJob}
                                    </p>
                                </div>
                                <hr width="70%" color="#fff" align="left" size="3px">

                                <div class="col-8 text-left" >
                                    <h4 class="mb-4">ACHIEVEMENTS</h4>
                                    <p>
                                        ${cv.achievement}
                                    </p>
                                </div>
                            </div>
                            <div class="row text-left">
                                <div class="col-8">
                                    <h4 class="mb-4">SKILL </h4>

                                    <c:forEach items="${listS}" var="c">
                                        <div class="circular-image"><img src="${c.url}" width="30" height="40"/>&nbsp;&nbsp; ${c.name}</div>

                                        <br>
                                    </c:forEach>


                                </div>
                            </div>


                        </div>
                        <c:if test="${ account.role_id==3}">
                            <div class="properties-single ftco-animate mb-5 mt-4">
                                <h4 class="mb-4 text-left">RATE AND COMMEN</h4>
                                <form method="post" action="review?mentorid=${mentorid}" class="star-rating">
                                    <div class="row" style="margin-top: 50px">
                                        <div class="col-6">
                                            <div class="form-check">
                                                <input type="radio" class="form-check-input" value="5" name="rating" checked>
                                                <label class="form-check-label" for="exampleCheck1">
                                                    <p class="rate"><span><i class="bi bi-star-fill"></i>  <i class="bi bi-star-fill"></i>  <i class="bi bi-star-fill"></i>  <i class="bi bi-star-fill"></i>  <i class="bi bi-star-fill"></i> ${ratings.get(0)} Đánh Giá</span></p>
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input type="radio" class="form-check-input" value="4" name="rating">
                                                <label class="form-check-label" for="exampleCheck1">
                                                    <p class="rate"><span><i class="bi bi-star-fill"></i>  <i class="bi bi-star-fill"></i>  <i class="bi bi-star-fill"></i>  <i class="bi bi-star-fill"></i>  <i class="bi bi-star"></i> ${ratings.get(1)} Đánh Giá</span></p>
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input type="radio" class="form-check-input" value="3" name="rating">
                                                <label class="form-check-label" for="exampleCheck1">
                                                    <p class="rate"><span><i class="bi bi-star-fill"></i>  <i class="bi bi-star-fill"></i>  <i class="bi bi-star-fill"></i>  <i class="bi bi-star"></i>  <i class="bi bi-star"></i> ${ratings.get(2)} Đánh Giá</span></p>
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input type="radio" class="form-check-input" value="2" name="rating">
                                                <label class="form-check-label" for="exampleCheck1">
                                                    <p class="rate"><span><i class="bi bi-star-fill"></i>  <i class="bi bi-star-fill"></i>  <i class="bi bi-star"></i>  <i class="bi bi-star"></i>  <i class="bi bi-star"></i> ${ratings.get(3)} Đánh Giá</span></p>
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input type="radio" class="form-check-input"  value="1" name="rating">
                                                <label class="form-check-label" for="exampleCheck1">
                                                    <p class="rate"><span><i class="bi bi-star-fill"></i>  <i class="bi bi-star"></i>  <i class="bi bi-star"></i>  <i class="bi bi-star"></i>  <i class="bi bi-star"></i> ${ratings.get(4)} Đánh Giá</span></p>
                                                </label>
                                                <br>
                                                <br>
                                            </div>
                                        </div>

                                        <div class="col-6">
                                            <label style="font-size: 25px; margin-right:325px">Comment</label>
                                            <textarea class="form-control"  placeholder="Đánh giá của bạn" name="comment" style=""></textarea>
                                        </div>
                                        <div class="col-10">
                                            <input style="background-color: #ffc107" class=" btn btn-primary" type="submit" value="Gửi đánh giá">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </c:if>
                        <div class="border p-4 text-left" style="margin: 0px 40px 50px 40px;">
                            <div class="p-4"">
                                <h4>Rate Stars</h4>
                                <div>
                                    <span class="rating">1<i class="bi bi-star-fill"></i> (${ratings.get(4)})</span>
                                    <span class="rating">2<i class="bi bi-star-fill"></i> (${ratings.get(3)})</span>
                                    <span class="rating">3<i class="bi bi-star-fill"></i> (${ratings.get(2)})</span>
                                    <span class="rating">4<i class="bi bi-star-fill"></i> (${ratings.get(1)})</span>
                                    <span class="rating">5<i class="bi bi-star-fill"></i> (${ratings.get(0)})</span>

                                </div>
                            </div>
                            <div class="p-4"">
                                <h4>Đánh giá gần đây</h4>
                            </div>
                            <c:choose>
                                <c:when test="${reviewList.size()==0}">
                                    <div class="pl-5">
                                        <p>Chưa có đánh giá nào.</p>
                                    </div>  
                                </c:when>  

                                <c:otherwise>
                                    <c:forEach items="${reviewList}" var="r">
                                        <hr>
                                        <div class="pl-5" >
                                            <p style="font-family: serif ;font-size: 18px;"> <i class="bi bi-person"></i> ${r.getAccount().getUser_name()} - <span class="ml-1">${r.getRating()} <i class="bi bi-star-fill"></i>  
                                                    <span>__<fmt:formatDate value="${r.getCommentDate()}" pattern="dd/MM/yyyy" /></span>

                                                    <div class="">
                                                        <p class="text-muted ml-0">${r.getComment()}</p>
                                                    </div>
                                                </span>
                                            </p>
                                        </div>
                                        <hr>



                                    </c:forEach>
                                </c:otherwise>
                            </c:choose>
                            <div class="pagination">
                                <a href="#"><i class="fas fa-angle-double-left"></i></a>
                                <a href="#" class="active">1</a>
                                <a href="#" >2</a>
                                <a href="#">3</a>
                                <a href="#"><i class="fas fa-angle-double-right"></i></a>
                            </div>
                        </div>

                    </div>                       
                </div>
            </div>
    </section>

<div class="modal fade" id="message" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Thông báo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    ${action eq 'addrv' ? "Thêm đánh giá" : ""} ${success eq "true" ? ("thành công.") : "thất bại."}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
             
    <script>
        var act = "${action}";
        if (act !== "") {
            $('#message').modal('show');
        }
    </script>
 


</body>
</html>


