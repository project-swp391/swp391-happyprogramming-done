

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
    <head>
        <title>Register</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/register_1.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div class="container">
            <div class="d-flex justify-content-center h-100">


                <div class="card">

                    <div class="card-header" style="padding:0">
                        <h3><a style="text-decoration: none;
                               margin-left: 180px;" href="home.jsp"><img style="width: 80px; " src="./images/R-removebg-preview.png" alt="alt"/></a></h3>
                        <div class="d-flex justify-content-end social_icon">

                        </div>
                    </div>
                    <div class="card-body" style=" margin-top: 20px;
                         padding: 0px 10px;
                         height: 609px;">
                        <form action="register" method="post">
                            <!-- get code -->
                            <div class="input-group form-group">
                                <input name="otp" type="text" class="form-control " style="border-top-left-radius: 5px;
                                       border-bottom-left-radius: 5px; " placeholder="OTP" autocomplete="otp" >
                                <div class="input-group-prepend" style="width: 30%;">
                                    <button class="lay-ma">Get Code</button>
                                </div>
                            </div>
                            ${message}
                        </form>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex justify-content-center links">
                            Don't have an account?<a href="login.jsp">Login</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>
</html>
