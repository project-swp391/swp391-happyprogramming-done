<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <title>Home</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/home1.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>
    <style>
        .price{
            border: 1px solid grey;
            width: 40%;
            border-radius: 6px;
            text-align: center;
        }
        .price h5{
            color: red;
        }
        .subject-tag a:hover{
            background-color: #333333;
            color: white;
            text-decoration: none;
        }
    </style>

    <body>
        <jsp:include page="menu.jsp"></jsp:include>
            <div class="page-content bg-white" style="padding-top: 135px;">
                <div class="section-area section-sp1 ovpr-dark bg-fix online-cours">
                    <div class="background-color-tim background-header-1">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 text-center text-white pt-5 pb-5">
                                    <h2>Online Courses To Learn</h2>
                                    <h5>Own Your Feature Learning New Skills Online</h5>
                                    <form class="cours-search pt-4 ">
                                        <div class="input-group" style="width: 40%; margin: auto;">
                                            <input type="text" class="form-control" placeholder="What do you want to learn today?	">
                                            <div class="input-group-append">
                                                <button class="btn bg-warning" type="submit">Search</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="mw800 m-auto">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6">

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Main slider -->
                <div class="content-block pt-5">
                    <!-- Popular Courses -->
                    <div class="section-area section-sp2 popular-courses-bx">
                        <div class="container">
                            <div class="row pb-5">
                                <div class="col-md-12 heading-bx left">
                                    <h2 class="title-head">Popular<span>Courses</span></h2>
                                    <p style="margin-top: 10px;">It is a long established fact that a reader will be distracted
                                        by the readable content of a page</p>
                                </div>
                            </div>
                            <div id="carouselId" class="carousel slide p-3 bg-light" data-ride="carousel"
                                 style="border-radius: 5px;">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselId" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselId" data-slide-to="1"></li>
                                    <li data-target="#carouselId" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <div class="row">
                                        <c:forEach items="${listS}" var="o">
                                            <div class="item col-md-3" >
                                                <div class="cours-bx">
                                                    <div class="action-box">
                                                        <img src="${o.url}" alt="${o.name}" style="width: 100%; height: 220px;box-shadow: 0px 4px 8px rgba(0, 0, 0, 0.1);
                                                             margin-bottom: 20px; padding: 5px; ">

                                                    </div>
                                                    <div class="info-bx text-center">
                                                        <h5><a href="mentorskill?skill_Id=${o.id}">${o.name}</a></h5>

                                                    </div>
                                                    <div class="cours-more-info">

                                                        <div class="price">
                                                            <del>$${o.price * 120/100}</del>
                                                            <h5>$${o.price}</h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


            <!-- Popular Courses END -->
            <div class="content-block">
                <div class="section-area section-sp2 bg-fix ovbl-dark join-bx text-center"
                     style="background-image:url(image/bg1.jpg);">

                </div>
            </div>

            <!-- Form END -->


            <!-- Testimonials -->
            <div class="section-area section-5 bg-fix text-white" style="background-image:url(../images/bg1.jpg);">
                <div class="background-color-tim">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                                <div class="counter-style-1">
                                    <div class="text-white">
                                        <span class="counter">3000</span><span class="cong">+</span>
                                    </div>
                                    <span class="counter-text">Completed Projects</span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                                <div class="counter-style-1">
                                    <div class="text-white">
                                        <span class="counter">2500</span><span class="cong">+</span>
                                    </div>
                                    <span class="counter-text">Happy Clients</span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                                <div class="counter-style-1">
                                    <div class="text-white">
                                        <span class="counter">1500</span><span class="cong">+</span>
                                    </div>
                                    <span class="counter-text">Questions Answered</span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                                <div class="counter-style-1">
                                    <div class="text-white">
                                        <span class="counter">1000</span><span class="cong">+</span>
                                    </div>
                                    <span class="counter-text">Ordered Coffee's</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Testimonials END -->

            <!-- Lớp học nổi bật -->
            <section>
                <div class="container" style="padding: 70px 0px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="container ">
                                    <div class="row">
                                       
                                               
                                        <div class="col-md-6">
                                            <div class="widget">
                                                <h2 class="widget-title line-bottom-theme-colored-3 ">Gia sư nổi bật</h2>
                                                <ul class="angle-double-right list-border">
                                                <c:forEach items="${listRm}" var="o">
                                                    <c:forEach items="${listCV}" var="cv" >
                                                        <c:if test="${o.mentorId == cv.mentorId}">
                                                    <li><a target="_blank" href="viewcvmentor?mentorid=${o.mentorId}"><img
                                                                style="width:24px;"
                                                                src="${cv.avatar}"
                                                                alt="${cv.fullname}" class="img-circle mr-10"> ${cv.fullname} </a>
                                                        <span class="badge" data-toggle="tooltip" data-placement="top" title="${o.count} request"
                                                              > ${o.count}
                                                        </span>
                                                    </li>
                                                    </c:if>
                                                    </c:forEach>
                                                  </c:forEach>  

                                                </ul>

                                            </div>
                                        </div>


                                        <div class="col-md-12" style="padding: 0;">
                                            <div class="widget">
                                                <h4 class="widget-title line-bottom-theme-colored-2">Tìm gia sư theo các <b
                                                        class="text-theme-colored2">môn phổ biến</b> </h4>
                                                <div class="tags subject-tag">
                                                    <c:forEach items="${lists}" var="o" >

                                                        <a class="active" href="mentorskill?skill_Id=${o.id}">${o.name}</a>

                                                    </c:forEach>
                                                </div>

                                            </div>
                                        </div>


                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
            </section>
            <!-- section - event -->

        </div>
        <jsp:include page="footer.jsp"></jsp:include>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    </body>

</html>