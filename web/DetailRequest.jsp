
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zxx" class="no-js">

    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="img/fav.png" />
        <!-- Author Meta -->
        <meta name="author" content="colorlib" />
        <!-- Meta Description -->
        <meta name="description" content="" />
        <!-- Meta Keyword -->
        <meta name="keywords" content="" />
        <!-- meta character set -->
        <meta charset="UTF-8" />
        <!-- Site Title -->
        <title>Happy Programming</title>

        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:900|Roboto:400,400i,500,700" rel="stylesheet" />
        <!--
            CSS
            =============================================
        -->
        <link rel="stylesheet" href="css/linearicons.css" />
        <link rel="stylesheet" href="css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/magnific-popup.css" />
        <link rel="stylesheet" href="css/owl.carousel.css" />
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/hexagons.min.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css" />
        <link rel="stylesheet" href="css/main.css" />
        <link rel="stylesheet" href="https://unpkg.com/@webpixels/css/dist/index.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <style>
            /* Login v4 */

            .signup_v4 .social-area {
                text-align: center;
                padding-top: 14px;
            }

            .signup_v4 .social-area .title {
                font-size: 20px;
                text-transform: uppercase;
                font-weight: 600;
                display: inline-block;
                color: #007bff;
                position: relative;
            }

            .signup_v4 .social-area .text {
                font-size: 17px;
                font-weight: 400;
                color: #143250;
            }

            .signup_v4 .social-area .title::before {
                position: absolute;
                content: '';
                width: 40px;
                height: 1px;
                background: rgba(0, 0, 0, .2);
                top: 50%;
                -webkit-transform: translateY(-50%);
                -ms-transform: translateY(-50%);
                transform: translateY(-50%);
                left: 100%;
                margin-left: 7px;
            }

            .signup_v4 .social-area .title::after {
                position: absolute;
                content: '';
                width: 40px;
                height: 1px;
                background: rgba(0, 0, 0, .2);
                top: 50%;
                -webkit-transform: translateY(-50%);
                -ms-transform: translateY(-50%);
                transform: translateY(-50%);
                right: 100%;
                margin-right: 7px;
            }

            .signup_v4 ul.social-links {
                padding: 0;
                margin: 0;
            }

            .signup_v4 .social-area .social-links li {
                display: inline-block;
            }

            .signup_v4 .social-area .social-links li a i {
                width: 50px;
                height: 50px;
                border-radius: 50%;
                line-height: 50px;
                display: inline-block;
                color: #fff;
                margin: 0 5px;
                -webkit-box-shadow: 0 5px 10px rgb(0 0 0 / 15%);
                box-shadow: 0 5px 10px rgb(0 0 0 / 15%);
            }

            .signup_v4 .social-area .social-links li a i.fa-facebook-f {
                background: #0069f7;
            }

            .signup_v4 .social-area .social-links li a i.fa-google-plus-g {
                background: #d1062c;
            }

            .signup_v4 .nav-tabs .nav-link.active {
                background: #007bff;
            }

            .signup_v4 .nav-tabs .nav-link {
                background: #143250;
            }

            .signup_v4 .nav-tabs .nav-link {
                border: 0;
                margin: 0;
                padding: 10px 0;
                text-align: center;
                border-radius: 0;
                color: #fff;
            }

            .signup_v4 .nav-tabs li.nav-item {
                width: 50%;
            }

            .signup_v4 .card-body {
                padding: 0px;
            }

            .signup_v4 .card-body .tab-content {
                padding: 0 1.25rem 1.75em;
            }
        </style>
    </head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"/>
</head>

<body>

    <section class="home-banner-area">
        <div class="container">
            <div style="max-height: 440px;" class="row justify-content-center fullscreen align-items-center">
                <div class="col-lg-5 col-md-8 home-banner-left">
                    <h1 class="text-white">
                        Take the first step <br />
                        to learn with us
                    </h1><!--
                    <p class="mx-auto text-white  mt-20 mb-40">
                        In the history of modern astronomy, there is probably no one
                        greater leap forward than the building and launch of the space
                        telescope known as the Hubble.
                    </p>-->
                </div>
                <div class="offset-lg-2 col-lg-5 col-md-12 home-banner-right">
                    <img class="img-fluid" src="img/header-img.png" alt="" />
                </div>
            </div>
        </div>
    </section>
    <!-- ================ End banner Area ================= -->

    <!-- ================ Start Feature Area ================= -->
    <section class="feature-area" style="margin-top: 1.5rem">
        <div class="p-10 bg-surface-secondary">
            <div class="card signup_v4 mb-30">
                <div class="card-body">
                    <!--                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-selected="true">Login</a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link" id="register-tab" data-toggle="tab" href="#register" role="tab" aria-controls="register" aria-selected="false">Register</a>
                                        </li>
                                    </ul>-->
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
                            <h4 class="text-center mt-4 mb-4" style="text-transform: uppercase;">Request Detail</h4>
                            <form action="updateRequest" method="post" id="frm-request"> 
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="inputName">ID: </label>
                                        <input id="name" class="form-control" id="inputEmail4" readonly value="${rq.id}">
                                    </div>
                                    <!--<div><span id='message'></span></div>-->
                                    <div class="form-group col-md-12">
                                        <label for="inputName">From: </label>
                                        <input id="name" class="form-control" id="inputEmail4" readonly value="${rq.from.user_name}" >
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="inputName">To: </label>
                                        <select name="mentorID" id="name" type="text" class="form-control" id="inputEmail4">
                                            <c:forEach items="${listM}" var="m">
                                                <option value="${m.mentor.id}"
                                                        <c:forEach items="${listRM}" var="rm">
                                                            <c:if test="${rm.request.id == rq.id}">
                                                                <c:if test="${rm.mentor.id == m.mentor.id}">selected</c:if>
                                                            </c:if>
                                                        </c:forEach>
                                                        >
                                                    ${m.mentor.user_name}</option>
                                                </c:forEach>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="inputName">Title: </label>
                                        <input id="name" type="text" class="form-control" id="inputEmail4"  required name="title" value="${rq.title}"  >
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="inputName">desciption of request: </label>
                                        <input id="name" type="text" class="form-control" id="inputEmail4"  required="" name="content" value="${rq.content}" >
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="inputName">Date of Request: </label>
                                        <input id="name" type="text" class="form-control" id="inputEmail4"  readonly value="${rq.createDate}" >
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="inputName">Deadline Date: </label>
                                        <input id="name" min="${now}" type="date" class="form-control" id="inputEmail4"  required name="deadlineDate" value="${rq.deadlineDate}">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="inputName">Deadline Hour: </label>
                                        <input id="name" type="text" class="form-control" id="inputEmail4"  required name="deadlineHour" value="${rq.deadlineHour}" >
                                    </div>
                                    <div id="skill-old">
                                        <label>Your selection skill:</label>
                                        <div class="row d-flex">
                                            <div class="form-group col-md-4 text-center">
                                                <select required>
                                                    <c:forEach var="s" items="${rq.skill}">
                                                        <option value="${s.id}"
                                                                >${s.name}</option>
                                                    </c:forEach>
                                                </select>
                                                <button type="button" onclick="changeSkill()">Change skill</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="padding-left: 15px;display: none" id="skill-new">
                                        <label>Choose skill to update:</label>
                                        <div class="row d-flex">
                                            <div class="form-group col-md-4 text-center">
                                                <label for="inputName">Skill 1: </label>
                                                <select name="skill_1" required id="skill1">
                                                    <option value="-1">Select</option>
                                                    <c:forEach var="i" items="${listSkill}">
                                                        <option value="${i.id}"

                                                                >${i.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4 text-center">
                                                <label for="inputName">Skill 2:  </label>
                                                <select name="skill_2" required id="skill2">
                                                    <option value="-1">Select</option>
                                                    <c:forEach var="i" items="${listSkill}">
                                                        <option value="${i.id}"

                                                                >${i.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4 text-center">
                                                <label for="inputName">Skill 3:  </label>
                                                <select name="skill_3" required id="skill3">
                                                    <option value="-1">Select</option>
                                                    <c:forEach var="i" items="${listSkill}">
                                                        <option value="${i.id}"

                                                                >${i.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>
                                        </div>
                                        <input type="hidden" id="skill_1" name="skill_1">
                                        <input type="hidden" id="skill_2" name="skill_2">
                                        <input type="hidden" id="skill_3" name="skill_3">
                                        <div id="btn">
                                            <button type="button" onclick="cancel()">Cancel</button>
                                            <button type="button" onclick="save()">Save</button>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="inputName">Status: </label>
                                        <input id="name" type="text" class="form-control" id="inputEmail4"  readonly value="${rq.status.name}">

                                    </div>
                                    <div class="form-group col-md-12">
                                        <div class="col-md-12">
                                            <button><a href="listRequest">Back</a></button>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="hidden" name="reqID" value="${rq.id}"/>
                                            <button type="button" onclick="submitForm()">Update</button>
                                        </div>
                                        <input type="hidden" name="action" value="update"/>
                                        </form>                                        
                                        <form action="updateRequest" method="post"> 
                                            <div class="col-md-12">
                                                <input type="hidden" name="reqID" value="${rq.id}"/>
                                                <input type="hidden" name="action" value="delete"/>
                                                <button type="submit">Delete</button>
                                            </div>
                                        </form>
                                    </div>



                                </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>


    </section>


    <!-- ================ End footer Area ================= -->

    <script src="js/vendor/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
    crossorigin="anonymous"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/parallax.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/hexagons.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/main.js"></script>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
                                                var isChange = false;
                                                function changeSkill() {
                                                    var oldSkill = document.getElementById('skill-old');
                                                    var newSkill = document.getElementById('skill-new');
                                                    oldSkill.style.display = 'none';
                                                    newSkill.style.display = 'block';
                                                    isChange = true;
                                                }
                                                function cancel() {
                                                    var oldSkill = document.getElementById('skill-old');
                                                    var newSkill = document.getElementById('skill-new');
                                                    oldSkill.style.display = 'block';
                                                    newSkill.style.display = 'none';
                                                    isChange = false;
                                                }
                                                function save() {
                                                    var select1 = document.getElementById("skill1").value;
                                                    var select2 = document.getElementById("skill2").value;
                                                    var select3 = document.getElementById("skill3").value;
                                                    if (select1 === "-1" && select2 === "-1" && select3 === "-1") {
                                                        alert("Please select at least one skill."); // Display an error message
                                                    } else {
                                                        var btn = document.getElementById('btn');
                                                        btn.style.display = 'none';
                                                        var skill1 = document.getElementById("skill1");
                                                        var skill2 = document.getElementById("skill2");
                                                        var skill3 = document.getElementById("skill3");
                                                        skill1.disabled = true;
                                                        skill2.disabled = true;
                                                        skill3.disabled = true;
                                                        isChange = false;
                                                    }
                                                }
                                                function submitForm() {
                                                    if (!isChange) {
                                                        var select1 = document.getElementById("skill1").value;
                                                        var select2 = document.getElementById("skill2").value;
                                                        var select3 = document.getElementById("skill3").value;
                                                        var skill1 = document.getElementById("skill_1");
                                                        var skill2 = document.getElementById("skill_2");
                                                        var skill3 = document.getElementById("skill_3");
                                                        skill1.value = select1;
                                                        skill2.value = select2;
                                                        skill3.value = select3;
                                                        document.getElementById('frm-request').submit();
                                                    } else {
                                                        alert("Please save skill first!.");
                                                    }
                                                }
</script>

</html>



