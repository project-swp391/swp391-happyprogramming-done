
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Fixed Menu and Scrollable Content</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    </head>
    <style>
        .list-group li {
            padding: 0;
        }
        body {
           
            padding: 15px;
            font-family: Arial, sans-serif;
            background-color: #f8f8f8;
            background-image: url('https://phunugioi.com/wp-content/uploads/2020/08/hinh-nen-mau-trang.jpg');
        }

        .menu {
            height: 100vh;
            background-color: #545b62;
            padding: 20px;
        }
        .menu .list-group-item {
            transition: background-color 0.3s;
        }
        .avatar {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-bottom: 20px;
        }

        .avatar-img {
            width: 120px;
            height: 120px;
            border-radius: 50%;
        }

        .username {
            margin-top: 10px;
            font-weight: bold;
            color:white;
        }


        .menu .list-group-item:hover {
            background-color: #e9ecef;
        }

        .menu .list-group-item.active {
            background-color: #007bff;
            color: blue;
        }


        .content {
            height: 100vh;
            overflow-y: scroll;
            padding: 20px;
        }

        .scrollable-content {
            height: 100%;
        }
        .top-menu {
            background-color: #f8f8f8;
            padding: 10px 0;
        }

        .top-menu .container {
            display: flex;
            justify-content: flex-start;
        }

        .menu-list {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        .menu-item {
            display: inline-block;
            margin-right: 20px;
        }

        .menu-link {
            color: #545b62;
            text-decoration: none;
            font-weight: bold;
            padding: 5px;
        }

        .menu-link:hover {
            color: #007bff;
        }


    </style>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand ml-5" href="home"><img style="width: 100px;"
            src="./images/R-removebg-preview.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="nav justify-content-between">
                    <li class="nav-item">
                        <a class="nav-link" href="home">
                            <i class="fas fa-home"></i> Trang chủ
                        </a>
                    </li>
                    <li class="nav-item " >
                            <a class="nav-link" href="StatisticOfAllMentee" >
                                <i class="fas fa-plus-square"></i> Statistic Of All Mentee
                            </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="createskill.jsp">
                            <i class="fas fa-plus-square"></i> Create Skill
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="list-all-mentor">
                            <i class="fas fa-users"></i> Manager mentor
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="listskill">
                            <i class="fas fa-book"></i> Manager skills
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="fas fa-cogs"></i> Cài đặt
                        </a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 menu">
                    <div class="avatar">
                        <img src="https://antimatter.vn/wp-content/uploads/2022/10/hinh-anh-gai-xinh-de-thuong.jpg" alt="Avatar" class="avatar-img">
                        <div class="username">Admin</div>
                    </div>
                    <div class="scroll-container">
                    <ul class="list-group">
                        <li class="list-group-item ">
                            <a class="nav-link " href="home1.jsp">
                                <i class="fas fa-home"></i> Trang chủ
                            </a>
                        </li>
                        
                        <li class="list-group-item " >
                            <a class="nav-link" href="StatisticOfAllMentee" >
                                <i class="fas fa-plus-square"></i> Statistic Of All Mentee
                            </a>
                        </li>
                        <li class="list-group-item " >
                            <a class="nav-link" href="createskill.jsp" >
                                <i class="fas fa-plus-square"></i> Create Skill
                            </a>
                        </li>
                        <li class="list-group-item ">
                            <a class="nav-link " href="list-all-mentor">
                                <i class="fas fa-users "></i> Manager mentor
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="listskill">
                                <i class="fas fa-book"></i> Manager skills
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-cogs"></i> Cài đặt
                            </a>
                        </li>
                    </ul>
                    </div>
                </div>
                <div class="col-md-9 content">
                    <div class="scrollable-content">


                    </div>
                </div>
            </div>
        </div>

    </body>
</html>






