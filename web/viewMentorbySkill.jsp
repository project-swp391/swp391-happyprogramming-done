
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Mentor By Skill</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/mentor.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <jsp:include page="menu.jsp"></jsp:include>
            <section class="layer-overlay overlay-theme-colored2-1 border-bottom"
                     data-bg-img="/public/templates/public/giasu/images/bg/bg-pattern.png"
                     style="background-image: url('./images/bg-pattern.png');padding-top: 135px;">
                <div class="container pt-5 pb-1">
                    <div class="row">
                        <div class="call-to-action m-auto">

                            <div style="width:100%;">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <div class="form-group mb-15">
                                            <input class="form-control required " type="text" placeholder="Môn học" >
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-2">
                                        <div class="form-group mb-15">
                                            <input class="form-control required " type="text" placeholder="Năm sinh"
                                                   >
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-2">
                                        <div class="form-group mb-15">
                                            <div class="styled-select" style="width: 72px;">
                                                <select class="form-control" placeholder="Giới tính" aria-required="true" >
                                                    <option value="" selected="selected">Giới tính</option>
                                                    <option value="male">Nam</option>
                                                    <option value="female">Nữ</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <div class="form-group mb-15">
                                            <div class="styled-select">
                                                <select class="form-control ng-pristine ng-untouched ng-valid ng-empty" placeholder="Giới tính"
                                                        aria-required="true" >
                                                    <option value="" selected="selected">Trình độ</option>
                                                    <option value="student">Sinh viên</option>
                                                    <option value="teacher">Giáo viên</option>
                                                    <option value="graduated">Đã tốt nghiệp</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-2">
                                        <div class="form-group mb-1 mt-0">

                                            <button type="submit"  class="btn btn-success btn-lg"><i
                                                    class="fa fa-search" aria-hidden="true"></i><b style="color:yellow; ">Tìm</b></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <!-- section body -->

            <section>
                <div class="container pt-5" id="listTeachers">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">

                                <div class="container pt-3">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row mtli-row-clearfix">
                                            <c:if test="${listCV.size() == 0}">
                                                <div class="col-md-12 empty-page" style="text-align: center;margin-top: 100px;margin-bottom: 90px;">
                                                    <div class="empty-image"></div>
                                                    <h2>Danh sách trống</h2>
                                                    <p>Không có Mentor nào hiển thị</p>
                                                </div>
                                            </c:if>
                                            <c:if test="${listCV.size() != 0}">
                                                <c:forEach var="o" items="${listCV}"> 
                                                    <div  class="col-sx-12 col-sm-4 col-md-3 mb-2 teacher-item ">
                                                        <div class="doctor-thumb">

                                                            <img src="${o.avatar}"
                                                                 alt="${o.fullname}">
                                                            <div class="descInfo">
                                                                <div class="descDetail">
                                                                    ${o.introJob} </div>
                                                                <div class="buttonCtrl">
                                                                    <a class="btn btn-success btn-create-class btn-xs" href="createRequest?mentorID=${o.mentorId}">Create Request</a>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="info p-3 bg-dark">
                                                            <h4 class="text-white mt-0">
                                                                <a href="viewcvmentor?mentorid=${o.mentorId}" class="titleTeacher">${o.fullname}</a>
                                                            </h4>
                                                            <ul class="list angle-double-right m-0">
                                                                <li class="text-gray-silver "><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp; ${o.address}</li>
                                                                <li class="text-gray-silver"><i class="fa fa-intersex" aria-hidden="true"></i> &nbsp;${o.sex?"Male":"Female"}</li>
                                                                <li class="text-gray-silver"><i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp; ${o.job}
                                                                </li>
                                                                <li class="text-gray-silver textOverflow"><i class="fa fa-book" aria-hidden="true">&nbsp;
                                                                        <c:forEach var="l" items="${listMS}" >
                                                                            <c:if test="${l.mentorId == o.mentorId}">
                                                                                <c:forEach var="p" items="${listS}">
                                                                                    <c:if test="${l.skillId == p.id}">
                                                                                        ${p.name},
                                                                                    </c:if>
                                                                                </c:forEach>
                                                                            </c:if>
                                                                        </c:forEach>
                                                                    </i>
                                                                </li>


                                                            </ul>
                                                        </div>
                                                    </div>
                                                </c:forEach> 
                                            </c:if>


                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <nav>
                                                    <ul class="pagination theme-colored xs-pull-center m-0 mt-1">
                                                        <li> <a href="/gia-su?page=1" aria-label="Previous"> <span aria-hidden="true">«</span> </a>
                                                        </li>
                                                        <li> <a href="/gia-su?page=1" aria-label="Previous"> <i class="fa fa-angle-left"
                                                                                                                aria-hidden="true"></i> </a> </li>
                                                        <li class="active"><a href="/gia-su?page=1">1</a></li>
                                                        <li class=""><a href="/gia-su?page=2">2</a></li>
                                                        <li class=""><a href="/gia-su?page=3">3</a></li>
                                                        <li class=""><a href="/gia-su?page=4">4</a></li>
                                                        <li class=""><a href="/gia-su?page=5">5</a></li>

                                                        <li> <a href="/gia-su?page=2" aria-label="Next"> <i class="fa fa-angle-right"
                                                                                                            aria-hidden="true"></i> </a> </li>
                                                        <li> <a href="/gia-su?page=784" aria-label="Next"> <span aria-hidden="true">»</span> </a>
                                                        </li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>




        </section>
        <jsp:include page="menu.jsp"></jsp:include>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    </body>
</html>
