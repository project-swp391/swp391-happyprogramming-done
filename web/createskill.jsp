
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Create Skill</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <link rel="stylesheet" href="./css/createskill.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    </head>
    <style>
        
input[type="text"] {
    width: 100%;
    padding: 10px;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
.btn-custom {
    display: block;
    width: 100%;
    padding: 10px;
    background-color: #4CAF50;
    color: #fff;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    transition: background-color 0.3s ease;
}
.list-group li {
            padding: 0;
        }


    </style>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 menu">
                    <div class="avatar">
                        <img src="https://antimatter.vn/wp-content/uploads/2022/10/hinh-anh-gai-xinh-de-thuong.jpg" alt="Avatar" class="avatar-img">
                        <div class="username">${sessionScope.acc.user_name}</div>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a class="nav-link" href="home">
                                <i class="fas fa-home"></i> Trang chủ
                            </a>
                        </li>
                      
                        <li class="list-group-item" >
                            <a class="nav-link" href="StatisticOfAllMentee" >
                                <i class="fas fa-plus-square"></i> Statistic Of All Mentee
                            </a>
                        </li>
                        <li class="list-group-item active" >
                            <a class="nav-link text-white" href="createskill.jsp" >
                                <i class="fas fa-plus-square"></i> Create Skill
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="list-all-mentor">
                                <i class="fas fa-users"></i> Manager all mentor
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="listskill">
                                <i class="fas fa-book"></i> Manager skills
                            </a>
                        </li>
                        <li class="list-group-item ">
                            <a class="nav-link " href="list_register_mentor">
                                <i class="fas fa-book"></i> List register mentor
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-cogs"></i> Cài đặt
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9 content">
                    <div class="scrollable-content">
                        
                        <h1 class="mb-4" style="font-family:  serif;">Create Skill</h1>
                        <form action="createskill" method="post">
                            <h2 class="text-danger" style="font-family: serif ; font-weight: bold; font-size: 19px;">${mess}</h2>
                            <div class="mb-2">
                                <label for="skillName" class="form-label">Skill Name</label>
                                <input type="text" class="form-control"   name="name" required>
                            </div>
                            <div class="mb-2">
                                <label for="price" class="form-label">Price</label>
                                <input type="text" class="form-control" name="price" required>
                            </div>

                            <div class="mb-2">
                                <label for="url" class="form-label">URL Image</label>
                                <input type="text" class="form-control"  name="url" required>
                            </div>
                            <div class="mb-2">
                                <label for="skillName" class="form-label">description</label>
                                <textarea name="description"
                                          class="form-control ng-pristine ng-valid ng-empty ng-touched" cols="20"
                                          rows="2"   ></textarea>
                            </div>

                            <div class="mb-2">
                                <label for="status" class="form-label">Status</label>
                                <select class="form-select"  name="status">
                                    <option value="active" selected>Active</option>
                                    <option value="inactive">Inactive</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-custom">OK</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>






