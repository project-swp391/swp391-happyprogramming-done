<%-- 
    Document   : login
    Created on : May 16, 2023, 8:43:48 AM
    Author     : quanr
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
  <head>
    <title>Login</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/login.css">
  </head>
  <body>

    <div class="container">
       
        <div class="d-flex justify-content-center h-100">
            <div class="card">
                 <a style="text-decoration: none;" href="home.jsp"><img style="width: 80px;margin-top: 20px " src="./images/R-removebg-preview.png" alt="alt"/></a>
                <div class="card-header">
                    <h3>Sign In</h3>
                    <div class="d-flex justify-content-end social_icon">
                        <span><i class="fa fa-facebook-square" aria-hidden="true"></i></span>
                        <span><i class="fa fa-google-plus-square" aria-hidden="true"></i></span>
                     
                    </div>
                </div>
                <div class="card-body">
                     <% Boolean passwordExpired = (Boolean) request.getAttribute("passwordExpired"); %>
                        <% if (passwordExpired != null && passwordExpired) { %>
                          <p style="color: red;">Your password has expired. Please reset your password.</p>
                    <% } %>
                    <form action="login" method="post">
                        <p class="text-danger">${mess}</p>
                        
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></span>
                            </div>
                            <input type="text" value="${username}" class="form-control" placeholder="username" name="user">
                            
                        </div>
                        <div class="input-group form-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-key" aria-hidden="true"></i></span>
                            </div>
                            <input type="password" value="${password}" class="form-control" placeholder="password" name="pass">
                        </div>
                        
                        <div class="row align-items-center remember">
                            <input name="remember"   type="checkbox" checked>Remember Me
                        </div>
                        <div class="form-group">
                            <input type="submit" value="login" class="btn float-right login_btn">
                        </div>
                    </form>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-center links">
                        Don't have an account?<a href="register.jsp">Sign Up</a>
                    </div>
                    <div class="d-flex justify-content-center">
                        <a href="fogotpass.jsp">Forgot your password?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>