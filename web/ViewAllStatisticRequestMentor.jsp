<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="img/fav.png" />
        <!-- Author Meta -->
        <meta name="author" content="colorlib" />
        <!-- Meta Description -->
        <meta name="description" content="" />
        <!-- Meta Keyword -->
        <meta name="keywords" content="" />
        <!-- meta character set -->
        <meta charset="UTF-8" />
        <!-- Site Title -->
        <title>Eclipse Education</title>

        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:900|Roboto:400,400i,500,700" rel="stylesheet" />
        <!--
            CSS
            =============================================
        -->
        <link rel="stylesheet" href="css/linearicons.css" />
        <link rel="stylesheet" href="css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/magnific-popup.css" />
        <link rel="stylesheet" href="css/owl.carousel.css" />
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/hexagons.min.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css" />
        <link rel="stylesheet" href="css/main.css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>JSP Page</title>
    </head>
    <style>
        body {
            background-image: url("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQCHlZo0FxUN3WuSp1Z_eM1OzoJh4qCdIzgCWaNmfjXTLrIA_p8LQCSa8k18GJw2o6n2y0&usqp=CAU");
        }
    </style>
    <body>
        <jsp:include page="menu.jsp" ></jsp:include>
        <div class="p-10 bg-surface-secondary" style="padding-top: 176px;padding-bottom: 50px;">
            <div class="container">
                <div class="card">
                    <div class="card-header" style="text-align: center">
                        <h6>VIEW STATISTIC ALL REQUEST </h6>

                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover table-nowrap">
                            <thead class="table-light">
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Skill</th>
                                    <th scope="col">Status</th>              
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${listR}" var="r">
                                    <tr>                                   
                                        <td data-label="Name">
                                            <span>${r.from.user_name}</span>
                                        </td>
                                        <td data-label="Title">
                                            <a class="text-current">
                                                ${r.title}
                                            </a>
                                        </td>
                                        <td data-label="Skill">
                                            <select>
                                                <c:forEach items="${r.skill}" var="s">
                                                    <option>${s.name}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                        <c:if test="${r.status.id == 4}">
                                            <td data-label="Status">
                                                <span class="badge bg-soft-success text-success">Accept</span>
                                            </td>
                                        </c:if>
                                        <c:if test="${r.status.id== 3}">
                                            <td data-label="Status">
                                                <span class="badge bg-soft-danger text-danger">Cancel</span>
                                            </td>
                                        </c:if>
                                        <c:if test="${r.status.id == 2}">
                                            <td data-label="Status">
                                                <span class="badge bg-soft-warning text-warning">Waiting</span>
                                            </td>
                                        </c:if>
                                        <c:if test="${r.status.id == 1}">
                                            <td data-label="Status">
                                                <span class="badge bg-soft-warning text-warning">Open</span>
                                            </td>
                                        </c:if>
                                    </tr>
                                </c:forEach>     
                            </tbody>
                        </table>

                        <div class="d-flex justify-content-center mt-5">
                            <nav aria-label="Page navigation example col-12">
                                <ul class="pagination">
                                    <%--For displaying Previous link except for the 1st page --%>
                                    <c:if test="${currentPage != 1}">
                                        <li class="page-item">
                                            <a class="page-link" href="ViewAllStatisticRequestMentor?page=${currentPage - 1}" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                            </a>
                                        </li>
                                    </c:if>

                                    <%--For displaying Page numbers. The when condition does not display
                                                a link for the current page--%>
                                    <c:forEach begin="1" end="${noOfPages}" var="i">
                                        <c:choose>
                                            <c:when test="${currentPage eq i}"> 
                                                <li class="page-item"><a class="page-link bg-light" href="#">${i}</a></li>
                                                </c:when>
                                                <c:otherwise>
                                                <li class="page-item"><a class="page-link" href="ViewAllStatisticRequestMentor?page=${i}">${i}</a></li>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>

                                    <%--For displaying Next link --%>
                                    <c:if test="${currentPage lt noOfPages}">
                                        <li class="page-item">
                                            <a class="page-link" href="ViewAllStatisticRequestMentor?page=${currentPage + 1}" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                            </a>
                                        </li>
                                    </c:if>
                                </ul>
                            </nav>
                        </div>


                        <table class="table table-hover table-nowrap">
                            <thead class="table-light">
                                <tr style="text-align: center">
                                    <th scope="col">Percent of accepted request</th>
                                    <th scope="col">Percent of canceled request</th>
                                    <th scope="col">Currently accepted request</th> 
                                    <th scope="col">Currently invited request</th> 
                                    <th scope="col">Canceled request</th>
                                    <th scope="col">Rating star</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr style="text-align: center">
                                    <td data-label="%accepted">
                                        <a>
                                            <fmt:formatNumber value="${percentAccept}" pattern="#,##0.00" var="acceptFormat" />
                                            ${acceptFormat}%
                                        </a>
                                    </td>
                                    <td data-label="%canceled">
                                        <a>
                                            <fmt:formatNumber value="${percentCancel}" pattern="#,##0.00" var="cancelFormat" />
                                            ${cancelFormat}%
                                        </a>
                                    </td>
                                    <td data-label="%canceled">
                                        <a>${totalAccept}</a>
                                    </td>
                                    <td data-label="%canceled">
                                        <a>${invited}</a>
                                    </td>
                                    <td data-label="%canceled">
                                        <a>${totalCancel}</a>
                                    </td>
                                    <td data-label="%canceled">
                                        <a>${rate}<i class="fa fa-star" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
         <jsp:include page="footer.jsp" ></jsp:include>                           
        <script src="js/vendor/jquery-2.2.4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/parallax.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.sticky.js"></script>
        <script src="js/hexagons.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/main.js"></script>
    </body>
    <style>

    </style>
</html>
