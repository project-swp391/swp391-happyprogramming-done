<%-- 
    Document   : createrequest
    Created on : May 30, 2023, 6:28:44 PM
    Author     : quanr
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <title>Create request</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="./css/createrequest.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>

    <body>
        <jsp:include page="menu.jsp"></jsp:include>
        <section ng-app="mainApp" class="ng-scope pb-5" style="padding-top: 192px;">
                <div class="container ng-scope" ng-controller="mainCtroller">
                    <div class="row" id="createClassRequest">
                        <div class="col-md-9 infoDetailContent">
                            <form name="createRequet" action="createRequest" method="post" id="frm-request"
                                  class="ng-pristine ng-valid ng-valid-date ng-valid-date-disabled">

                                <h3 class="line-bottom">Tạo yêu cầu</h3>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label>Title<span class="text-danger">*</span></label>
                                        <input name="title" class="form-control ng-pristine ng-untouched ng-valid ng-empty" required
                                               type="text" placeholder="Ví dụ: Tìm gia sư môn C">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Content<span class="text-danger">*</span></label>
                                        <input name="content" class="form-control ng-pristine ng-untouched ng-valid ng-empty" required
                                               type="text"  placeholder="Ví dụ: ...">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Deadline date <span class="text-danger">*</span>: </label>
                                        <input
                                            class="form-control ng-pristine ng-valid ng-isolate-scope ng-empty ng-valid-date ng-touched"
                                            type="date" name="date" min="${now}" required>


                                </div>
                                <div class="form-group col-md-3">
                                    <label>Deadline hour</label><br>
                                    <input name="hour" class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                           type="number" step="any" min="0" required>
                                </div>

                                <div class="form-group col-md-6 " style="margin-top: 8px;">



                                </div>
                                <div style="padding-left: 15px">
                                    <label>Môn học:</label>
                                    <p>${error}</p>
                                    <div class="row d-flex">
                                        <div class="form-group col-md-4 text-center">
                                            <label for="inputName">Skill 1: </label>
                                            <select name="skill_1" required id="skill1">
                                                <option value="-1" selected>Select</option>
                                                <c:forEach var="i" items="${listSkill}">
                                                    <option value="${i.id}">${i.name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4 text-center">
                                            <label for="inputName">Skill 2:  </label>
                                            <select name="skill_2" required id="skill2">
                                                <option value="-1" selected>Select</option>
                                                <c:forEach var="i" items="${listSkill}">
                                                    <option value="${i.id}">${i.name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4 text-center">
                                            <label for="inputName">Skill 3:  </label>
                                            <select name="skill_3" required id="skill3">
                                                <option value="-1" selected>Select</option>
                                                <c:forEach var="i" items="${listSkill}">
                                                    <option value="${i.id}">${i.name}</option>
                                                </c:forEach>
                                            </select>
                                            <p class="text-danger">${mess}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group text-center stickybar col-md-6">
                                    <button class="btn btn-success btn-lg" type="submit" >Create</button>
                                </div>
                                <!-- <style>
                                    .status {
                                      display: inline-block;
                                      padding: 5px 10px;
                                      color: #fff;
                                      font-weight: bold;
                                      border-radius: 5px;
                                      height: 40px;
                                    }
                                  
                                    .status-open {
                                      background-color: green;
                                    }
                                  
                                    .status-processing {
                                      background-color: orange;
                                    }
                                  
                                    .status-cancel {
                                      background-color: red;
                                    }
                                  
                                    .status-closed {
                                      background-color: gray;
                                    }
                                  
                                    .delete-button {
                                      background-color: #f44336;
                                      color: #fff;
                                      padding: 5px 10px;
                                      border-radius: 5px;
                                      cursor: pointer;
                                    }
                                  </style>
                                  
                                 <div class="trang-thai col-md-6">
                                    <div class="status status-open">Open</div>
                                    <div class="status status-processing">Processing</div>
                                    <div class="status status-cancel">Cancel <span class="delete-button" onclick="deleteRequest()"><i class="fa fa-trash" aria-hidden="true"></i></div>
                                    <div class="status status-closed">Closed  <span class="delete-button" onclick="deleteRequest()"><i class="fa fa-trash" aria-hidden="true"></i></div>
                                 </div>
                                  <script>
                                    function deleteRequest() {
                                      // Code xử lý xóa yêu cầu
                                      alert("Yêu cầu đã được xóa.");
                                    }
                                  </script> -->

                            </div>

                    </div>
                    <div class="col-md-3">
                        <div class="sidebar sidebar-left mt-sm-30">
                            <div class="widget">
                                <div class="right-class-new-top-content">
                                    <h4>Tại sao bạn nên tìm giáo viên trên happyprogramming ?</h4>
                                    <p><i class="fa fa-check" aria-hidden="true"></i> Gia sư chất lượng. được kiểm duyệt gắt
                                        gao</p>
                                    <p><i class="fa fa-check" aria-hidden="true"></i> Chỉ cần đăng yêu cầu học</p>
                                    <p><i class="fa fa-check" aria-hidden="true"></i> Chúng tôi sẽ làm cầu nối giữa bạn và
                                        Gia sư</p>
                                    <p><i class="fa fa-check" aria-hidden="true"></i> Hỗ trợ nhanh chóng, thân thiện.</p>
                                    <p><i class="fa fa-check" aria-hidden="true"></i> Dịch vụ gia sư miễn phí</p>
                                </div>
                            </div>
                            <div class="widget list-teacher">
                                <!-- <h4>Danh sách giáo viên đã chọn</h4> -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="successModal" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                        <div class="modal-content">

                            <div class="modal-body bg-theme-colored2">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <span class="text-white">

                                    <br><br><i class="fa fa-check animated tada" style="font-size:8em;"
                                               aria-hidden="true"></i> <br>
                                    <h2 class="text-white">Bạn đã đăng ký thành công!!!</h2>
                                    <div>
                                        - Lớp học của bạn sẽ được đưa lên trang chủ sau khi chúng tôi kiểm duyệt.<br>
                                        - Bạn có thể vào <a href="/thong-tin-ca-nhan" target="_blank"
                                                            style="color:yellow;">thông tin cá nhân</a> để theo dõi tình trạng các lớp của
                                        bạn.
                                    </div>
                                    <button class="btn btn-danger" ng-click="reloadWindow()">Đồng ý</button>

                                </span>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <!-- <div class="modal fade" id="errorModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
                        <div class="modal-content">
    
                            <div class="modal-body bg-danger">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <span>
    
                                    <br><br><i class="fa fa-exclamation animated tada" style="font-size:8em;"
                                        aria-hidden="true"></i> <br>
                                    <h2 class="text-red">Lỗi!!!</h2>
                <!-- ngIf: errors -->

                </span>

            </div>

        </div>
    </div>
</div> 
</div>
</section>
<jsp:include page="footer.jsp"></jsp:include>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
crossorigin="anonymous"></script>
<script>
                                        document.getElementById("frm-request").addEventListener("submit", function (event) {
                                            var select1 = document.getElementById("skill1").value;
                                            var select2 = document.getElementById("skill2").value;
                                            var select3 = document.getElementById("skill3").value;

                                            if (select1 === "-1" && select2 === "-1" && select3 === "-1") {
                                                event.preventDefault(); // Prevent form submission
                                                alert("Please select at least one option."); // Display an error message
                                            }
                                        });
</script>
</body>

</html>
