

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <title>Title</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="./css/viewallskill.css">
    </head>

    <body>
        <jsp:include page="menu.jsp" ></jsp:include>
            <div class="zone-content">
                <div class="head-course">
                    <div class="container">
                        <h2>Learning code online. Let's start with your first course!</h2>
                        <div id="search" class="block-top-head">
                            <div class="input-group">
                                <form id="form-search" action="/learning?">
                                    <input name="name" id="search-course" type="text" class="form-control"
                                           placeholder="Search...">
                                    <span class="input-group-addon btn"><i class="cl-icon-search"></i></span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- skill -->
                <section>
                    <div class="container pt-5" id="listTeachers">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">

                                    <div class="container pt-3">
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="row mtli-row-clearfix">
                                                    <table class="table " style="border:  1px solid gray;">
                                                        <thead class="bg-dark text-white">
                                                            <tr>
                                                                <th scope="col">STT</th>
                                                                <th scope="col">Skill Name</th>
                                                                <th scope="col">Image</th>
                                                                <th scope="col">Detail</th>
                                                            </tr>
                                                        </thead>
                                                         <c:set var="stt" value="1"></c:set>
                                                        <tbody>
                                                            <c:forEach items="${listS}" var="o" begin="1" end="${listS.size()}" varStatus="loop">
                                                            <tr>
                                                                <th scope="row"><c:out value="${loop.index}"/></th>
                                                                <td>${o.name}</td>
                                                                <td><img style="width: 20%;height: 76%" src="${o.url}" alt="alt"/></td>
                                                                <td><a href="mentorskill?skill_Id=${o.id}">Detail</a></td>
                                                            </tr>
                                                             </c:forEach>
                                                        </tbody>
                                                    </table>
                                                    <!--                                                    <table class="table">
                                                                                                            <thead>
                                                                                                                <tr class="row" style="text-align: center">
                                                                                                                    <th class="col-md-2" scope="col">STT</th>
                                                                                                                    <th class="col-md-3" scope="col">Skill Name</th>
                                                                                                                    <th class="col-md-5" scope="col">Image</th>
                                                                                                                    <th class="col-md-2" scope="col">Detail</th>
                                                    
                                                                                                                </tr>
                                                                                                            </thead>
                                                <c:set var="stt" value="1"></c:set>
                                                    <tbody style="text-align: center">
                                                <c:forEach items="${listS}" var="o" begin="1" end="${listS.size()}" varStatus="loop">
                                                    <tr class="row">
                                                        <th class="col-md-2" scope="row"><c:out value="${loop.index}"/></th>
                                                        <td class="col-md-3">${o.name}</td>                                                               
                                                        <td class="col-md-5"><img style="width: 20%;height: 76%" src="${o.url}" alt="alt"/></td>                                                               
                                                        <td class="col-md-2"><a href="">Detail</a></td>

                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>-->



                                            </div>
<!--                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <nav>
                                                        <ul class="pagination theme-colored xs-pull-center m-0 mt-1">
                                                            <li> <a href="/gia-su?page=1" aria-label="Previous"> <span
                                                                        aria-hidden="true">«</span> </a>
                                                            </li>
                                                            <li> <a href="/gia-su?page=1" aria-label="Previous"> <i
                                                                        class="fa fa-angle-left" aria-hidden="true"></i>
                                                                </a> </li>
                                                            <li class="active"><a href="/gia-su?page=1">1</a></li>
                                                            <li class=""><a href="/gia-su?page=2">2</a></li>
                                                            <li class=""><a href="/gia-su?page=3">3</a></li>
                                                            <li class=""><a href="/gia-su?page=4">4</a></li>
                                                            <li class=""><a href="/gia-su?page=5">5</a></li>

                                                            <li> <a href="/gia-su?page=2" aria-label="Next"> <i
                                                                        class="fa fa-angle-right" aria-hidden="true"></i>
                                                                </a> </li>
                                                            <li> <a href="/gia-su?page=784" aria-label="Next"> <span
                                                                        aria-hidden="true">»</span> </a>
                                                            </li>
                                                        </ul>
                                                    </nav>
                                                </div>
                                            </div>-->
                                        </div>
                                        <div style="margin-left: 70px;" class="col-md-3">
                                            <div class="widget">
                                                <h5 class="widget-title line-bottom">Các môn dạy</h5>
                                                <div class="tags">
                                                    <c:forEach items="${listS}" var="o">
                                                        <a
                                                            ng-href="/gia-su?mon-hoc=Toán Lớp 2"
                                                            ng-repeat="subject in topSubject" class="ng-binding ng-scope"
                                                            href="/gia-su?mon-hoc=Toán Lớp 2">${o.name}</a>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </section>

        </div>
        <jsp:include page="footer.jsp" ></jsp:include>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    </body>

</html>