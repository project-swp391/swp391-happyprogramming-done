
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <title>Profile</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>

    <body>
        <style>
            ul.list.menuList {
                list-style: none;
            }
            ul.list.menuList li {
                padding: 0;

            }

            ul.list.menuList li:before {
                content: "";
                display: none;
            }

            ul.list.menuList li {
                list-style: none;
            }

            #avatarThumb {
                position: relative;
            }

            #avatarThumb .btn-change-avatar {
                position: absolute;
                top: 0;
                left: 0;
                background: rgba(0, 0, 0, 0.4);
                color: #ccc;
                border-radius: 0;
            }

            #avatarThumb:hover .btn-change-avatar {

                background: rgba(0, 0, 0, 0.8);
                color: white;
            }
            ul li a{
                text-decoration: none ;
                color: black;
                font-family: sans-serif;
                font-size: 17px;
            }
            .regis-mentor{
                text-align: center;
            }
            label{
                font-family: serif;
                font-weight: bold;
                font-size: 17px;
                 }
        </style>
        <jsp:include page="menu.jsp" ></jsp:include>
            <section ng-app="mainApp" id="mainAppContent" class="ng-scope" style="padding-top: 135px;">
                <div class="container ng-scope" ng-controller="mainCtroller">
                    <div class="section-content">
                        <div class="row" style="margin-top: 20px;">
                            <div class="col-sx-12 col-sm-3 col-md-3" style="background-color: aliceblue;
                                 height: 710px;
                                 margin-bottom: 50px;
                                 padding-top: 20px;
                                 box-shadow: 0 2px 5px rgba(0, 0, 1, 0.2);
                                 ">

                                <div class="doctor-thumb"  style="background-color: aliceblue;padding-left: 15px;padding-top: 10px;margin-bottom: 20px;" >
                                    <img  src="${p.avatar}" alt="" style="width:95%; height: 230px; object-fit: cover;border-radius: 70%;"  >
                                <div class="avatarEditor croppie-container" id="avatarThumbCroppie" style="display:none;">
                                    
                                    
                                </div>
                                <form action="upimage" method="post" enctype="multipart/form-data">
                                    <div class="text-center btnSave mb-10" style="display: grid;">
                                        <input type="file"  name="fileimage" style="width: 95% ;">
                                        <input class="btn btn-xs btn-dark" type="submit" value="cập nhật" style="width: 95% ;">
                                    </div>
                                </form> 
                                <hr/>
                            </div>

                            <div class="info p-20 bg-black-333" style="background-color: aliceblue;">
                                <!-- <p class="text-gray-silver"></p> -->
                                <ul class="list angle-double-right m-0">
                                    <li class="fas fa-users"><strong class="text-gray-lighter"> Username: </strong> <b
                                            style="color:green;">${sessionScope.acc.user_name}</b></li>
                                </ul>
                                <ul class="list list-divider list-border menuList" style="padding: 20px;" >
                                    <li class="list-group-item m-0 active">
                                        <a class="nav-link " href="editprofile">
                                            <i class="fas fa-user"></i> Thông tin cá nhân
                                        </a>
                                    </li>
                                    <c:if test="${sessionScope.acc.role_id == 3}">
                                        <li class="list-group-item ">
                                            <a class="nav-link " href="listRequest">
                                                <i class="fas fa-list-alt"></i> Danh sách yêu cầu 
                                            </a>
                                        </li>
                                        <li class="list-group-item ">
                                            <a class="nav-link " href="staticRequest">
                                                <i class="fas fa-list-alt"></i> Thống kê yêu cầu
                                            </a>
                                        </li>
                                    </c:if>
                                    <c:if test="${sessionScope.acc.role_id == 2 && (sessionScope.status == 1 || sessionScope.status == 2)}">
                                        <li class="list-group-item ">
                                            <a class="nav-link " href="updatementor">
                                                <i class="far fa-user-circle"></i> Cập nhật CV
                                            </a>
                                        </li>
                                        <li class="list-group-item ">
                                            <a class="nav-link " href="listinviting">
                                                <i class="fas fa-list-alt"></i> Danh sách lời mời
                                            </a>
                                        </li>
                                    </c:if>
                                    <li class="list-group-item m-0">
                                        <a class="nav-link " href="changepass.jsp">
                                            <i class="fas fa-address-card"></i> Đổi mật khẩu
                                        </a>
                                    </li>
                                    <li class="list-group-item ">
                                        <a class="nav-link " href="logout">
                                            <i class="fas fa-sign-out-alt"></i> Đăng xuất
                                        </a>
                                    </li>

                                </ul>

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-9" style="
                             margin-bottom: 45px;">
                            <div>
                                <form method="Post" action="editprofile" id="Myform" style="box-shadow: 0 2px 5px rgba(0, 0, 1, 0.2);
                                      padding:25px 25px ; margin-bottom:20px; background-color:aliceblue;height: 710px;  " >
                                    <h3 class="line-bottom mt-0" style="font-family: serif;font-weight: bold ; text-align: center;">Thông tin cá nhân</h3>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>Fullname :<i class="fas fa-star-of-life text-danger"></i></label>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-not-empty"
                                                   type="text" name="fullname" required="" value="${p.fullname}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Phone :<i class="fas fa-star-of-life text-danger"></i></label>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                                   type="phone" id="phone" required="" name="phone" value="${p.phone}" >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-5">
                                            <label>Email :<i class="fas fa-star-of-life text-danger"></i></label>
                                            <input
                                                class="form-control ng-pristine ng-valid ng-empty ng-valid-email ng-touched"
                                                type="email" value="${sessionScope.acc.email}" readonly>
                                        </div>


                                        <div class="form-group col-md-3">
                                            <label>Date of birth :<i class="fas fa-star-of-life text-danger"></i></label><br>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                                   type="date" max="${now}" name="dob" value="${p.dob}" required="" >
                                        </div>
                                        <div class="form-group col-md-4" >
                                            <label>Gender : </label><br>
                                            <c:if test="${p.sex == false}">
                                                <select name="gender" style="height: 38px;" >
                                                    <option selected="" value="0" >Male</option>
                                                    <option value="1" >Female</option>
                                                </select>
                                            </c:if>
                                            <c:if test="${p.sex == true}">
                                                <select name="gender" style="height: 38px;" >
                                                    <option  value="0" >Male</option>
                                                    <option selected="" value="1" >Female</option>
                                                </select>
                                            </c:if>

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Address :<i class="fas fa-star-of-life text-danger"></i></label>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                                   type="text" value="${p.address}" name="address" required="">
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Introduce Yourself<span
                                                    class="ng-binding">(1500 character)</span></label>
                                            <textarea name="description"
                                                      class="form-control ng-pristine ng-valid ng-empty ng-touched" cols="20" 
                                                      rows="5"  id="giasu">${p.description}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group text-center">
                                        <button onclick="return validateForm()" class="btn btn-success btn-lg mt-15" style="margin-top: 40px" >Cập nhật</button>
                                    </div>
                                </form>
                            

                            </div>
                        </div>

                    </div>
                </div>



            </div>
        </section>
        <script>
            function validateForm() {
                var phoneNumber = document.getElementById("phone").value;
                var phoneNumberPattern = /^\d{10}$/; // Biểu thức chính quy kiểm tra dạng 10 chữ số

                if (!phoneNumberPattern.test(phoneNumber)) {
                    alert("Vui lòng nhập số điện thoại cho đúng.");
                    return false; // Ngăn chặn việc gửi form
                }

                return true; // Cho phép gửi form
            }
        </script>                   


        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
        <jsp:include page="footer.jsp" ></jsp:include>
    </body>

</html>
