CREATE DATABASE  IF NOT EXISTS `happy_progamming` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `happy_progamming`;
-- MySQL dump 10.13  Distrib 8.0.33, for Win64 (x86_64)
--
-- Host: localhost    Database: happy_progamming
-- ------------------------------------------------------
-- Server version	8.0.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cvmentor`
--

DROP TABLE IF EXISTS `cvmentor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cvmentor` (
  `cv_id` int NOT NULL AUTO_INCREMENT,
  `mentor_id` int DEFAULT NULL,
  `full_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `sex` bit(1) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `profession` varchar(100) DEFAULT NULL,
  `intro_job` varchar(1500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `avatar` varchar(200) DEFAULT NULL,
  `Service` varchar(1500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `Achievement` varchar(1500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`cv_id`),
  KEY `mentor_id` (`mentor_id`),
  CONSTRAINT `cvmentor_ibfk_1` FOREIGN KEY (`mentor_id`) REFERENCES `mentor` (`mentor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cvmentor`
--

LOCK TABLES `cvmentor` WRITE;
/*!40000 ALTER TABLE `cvmentor` DISABLE KEYS */;
INSERT INTO `cvmentor` VALUES (1,2,'Maria Ozawa','2002-01-25','hoangtuan69a1@gmail.com',_binary '','0978300936','Hà Nội','Sinh viên','chuyen kèm riêng các em từ 18 -20','https://i.pinimg.com/originals/e0/0e/d9/e00ed96cc83f6dc8d3b54e9fb63161d2.jpg',NULL,NULL),(2,1,'tokoda','2002-01-25','hoangtuan69a1@gmail.com',_binary '','0978300936','Hà Nội','Sinh viên','chuyen kèm riêng các em từ 18 -20','https://i.pinimg.com/originals/e0/0e/d9/e00ed96cc83f6dc8d3b54e9fb63161d2.jpg',NULL,NULL),(3,4,'Fukuda','2001-06-06','annhiendau0909@gmail.com',_binary '','0378793050','Japan','Giáo viên','Một giáo viên có tâm và có tầm','images/fukuda.jpg','Một giáo viên có tâm và có tầm','Một giáo viên có tâm và có tầm');
/*!40000 ALTER TABLE `cvmentor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `famework`
--

DROP TABLE IF EXISTS `famework`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `famework` (
  `fame_id` int NOT NULL AUTO_INCREMENT,
  `fame_name` varchar(50) NOT NULL,
  PRIMARY KEY (`fame_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `famework`
--

LOCK TABLES `famework` WRITE;
/*!40000 ALTER TABLE `famework` DISABLE KEYS */;
INSERT INTO `famework` VALUES (1,'VueJS'),(2,'Angular.JS'),(3,'Laravel'),(4,'ASP.NET'),(5,'Spring Boot'),(6,'NodeJs'),(7,'Ruby on Rails');
/*!40000 ALTER TABLE `famework` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mentor`
--

DROP TABLE IF EXISTS `mentor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mentor` (
  `mentor_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `status_id` int DEFAULT NULL,
  PRIMARY KEY (`mentor_id`),
  KEY `user_id` (`user_id`),
  KEY `status_id` (`status_id`),
  CONSTRAINT `mentor_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `mentor_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `mentorstatus` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mentor`
--

LOCK TABLES `mentor` WRITE;
/*!40000 ALTER TABLE `mentor` DISABLE KEYS */;
INSERT INTO `mentor` VALUES (1,1,1),(2,2,1),(3,5,1),(4,10,1);
/*!40000 ALTER TABLE `mentor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mentorfamework`
--

DROP TABLE IF EXISTS `mentorfamework`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mentorfamework` (
  `mentor_id` int DEFAULT NULL,
  `fame_id` int DEFAULT NULL,
  KEY `fame_id` (`fame_id`),
  KEY `mentor_id` (`mentor_id`),
  CONSTRAINT `mentorfamework_ibfk_1` FOREIGN KEY (`fame_id`) REFERENCES `famework` (`fame_id`),
  CONSTRAINT `mentorfamework_ibfk_2` FOREIGN KEY (`mentor_id`) REFERENCES `mentor` (`mentor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mentorfamework`
--

LOCK TABLES `mentorfamework` WRITE;
/*!40000 ALTER TABLE `mentorfamework` DISABLE KEYS */;
INSERT INTO `mentorfamework` VALUES (4,1);
/*!40000 ALTER TABLE `mentorfamework` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mentorskill`
--

DROP TABLE IF EXISTS `mentorskill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mentorskill` (
  `mentor_id` int DEFAULT NULL,
  `Skill_id` int DEFAULT NULL,
  KEY `Skill_id` (`Skill_id`),
  KEY `mentor_id` (`mentor_id`),
  CONSTRAINT `mentorskill_ibfk_1` FOREIGN KEY (`Skill_id`) REFERENCES `skill` (`skill_id`),
  CONSTRAINT `mentorskill_ibfk_2` FOREIGN KEY (`mentor_id`) REFERENCES `mentor` (`mentor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mentorskill`
--

LOCK TABLES `mentorskill` WRITE;
/*!40000 ALTER TABLE `mentorskill` DISABLE KEYS */;
INSERT INTO `mentorskill` VALUES (2,1),(2,3),(2,4),(1,3),(1,4),(4,9),(4,10),(4,11);
/*!40000 ALTER TABLE `mentorskill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mentorstatus`
--

DROP TABLE IF EXISTS `mentorstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mentorstatus` (
  `status_id` int NOT NULL,
  `status_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mentorstatus`
--

LOCK TABLES `mentorstatus` WRITE;
/*!40000 ALTER TABLE `mentorstatus` DISABLE KEYS */;
INSERT INTO `mentorstatus` VALUES (1,'Active'),(2,'Inactive'),(3,'Lock'),(4,'Waiting');
/*!40000 ALTER TABLE `mentorstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment` (
  `payment_id` int NOT NULL AUTO_INCREMENT,
  `request_id` int DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `amount` int DEFAULT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `request_id` (`request_id`),
  CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profile` (
  `profile_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `avatar` varchar(200) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `sex` bit(1) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `description` varchar(1500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`profile_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `profile_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (1,2,'https://photo-cms-giaoducthoidai.epicdn.me/w820/Uploaded/2023/sfrss/2015_07_30/ab9d064ebfc259af11405afc7cbb023b_FZQT.jpg','Maria Ozawa',_binary '','0978300936','1990-01-01','Japan','do you know me ?'),(2,1,'https://photo-cms-giaoducthoidai.epicdn.me/w820/Uploaded/2023/sfrss/2015_07_30/ab9d064ebfc259af11405afc7cbb023b_FZQT.jpg','tokoda',_binary '','0978300936','1990-01-01','Japan','do you know me ?'),(3,9,'https://static2.yan.vn/YanNews/2167221/202102/facebook-cap-nhat-avatar-doi-voi-tai-khoan-khong-su-dung-anh-dai-dien-e4abd14d.jpg','Thai Ba Quan 555',_binary '\0','0378793050','2002-12-11','Nghệ An',NULL),(4,10,'images/fukuda.jpg','Fukuda',_binary '','0378793050','2001-06-06','Japan','');
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request_skill`
--

DROP TABLE IF EXISTS `request_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `request_skill` (
  `request_id` int NOT NULL,
  `skill_id` int NOT NULL,
  PRIMARY KEY (`request_id`,`skill_id`),
  KEY `skill_id_idx` (`skill_id`),
  CONSTRAINT `request_id` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`),
  CONSTRAINT `skill_id` FOREIGN KEY (`skill_id`) REFERENCES `skill` (`skill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request_skill`
--

LOCK TABLES `request_skill` WRITE;
/*!40000 ALTER TABLE `request_skill` DISABLE KEYS */;
INSERT INTO `request_skill` VALUES (31,1),(33,1),(36,1),(30,2),(33,2),(35,2),(23,3),(26,3),(30,3),(32,3),(33,3),(34,3),(31,4),(32,4),(36,4),(27,5),(28,5),(29,5),(31,5),(29,6),(36,6),(27,7),(28,7),(30,7),(34,7),(28,8),(34,8),(26,9),(26,10),(32,11);
/*!40000 ALTER TABLE `request_skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requestdetail`
--

DROP TABLE IF EXISTS `requestdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `requestdetail` (
  `request_id` int DEFAULT NULL,
  `skill_id` int DEFAULT NULL,
  KEY `skill_id` (`skill_id`),
  KEY `request_id` (`request_id`),
  CONSTRAINT `requestdetail_ibfk_1` FOREIGN KEY (`skill_id`) REFERENCES `skill` (`skill_id`),
  CONSTRAINT `requestdetail_ibfk_2` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requestdetail`
--

LOCK TABLES `requestdetail` WRITE;
/*!40000 ALTER TABLE `requestdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `requestdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requestinvited`
--

DROP TABLE IF EXISTS `requestinvited`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `requestinvited` (
  `request_id` int NOT NULL,
  `user__id` int NOT NULL,
  PRIMARY KEY (`user__id`,`request_id`),
  KEY `user_id_idx` (`user__id`),
  KEY `request_id_invited` (`request_id`),
  CONSTRAINT `request_id_invited` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`),
  CONSTRAINT `user_id_invited` FOREIGN KEY (`user__id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requestinvited`
--

LOCK TABLES `requestinvited` WRITE;
/*!40000 ALTER TABLE `requestinvited` DISABLE KEYS */;
/*!40000 ALTER TABLE `requestinvited` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requestmentor`
--

DROP TABLE IF EXISTS `requestmentor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `requestmentor` (
  `request_id` int DEFAULT NULL,
  `mentor_id` int DEFAULT NULL,
  KEY `request_id` (`request_id`),
  KEY `mentor_id` (`mentor_id`),
  CONSTRAINT `requestmentor_ibfk_1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`),
  CONSTRAINT `requestmentor_ibfk_2` FOREIGN KEY (`mentor_id`) REFERENCES `mentor` (`mentor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requestmentor`
--

LOCK TABLES `requestmentor` WRITE;
/*!40000 ALTER TABLE `requestmentor` DISABLE KEYS */;
INSERT INTO `requestmentor` VALUES (17,2),(19,2),(20,1),(21,2),(23,2),(26,2),(27,2),(28,1),(29,1),(30,2),(31,2),(32,2),(33,2),(34,2),(35,2),(36,1);
/*!40000 ALTER TABLE `requestmentor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `requests` (
  `request_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `title` text,
  `deadline_date` date DEFAULT NULL,
  `deadline_hour` double DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `content` text,
  `status_id` int DEFAULT NULL,
  PRIMARY KEY (`request_id`),
  KEY `user_id` (`user_id`),
  KEY `status_id` (`status_id`),
  CONSTRAINT `requests_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `requests_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `requeststatus` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requests`
--

LOCK TABLES `requests` WRITE;
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
INSERT INTO `requests` VALUES (17,6,'nguyễn quốc tuấn','2023-07-06',6.5,'2023-06-27','aaaaaaaaaaa',2),(19,6,'hoang hoang anh','2023-07-07',8.5,'2023-06-27','bbbbbbbbbbbbbbbbbbbbbb',2),(20,6,'aaaaaaaaaaaaaa','2023-07-06',6.5,'2023-06-27','aaaaaaaaaaaaaaaaa',3),(21,6,'Thái Bá Quân','2023-07-07',8.5,'2023-06-27','Anh quan đẹp trai',3),(23,6,'test 10:30','2023-07-07',8.5,'2023-06-27','test 10:30',3),(26,6,'anhquandeptrai','2023-07-09',2,'2023-06-28','anhquandeptrai',2),(27,6,'tuấn xấu trai','2023-07-07',6.5,'2023-06-28','tuấn xấu trai',2),(28,6,'test ngoclong','2023-06-30',8.5,'2023-06-28','test ngoclong',2),(29,7,'adsad','2023-07-30',6.5,'2023-07-01','ád',1),(30,6,'ádasdasd','2023-08-05',8.5,'2023-07-02','ádasdasda',2),(31,6,'mmmmmmm','2023-08-05',6.5,'2023-07-03','mmmmmmmm',2),(32,6,'vvvvvvvvvv','2023-08-05',8.5,'2023-07-04','vvvvvvvv',1),(33,6,'học lập trình c','2023-07-06',10,'2023-07-05','mình muốn học lập trình',1),(34,6,'học lập trình java','2023-07-20',12,'2023-07-05','muốn học lập trình java',1),(35,6,'?:\">\"\":Ơ}\"\"','2023-07-19',23,'2023-07-12','FSDFRƯE',1),(36,6,'FDSFDSSFD','2023-07-21',-5,'2023-07-12','FSDFDSSFD',1);
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requeststatus`
--

DROP TABLE IF EXISTS `requeststatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `requeststatus` (
  `status_id` int NOT NULL,
  `status_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requeststatus`
--

LOCK TABLES `requeststatus` WRITE;
/*!40000 ALTER TABLE `requeststatus` DISABLE KEYS */;
INSERT INTO `requeststatus` VALUES (1,'Open'),(2,'Processing'),(3,'Cancel'),(4,'Closed');
/*!40000 ALTER TABLE `requeststatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews` (
  `review_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `rating` double DEFAULT NULL,
  `comment_review` varchar(255) DEFAULT NULL,
  `date_comment` date DEFAULT NULL,
  `mentor_id` int DEFAULT NULL,
  PRIMARY KEY (`review_id`),
  KEY `user_id` (`user_id`),
  KEY `mentor_id` (`mentor_id`),
  CONSTRAINT `mentor_id` FOREIGN KEY (`mentor_id`) REFERENCES `mentor` (`mentor_id`),
  CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,4,4.2,'okela','2023-06-24',NULL),(2,2,4.6,'good','2023-05-25',NULL);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin'),(2,'mentor'),(3,'mentee');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skill` (
  `skill_id` int NOT NULL AUTO_INCREMENT,
  `skill_name` varchar(50) NOT NULL,
  `decription` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `skill_price` double NOT NULL,
  `image` varchar(255) NOT NULL,
  `status_skill` varchar(255) NOT NULL,
  PRIMARY KEY (`skill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill`
--

LOCK TABLES `skill` WRITE;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
INSERT INTO `skill` VALUES (1,'Lập trình C','Kỹ năng lập trình ngôn ngữ C',50,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),(2,'Lập trình Python','Kỹ năng lập trình ngôn ngữ Python',60,'https://images.careerbuilder.vn/content/images/ngon-ngu-lap-trinh-python-CareerBuilder-8.png','inactive'),(3,'Lập trình Java','Kỹ năng lập trình ngôn ngữ Java',70,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),(4,'Lập trình PHP','Kỹ năng lập trình ngôn ngữ PHP',55,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),(5,'Lập trình Ruby','Kỹ năng lập trình ngôn ngữ Ruby',65,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),(6,'Lập trình JavaScript','Kỹ năng lập trình ngôn ngữ JavaScript',70,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','inactive'),(7,'Lập trình Swift','Kỹ năng lập trình ngôn ngữ Swift',75,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),(8,'Lập trình Kotlin','Kỹ năng lập trình ngôn ngữ Kotlin',80,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),(9,'Lập trình C++','Kỹ năng lập trình ngôn ngữ C++',65,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),(10,'Lập trình C#','Kỹ năng lập trình ngôn ngữ C#',70,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),(11,'Lập trình C','Kỹ năng lập trình ngôn ngữ C',50,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','inactive'),(12,'Lập trình Python','Kỹ năng lập trình ngôn ngữ Python',60,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','inactive');
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `password_user` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `email` varchar(50) NOT NULL,
  `role_id` int DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `email` (`email`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'ngoclong','123','long@gmail.com',2),(2,'vien','123','vien@gmail.com',2),(3,'quan54321','123','quandeptrai@gmail.com',1),(4,'hieu','123','hieu@gmail.com',3),(5,'long','123','long2@gmail.com',3),(6,'quan','123','quan@gmail.com',3),(7,'thanh','123','thanh@gmail.com',3),(8,'beo123456','123456','quanruoi2002@gmail.com',3),(9,'quan55555','123456','quantbhe163724@fpt.edu.vn',2),(10,'test123','123456','annhiendau0909@gmail.com',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-20 18:22:57
